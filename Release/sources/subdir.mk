################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../sources/Element.cpp \
../sources/Index.cpp \
../sources/List.cpp \
../sources/Node.cpp \
../sources/Tree.cpp \
../sources/run.cpp \
../sources/traits.cpp 

OBJS += \
./sources/Element.o \
./sources/Index.o \
./sources/List.o \
./sources/Node.o \
./sources/Tree.o \
./sources/run.o \
./sources/traits.o 

CPP_DEPS += \
./sources/Element.d \
./sources/Index.d \
./sources/List.d \
./sources/Node.d \
./sources/Tree.d \
./sources/run.d \
./sources/traits.d 


# Each subdirectory must supply rules for building sources it contributes
sources/%.o: ../sources/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -D__cplusplus=201103L -I"../includes" -I"../includes/exprtk" -I"../includes/antlr3.5.2" -O3 -Wall -c -fmessage-length=0 -std=c++11 -fopenmp -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


