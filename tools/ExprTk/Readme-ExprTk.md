# ANother Tool for Language Recognition (ANTLR) #
----
  - [Home]
  - [Download]
  - [Wiki]
  - [GitHub]
  - [API]

Puedes leer este fichero en: http://dillinger.io/

[Home]:     http://www.antlr3.org/
[Download]: http://www.antlr3.org/download.html
[Wiki]:     https://theantlrguy.atlassian.net/wiki/display/ANTLR3/ANTLR+3+Wiki+Home
[GitHub]:   https://github.com/antlr/antlr3
[API]:      http://www.antlr3.org/api/index.html

## Empezando con ANTLR ##

ANTLR es una herramienta que permite generar una analizador completo para un lenguaje definido por el usuario. También permite procesar cadenas de texto del lenguaje. ANTLR requiere de la máquina virtual [Java].
[Java]: http://www.oracle.com/technetwork/es/java/javase/downloads/index.html

Para definir un analizador debemos crear un fichero que especifique la gramática y los elementos del lenguaje. En el siguiente [enlace] podemos encontrar una ayuda para crear este fichero.
[enlace]: https://theantlrguy.atlassian.net/wiki/display/ANTLR3/Grammars

Acto seguido, debemos utilizar ANTLR para generar el analizador un lenguaje de programación y luego compilar los ficheros generados. ANTLR permite generar nuestro analizador en diferentes lenguajes de programación, siendo Java el lenguaje con mayor soporte por esta herramienta. En este documento se hará referencia al lenguaje C++ como lenguaje final generado por ANTLR.

## ¿Cómo usar ANTLR? ##
 1. Comprobar que tienes la máquina virtual JAVA. Abre una terminal y ejecuta el siguiente comando:
 ```sh
 $ java -version
 ```
 La salida debe ser similar a la siguiente:
 ```sh
 java version "1.8.0_20"
 Java(TM) SE Runtime Environment (build 1.8.0_20-b26)
 Java HotSpot(TM) 64-Bit Server VM (build 25.20-b23, mixed mode)
 ```
 2. Crear una gramática ANTLR válida. El fichero *T.g* contiene lo siguiente:
 ```sh
 grammar T;
 options {language=Cpp;}
 def : ID '=' INT ;
 ID : ('a'..'z')+ ;
 INT : ('0'..'9')+ ;
 WS  : (' '|'\t'|'\r'|'\n')+ {$channel = HIDDEN;} ;
 ```
 3. Descargar *antlr-3.5.2-complete.jar* del siguiente enlace: http://www.antlr3.org/download/antlr-3.5.2-complete.jar
 4. Generar el analizador en C++
 ```sh
 mkdir output
 java -jar antlr-3.5.2-complete.jar -o output T.g
 ```
 Los ficheros generados se encuentran en el directorio ```output```.

## ¿Cómo compilar? ##
 1. 
 ```sh
 $ unzip gtest-*.zip -d ${GTEST_DIR}
 ```

## Crear una librería estática ##
 1. 
 
----

## Resources ##
