DisPar
======

Dispersión Paralelizada (DisPar) es una implementación desarrollada como Proyecto Final de Carrera (PFC) en la  [Universidad de La Laguna][ULL] para el Grupo de Investigación ‘Evolución de galaxias en el Grupo Local’, del [Instituto Astrofísico de Canarias][IAC].

DisPar utiliza la plataforma [Bitbucket Cloud][Bitbucket Cloud] para alojar el repositorio remoto privado, disponible en este [enlace][Remote Repository]. Si se desea obtener privilegios en este repositorio, contactar con los autores. Lo ususarios inexpertos en esta plataforma pueden consultar: [Bitbucket Cloud Documentation][Bitbucket Cloud Documentation].

Detalles
--------

El problema abordado intenta corregir los errores observacionales en la fotometría introducidos por los errores del telescopio, condiciones meteorológicas, etcétera.

Este repositorio ofrece una implementación recursiva que pretende resolver el problema de forma genérica. La implementación explota el paralelismo de los procesadores de un ordenador, a través del uso de las directivas de [OpenMP][OpenMP]. El lenguaje base del desarrollo es C++11.

Existen tres objetivos o targets en esta implementación:
* Debug: orientada a desarrolladores con el fin de depurar errores.
* Test: orientada a probar el correcto funcionamiento de las funcionalidades implementadas.
* Release: orientada a ser el ejecutable utilizado en el entorno de producción.

Requisitos
----------

* Compilador compatible con C++11 y [OpenMP][OpenMP].
* [JsonCpp][JsonCpp].
* [ANother Tool for Language Recognition (ANTLR)][ANTLR] -- requiere [Java][Java].
* [The C++ Mathematical Expression Toolkit Library (ExprTk)][ExprTk].
* [Google C++ Testing Framework][Google Test].

Uso
---

Los targets 'Debug' y 'Release' requieren un fichero como primer y único parámetro. Dicho fichero contendrá toda la información de los parámetros de ejecución. La notación utilizada es [JSON][JSON]. Un ejemplo de este fichero es el siguiente:

```json
{
 "General" : {
  "minSetSize" : 10,
  "maxSetSize" : 100,
  "maxValue"   : 25.0,
  "customized" : false,
  "epsilons"   : [ 0.1, 0.04, 300, 300 ],
  "log"        : "./file-log.txt"
 },
 "ArtificialStars" : {
  "file"      : "./file-artificial",
  "rows"      : 10,
  "columns"   : 6,
  "index"     : [ "c1", "c1-c2", "c5", "c6" ],
  "sort_file" : "./file-artificial-sort"
 },
 "RealStars" : {
  "file"     : "./file-real",
  "rows"     : 10,
  "columns"  : 6,
  "index"    : [ "c1", "c1-c2", "c5", "c6" ]
 },
 "DispersedStars" : {
  "file"    : "./file-dispersed",
  "columns" : 8
 }
}
```

El target 'Test' no requiere de parámteros de ejecución, pero sí asume la existencia de un directorio llamado 'files' desde la ruta donde se lanza la ejecución.

El ejecutable de cada target está dentro de un directorio con el nombre del target.

Autores
-------

* [Anthony Vittorio Russo Cabrera](https://www.linkedin.com/in/anthonyrussocabrera)
* [Manuel Luis Aznar](https://es.linkedin.com/in/manuel-luis-aznar-6723758a)

[ULL]: <http://www.ull.es/>
[IAC]: <http://iac.es/>
[Bitbucket Cloud]: <https://bitbucket.org/>
[Remote Repository]: <https://bitbucket.org/ULLfypIAC/crowding-effects-cli-recursive>
[Bitbucket Cloud Documentation]: <https://confluence.atlassian.com/bitbucket>
[OpenMP]: <http://openmp.org/>
[JsonCpp]: <https://github.com/open-source-parsers/jsoncpp>
[ANTLR]: <http://www.antlr.org/>
[Java]: <http://www.oracle.com/technetwork/java/javase/overview/index.html>
[ExprTk]: <https://github.com/ArashPartow/exprtk>
[Google Test]: <https://github.com/google/googletest>
[JSON]: <http://www.json.org/>