/**
 * Index.h
 *
 * Fichero que define las cabeceras de la clase índice (Index).
 *
 * Esta clase es la encargada de almacenar de forma anidada las características
 * o índices de búsqueda. En primer lugar, se usa sobre una estructura de árbol
 * (Tree) que permite adaptarse sin problema a añadir nuevos elementos bajo
 * demanda. Una vez finalizada todas las inserciones se construye la estructura
 * de lista (List) que ofrece tiempos más reducidos en la operación de búsqueda.
 *
 */

#ifndef INDEX_H_
#define INDEX_H_

//==Includes===================================================================
#include "traits.h"
#include "Tree.h"
#include "List.h"
#include <iostream>

//==Classes====================================================================
class Index {
  public: // Members
    Tree *topTreeIndex;
    List *topListIndex;
  public: // Special members
    Index();
    virtual ~Index();
  public: // Public methods
    void insertTree(realNumber *&indexesValues, ArtificialStar*& artfStar);
    void finishInsert();
    void findTree(FoundStars*&, RealStar*&, Epsilon*&);
    void findList(FoundStars*&, RealStar*&, Epsilon*&);
    void buildTreeComplete();
  private: // Private methods
    void findRecursive (FoundStars *&foundStars,
                        Tree *&currentTree,
                        Range *&range);
    void findRecursive (FoundStars *&foundStars,
                        List *&currentList,
                        Range *&range);
    void buildCompleteRecursive(Tree *&currentTree);
  public: // Overloaded operators
    friend std::ostream& operator<<(std::ostream&, const Index&);
    std::ostream& ostreamRecursive(std::ostream&, Tree *&) const;
    std::ostream& ostreamRecursive(std::ostream&, List *&) const;
};

#endif /* INDEX_H_ */
