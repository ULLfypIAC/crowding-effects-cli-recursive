/**
 * run.cpp
 *
 * Fichero que define las funciones principales que ejecutará esta
 * implementación.
 *
 * Entre las tareas ha ejecutar se encuentran las siguientes:
 *  - Lectura y comprobación de los datos del fichero de parámetros.
 *  - Lectura del fichero de estrellas artificales.
 *  - Creación de la estructura de búsqueda para el proceso de dispersión.
 *  - Reserva de memoria disponible para procesar por lotes las estrellas
 *    reales.
 *  - Ejecución del algoritmo de dispersión hecho por lotes.
 *  - Almacenamiento de las estrellas dipersas, hecho por lotes.
 *  - Almacenamiento de las estrellas artifciales según el órden establecido
 *    en la creación del índice de búsqueda.
 *  - Liberación de los recursos reservados.
 *  - Abortar la ejecución, en caso de error.
 *  - Generar un registro de los errores.
 *  - Generar un fichero de ejemplo en caso de error.
 *
 */

//==Includes===================================================================
#include "json/json.h"
#include "analyzers/headers/ExpressionAnalyzerParser.hpp"
#include "analyzers/headers/StarAnalyzerParser.hpp"
#include "analyzers/headers/ExpressionEvaluator.hpp"
#include "traits.h"
#include "Index.h"
#include <iostream>
#include <chrono>
#include <functional>
#include <fstream>
#include <string>
#include <algorithm>
#include <new>

//==Data types=================================================================
using Evaluator = ExpressionEvaluator<realNumber>;

//==Variables==================================================================
//--Extern---------------------------------------------------------------------
unsigned int  minSetStars;    //  10
unsigned int  maxSetStars;    //   ?
realNumber    maxValue;       //  25.0
unsigned int  maxIndex;       //   4
unsigned int  artfMaxColumn;  //   6
unsigned int  artfMaxRow;     //   ?
unsigned int  realMaxColumn;  //   6
unsigned int  realMaxRow;     //   ?
unsigned int  dispMaxColumn;  //   8
unsigned int  *counterIndexNodes;
//--Local----------------------------------------------------------------------
// Índices
Index *indexes;
// Epsilon
Epsilon *epsilonsConst;
// Estrellas artificiales
ArtificialStar                      *artfStar;
std::string                          artfName;
std::ifstream                        artfFile;
std::string                          artfSortName;
std::ofstream                        artfSortFile;
unsigned int                         artfLines;
std::string                         *artfIndexExpressions;
Evaluator                           *artfEvaluador;
// Estrellas reales
RealStar                             *realStar;
std::string                           realName;
std::ifstream                         realFile;
unsigned int                          realLines;
std::string                          *realIndexExpressions;
Evaluator                            *realEvaluador;
std::pair <RealStar*,std::ptrdiff_t>  realBuffer;
std::streampos                        realFilePos;
// Estrellas dispersas
DispersedStar                              *dispStar;
std::string                                 dispName;
std::ofstream                               dispFile;
std::pair <DispersedStar*,std::ptrdiff_t>   dispBuffer;
// Otros
bool           customized;
intervalMin   *interval;
timePoint     *start;
std::ofstream  helpFile;
std::string    logName;
std::ofstream  logFile;
unsigned int   bufferMax;
unsigned int   bufferElements;
//==Headers====================================================================
void startTime(timePoint &start,
               const char *message);
void endTime(timePoint &start,
             intervalMin &interval,
             const char *message);
void help();
void checkParameters(int& argc,
                     const char**& argv);
bool loadNoCustomizedArtificialStars(const std::string &line,
                                     ArtificialStar *&star,
                                     realNumber *& indexValues);
bool loadCustomizedArtificialStars(const std::string &line,
                                   ArtificialStar *&star,
                                   realNumber *& indexValues);
void loadArtificialStars();
void getMemoryToBatchProcess();
bool loadNoCustomizedRealStars(const std::string &line,
                               RealStar *&star);
bool loadCustomizedRealStars(const std::string &line,
                             RealStar   *&star);
void loadRealStars();
void preProcess();
void process();
void postProcess();
int  main(int argc, const char *argv[]);
//==Functions==================================================================
/**
 * Se muestra el mensaje pasado por parámetros y se inicia el contador
 * temporal especificado en minutos.
 * @param message Mensaje que se mostrará.
 */
inline void startTime(timePoint &start, const char *message) {
  std::cout << "\n" << message << "\n";
  std::cout.flush();
  start = std::chrono::steady_clock::now();
}
//-----------------------------------------------------------------------------
/**
 * Se muestra el mensaje pasado por parámetros y se detiene el contador
 * temporal especificado en minutos.
 * @param message Mensaje que se mostrará.
 */
inline void endTime(timePoint &start, intervalMin &interval, const char *message) {
  interval = std::chrono::steady_clock::now() - start;
  std::cout << "\n" << message
            << "\n -> " << interval.count() << " minutos\n";
  std::cout.flush();
}
//-----------------------------------------------------------------------------
/**
 * Crea un fichero de ejemplo con el formato admitido para su lectura.
 */
inline void help() {
  helpFile.open("help-file.json", std::ofstream::trunc);
  helpFile
    << "{// Grammar-> http://www.json.org/\n"
    << " // Parser-> [jsoncpp]:https://github.com/open-source-parsers/jsoncpp\n"
    << "\n"
    << " //-------------------------------------------------------------------\n"
    << "  /** # General: object. #\n"
    << "    - Parámetros generales.\n"
    << "   */\n"
    << " \"General\" : {\n"
    << "\n"
    << "  /** # minSetSize: int number. #\n"
    << "    - Número mínimo que debe tener el conjunto de estrellas sintetizadas\n"
    << "      que cumplen con los parámetros de la búsqueda."
    << "   */\n"
    << "  \"minSetSize\" : 10,\n"
    << "\n"
    << "  /** # maxSetSize: int number. #\n"
    << "    - Número máximo que debe tener el conjunto de estrellas sintetizadas\n"
    << "      que cumplen con los parámetros de la búsqueda."
    << "   */\n"
    << "  \"maxSetSize\" : 100,\n"
    << "\n"
    << "  /** # maxValue: float number. #\n"
    << "    - Valor a partir del cual se consideran irrelevantes los datos\n"
    << "      obtenidos de la estrella sintetizada elegida aleatoriamente.\n"
    << "   */\n"
    << "  \"maxValue\" : 25.0,\n"
    << "\n"
    << "  /** # customized: boolean. #\n"
    << "    - Indica si se desea (true) o no (false) realizar una ejecución\n"
    << "      personalizada del algoritmo.\n"
    << "    - Si el valor es true, entonces deben estar presentes los campos\n"
    << "      \"index\" en los objetos \"ArtificialStars\" y \"RealStars\".\n"
    << "    - Si el valor es false, entonces los campos \"index\" en los objetos\n"
    << "      \"ArtificialStars\" y \"RealStars\" serán ignorados.\n"
    << "   */\n"
    << "  \"customized\" : false,\n"
    << "\n"
    << "  /** # epsilons: array of float numbers. #\n"
    << "    - Vector de números en coma flotante que representan los márgenes\n"
    << "      de búsqueda en cada índice.\n"
    << "    - Cada uno de estos valores no está relacionado con ningún otro y\n"
    << "      sólo afecta al índice que está definido en la misma posición.\n"
    << "    - El número de elementos que haya en este vector determina el\n"
    << "      número de índices que utilizará el programa. Por tanto, la\n"
    << "      cardinalidad de los campos \"index\" debe ser la misma que\n"
    << "      la de este vector.\n"
    << "   */\n"
    << "  \"epsilons\" : [\n"
    << "   0.1,\n"
    << "   0.04,\n"
    << "   300,\n"
    << "   300\n"
    << "  ],\n"
    << "\n"
    << "  /** # log: string. #\n"
    << "    - Ruta de donde se desea almacenar los mensajes generados.\n"
    << "    - La ruta puede ser relavita, aunque se recomienda encarecidamente\n"
    << "      que se utilice una ruta absoluta.\n"
    << "   */\n"
    << "  \"log\" : \"logFile.txt\"\n"
    << " },\n"
    << " //-------------------------------------------------------------------\n"
    << "  /** # ArtificialStars: object. #\n"
    << "    - Parámetros propios de las estrellas artificiales ó crowding ó\n"
    << "      sintetizadas.\n"
    << "   */\n"
    << " \"ArtificialStars\" : {\n"
    << "\n"
    << "  /** # file: string. #\n"
    << "    - Ruta de dónde se encuentra el fichero que contiene el registro\n"
    << "      de las estrellas artificiales.\n"
    << "    - La ruta puede ser relavita, aunque se recomienda encarecidamente\n"
    << "      que se utilice una ruta absoluta.\n"
    << "   */\n"
    << "  \"file\" : \"fC.txt\",\n"
    << "\n"
    << "  /** # rows: int number. #\n"
    << "    - Número de filas que contiene el fichero.\n"
    << "   */\n"
    << "  \"rows\" : 10,\n"
    << "\n"
    << "  /** # columns: int number. #\n"
    << "    - Número de la columna mayor que se desean procesar del registro.\n"
    << "    - El programa tendrá en cuenta en cada registro desde la columna\n"
    << "      1 hasta la colunma indicada, incluyendo esta última.\n"
    << "   */\n"
    << "  \"columns\" : 6,\n"
    << "\n"
    << "  /** # index : array of strings. #\n"
    << "    - Expresión que determina el valor que tendrán los índices de las\n"
    << "      estrellas artificiales a partir de sus columnas.\n"
    << "    - Las columnas se expresarán con la letra 'c', seguido del número\n"
    << "      de columna, empezando la numeración en 1. Ej: c01, c1.\n"
    << "    - Están soportadas la mayoría de funciones matemáticas encontradas\n"
    << "      en: http://www.partow.net/programming/exprtk/\n"
    << "    - El número de elementos debe ser el mismo del campo epsilons.\n"
    << "    - No es necesario especificar siempre que el campo customized\n"
    << "      sea false; en cuyo caso, es como si estuviera definido de la\n"
    << "      siguiente forma:\n"
    << "        \"index\":[\"c1\",\"c1-c2\",\"c5\",\"c6\"]\n"
    << "   */\n"
    << "  \"index\" : [\n"
    << "   \"c1\",\n"
    << "   \"c1-c2\",\n"
    << "   \"c5\",\n"
    << "   \"c6\"\n"
    << "  ],\n"
    << "\n"
    << "  /** # sort_file: string. #\n"
    << "    - Ruta de dónde se desea almacenar el fichero de salida ordenado\n"
    << "      de las estrellas artificiales. El criterio de ordenación es el\n"
    << "      el indicado en el campo 'index' anterior.\n"
    << "    - Este fichero contendrá una columna adicional al final de cada\n"
    << "      registro con un valor númerico (>=0) que indica el número de\n"
    << "      veces que la estrella artificial ha sido seleccionada aleato-\n"
    << "      riamente para realizar el proceso de dispersión.\n"
    << "    - La ruta puede ser relavita, aunque se recomienda encarecidamente\n"
    << "      que se utilice una ruta absoluta.\n"
    << "   */\n"
    << "  \"sort_file\" : \"sort_fC.txt\"\n"
    << " },\n"
    << "\n"
    << " //-------------------------------------------------------------------\n"
    << "  /** # RealStars: object. #\n"
    << "    - Parámetros propios de las estrellas reales.\n"
    << "   */\n"
    << " \"RealStars\" : {\n"
    << "\n"
    << "  /** # file: string. #\n"
    << "    - Ruta de dónde se encuentra el fichero que contiene el registro\n"
    << "      de las estrellas reales.\n"
    << "    - La ruta puede ser relavita, aunque se recomienda encarecidamente\n"
    << "      que se utilice una ruta absoluta.\n"
    << "   */\n"
    << "  \"file\" : \"fM.txt\",\n"
    << "\n"
    << "  /** # rows: int number. #\n"
    << "    - Número de filas que contiene el fichero.\n"
    << "   */\n"
    << "  \"rows\" : 10,\n"
    << "\n"
    << "  /** # columns: int number. #\n"
    << "    - Número de la columna mayor que se desean procesar del registro.\n"
    << "    - El programa tendrá en cuenta en cada registro desde la columna\n"
    << "      1 hasta la colunma indicada, incluyendo esta última.\n"
    << "   */\n"
    << "  \"columns\" : 6,\n"
    << "\n"
    << "  /** # index : array of strings. #\n"
    << "    - Expresión que determina el valor que tendrán los índices de las\n"
    << "      estrellas reales a partir de sus columnas.\n"
    << "    - Las columnas se expresarán con la letra 'c', seguido del número\n"
    << "      de columna, empezando la numeración en 1. Ej: c01, c1.\n"
    << "    - Están soportadas la mayoría de funciones matemáticas encontradas\n"
    << "      en: http://www.partow.net/programming/exprtk/\n"
    << "    - El número de elementos debe ser el mismo del campo epsilons.\n"
    << "    - No es necesario especificar siempre que el campo customized\n"
    << "      sea false; en cuyo caso, es como si estuviera definido de la\n"
    << "      siguiente forma:\n"
    << "        \"index\":[\"c1\",\"c1-c2\",\"c5\",\"c6\"]\n"
    << "   */\n"
    << "  \"index\" : [\n"
    << "   \"c1\",\n"
    << "   \"c1-c2\",\n"
    << "   \"c5\",\n"
    << "   \"c6\"\n"
    << "  ]\n"
    << " },\n"
    << "\n"
    << " //-------------------------------------------------------------------\n"
    << "  /** # DispersedStars: object. #\n"
    << "    - Parámetros propios de las estrellas dispersas.\n"
    << "   */\n"
    << " \"DispersedStars\" : {\n"
    << "\n"
    << "  /** # file: string. #\n"
    << "    - Ruta de dónde se desea almacenar el registro de estrellas\n"
    << "      dispersas que generará el programa.\n"
    << "    - La ruta puede ser relavita, aunque se recomienda encarecidamente\n"
    << "      que se utilice una ruta absoluta.\n"
    << "   */\n"
    << "  \"file\" : \"fOut.txt\","
    << "\n"
    << "  /** # columns: int number. #\n"
    << "    - Número de columnas que se desean que tenga el registro.\n"
    << "    - Actualmente este número debe ser dos unidades superior al mismo\n"
    << "      campo del objeto \"RealStars\".\n"
    << "   */\n"
    << "  \"columns\" : 8\n"
    << " }\n"
    << "\n"
    << " //-------------------------------------------------------------------\n"
    << "}\n";
  helpFile.flush();
  helpFile.close();
}
//-----------------------------------------------------------------------------
/**
 * Lee el fichero de parámetros y comrpueba la existencia de los ficheros
 * especificados
 * @param argc  Número de argumentos pasado al programa principal.
 * @param argv  Argumentos pasado al programa principal.
 */
inline void checkParameters(int& argc, const char**& argv) {
  std::string errors;
  std::exception excep;
  bool isOK(true), lastCheck(false);
  try {
    // Comprobar número de parámetros
    // ==============================
    if (argc != 2) {
      errors =   "- Error en el número de argumentos:\n";
      errors +=  " Uso:\n";
      errors +=  "   $ ./<exe> <fichero-parámetros.json>\n";
      throw excep;
    }
    // Comprobrar fichero con parámetros
    // =================================
    std::cout << "Cargar parámetros ...\n";
    std::cout.flush();
    std::ifstream jsonDocument;
    jsonDocument.open(argv[1], std::ifstream::in);
    if (!jsonDocument.is_open()) {
      errors = "- Error al abrir el fichero de parámetros : ";
      errors += argv[1];
      errors += "\n";
      throw excep;
    }
    Json::Value jsonRoot;
    Json::Reader reader(Json::Features::all());
    isOK &= lastCheck = reader.parse(jsonDocument, jsonRoot);
    if (!lastCheck) {
      errors = "- Error al analizar el fichero de parámetros:\n";
      errors += reader.getFormattedErrorMessages();
      throw excep;
    }
    // Opciones generales
    // ==================
    errors = "";
    isOK = true;
    lastCheck = false;
    Json::Value jsonGeneral = jsonRoot["General"];
    // 0. Elementos incluidos en el objeto.
    Json::Value jsonGeneral_minSetSize = jsonGeneral["minSetSize"];
    Json::Value jsonGeneral_maxSetSize = jsonGeneral["maxSetSize"];
    Json::Value jsonGeneral_maxValue   = jsonGeneral["maxValue"];
    Json::Value jsonGeneral_customized = jsonGeneral["customized"];
    Json::Value jsonGeneral_epsilons   = jsonGeneral["epsilons"];
    Json::Value jsonGeneral_log        = jsonGeneral["log"];
    // 1. Comprobar existencia del objeto y los elemtos.
    if (jsonGeneral.isNull()) {
      errors += "- Necesitas especificar el objeto General.\n";
      throw excep;
    }
    if (jsonGeneral_minSetSize.isNull()) {
      errors += "- Necesitas especificar el elemento General.minSetSize.\n";
      throw excep;
    }
    if (jsonGeneral_maxSetSize.isNull()) {
      errors += "- Necesitas especificar el elemento General.maxSetSize.\n";
      throw excep;
    }
    if (jsonGeneral_maxValue.isNull()) {
      errors += "- Necesitas especificar el elemento General.maxValue.\n";
      throw excep;
    }
    if (jsonGeneral_customized.isNull()) {
      errors += "- Necesitas especificar el elemento General.customized.\n";
      throw excep;
    }
    if (jsonGeneral_epsilons.isNull()) {
      errors += "- Necesitas especificar el elemento General.epsilons.\n";
      throw excep;
    }
    if (jsonGeneral_log.isNull()) {
      errors += "- Necesitas especificar el elemento General.log.\n";
      throw excep;
    }
    // 2. Comprobar tipos de datos.
    isOK &= lastCheck = jsonGeneral.isObject();
    if (!lastCheck) {
      errors += "- General debe ser un objeto.\n";
    }
    isOK &= lastCheck = jsonGeneral_minSetSize.isInt();
    if (!lastCheck) {
      errors += "- General.minSetSize debe ser un entero.\n";
    }
    isOK &= lastCheck = jsonGeneral_maxSetSize.isInt();
    if (!lastCheck) {
      errors += "- General.maxSetSize debe ser un entero.\n";
    }
    isOK &= lastCheck = jsonGeneral_maxValue.isDouble();
    if (!lastCheck) {
      errors += "- General.maxValue debe ser un número real.\n";
    }
    isOK &= lastCheck = jsonGeneral_customized.isBool();
    if (!lastCheck) {
      errors += "- General.customized debe ser un valor booleano.\n";
    }
    isOK &= lastCheck = jsonGeneral_epsilons.isArray();
    if (!lastCheck) {
      errors += "- General.epsilons debe ser un vector.\n";
    }
    isOK &= lastCheck = jsonGeneral_log.isString();
    if (!lastCheck) {
      errors += "- General.log debe ser una ruta de fichero.\n";
    }
    // 3. Obtener datos
    minSetStars = jsonGeneral_minSetSize.asInt();
    maxSetStars = jsonGeneral_maxSetSize.asInt();
    maxValue = jsonGeneral_maxValue.asFloat();
    customized = jsonGeneral_customized.asBool();
    maxIndex = jsonGeneral_epsilons.size();
    logName = jsonGeneral_log.asString();
    epsilonsConst = new Epsilon;
    for (unsigned int i(0); i < maxIndex; ++i) {
      isOK &= lastCheck = jsonGeneral_epsilons[i].isDouble();
      if (!lastCheck) {
        errors += "- El elemento ";
        errors += std::to_string(i+1);
        errors += " de General.epsilons debe ser un número real.\n";
      }
      epsilonsConst->index[i] = jsonGeneral_epsilons[i].asFloat();
    }
    // 4. Comprobar coherencia de los datos.
    isOK &= lastCheck = minSetStars > 0;
    if (!lastCheck) {
      errors += "- El valor General.minSetStars debe ser mayor que 0.\n";
    }
    isOK &= lastCheck = maxIndex > 0;
    if (!lastCheck) {
      errors += "- El vector General.epsilons debe tener al menos un elemento.\n";
    }
    isOK &= lastCheck = minSetStars <= maxSetStars;
    if (!lastCheck) {
      errors += "- El valor General.minSetStars no puede ser mayor ";
      errors += "que General.maxSetStars.\n";
    }
    if (!customized) {
      isOK &= lastCheck = maxIndex == 4;
      if (!lastCheck) {
        errors += "- Si General.customized es false, ";
        errors += "entonces General.epsilons debe tener cuatro elementos.\n";
      }
    }
    // 5. Lanzar excepción si hubo algún error.
    if (!isOK) {
      errors = "+ Error en el objeto General.\n" + errors;
      throw excep;
    }
    // Opciones de estrellas artificiales
    // ==================================
    errors = "";
    Json::Value jsonArtificial = jsonRoot["ArtificialStars"];
    // 0. Elementos incluidos en el objeto.
    Json::Value jsonArtificial_file        = jsonArtificial["file"];
    Json::Value jsonArtificial_rows        = jsonArtificial["rows"];
    Json::Value jsonArtificial_columns     = jsonArtificial["columns"];
    Json::Value jsonArtificial_index       = jsonArtificial["index"];
    Json::Value jsonArtificial_sort_file   = jsonArtificial["sort_file"];
    // 1. Comprobar existencia del objeto y los elemtos.
    if (jsonArtificial.isNull()) {
      errors += "- Necesitas especificar el objeto ArtificialStars.\n";
      throw excep;
    }
    if (jsonArtificial_file.isNull()) {
      errors += "- Necesitas especificar el elemento ArtificialStars.file.\n";
      throw excep;
    }
    if (jsonArtificial_sort_file.isNull()) {
      errors += "- Necesitas especificar el elemento ArtificialStars.sort_file.\n";
      throw excep;
    }
    if (jsonArtificial_rows.isNull()) {
      errors += "- Necesitas especificar el elemento ArtificialStars.rows.\n";
      throw excep;
    }
    if (jsonArtificial_columns.isNull()) {
      errors += "- Necesitas especificar el elemento ArtificialStars.columns.\n";
      throw excep;
    }
    // 2. Comprobar tipos de datos.
    isOK = lastCheck = jsonArtificial.isObject();
    if (!lastCheck) {
      errors += "- ArtificialStars debe ser un objeto.\n";
    }
    isOK &= lastCheck = jsonArtificial_file.isString();
    if (!lastCheck) {
      errors += "- ArtificialStars.file debe ser una ruta de fichero.\n";
    }
    isOK &= lastCheck = jsonArtificial_sort_file.isString();
    if (!lastCheck) {
      errors += "- ArtificialStars.sort_file debe ser una ruta de fichero.\n";
    }
    isOK &= lastCheck = jsonArtificial_rows.isInt();
    if (!lastCheck) {
      errors += "- ArtificialStars.rows debe ser un entero.\n";
    }
    isOK &= lastCheck = jsonArtificial_columns.isInt();
    if (!lastCheck) {
      errors += "- ArtificialStars.columns debe ser un entero.\n";
    }
    // 3. Obtener datos.
    artfName      = jsonArtificial_file.asString();
    artfSortName  = jsonArtificial_sort_file.asString();
    artfMaxRow    = jsonArtificial_rows.asInt();
    artfMaxColumn = jsonArtificial_columns.asInt();
    // 3.0 Comprobar coherencia de algunos datos
    isOK &= lastCheck = artfMaxRow > 0;
    if (!lastCheck) {
      errors += "- El valor ArtificialStars.artfMaxRow debe ser mayor que 0.\n";
    }
    isOK &= lastCheck = artfMaxColumn > 0;
    if (!lastCheck) {
      errors += "- El valor ArtificialStars.artfMaxColumn debe ser mayor que 0.\n";
    }
    if (customized) {
      // 3.1 Si las opciones personalizadas están activas,
      // 3.1.1 Comprobar existencia de index.
      if (jsonArtificial_index.isNull()) {
        errors += "- Necesitas especificar el elemento ArtificialStars.index.\n";
        throw excep;
      }
      // 3.1.2 Comprobar tipos de index.
      isOK &= lastCheck = jsonArtificial_index.isArray();
      if (!lastCheck) {
        errors += "- ArtificialStars.index debe ser un vector.\n";
      }
      // 3.1.3 Obtener datos de index y comprobar coherencia.
      isOK &= lastCheck = maxIndex == jsonArtificial_index.size();
      if (!lastCheck) {
        errors += "- El número de elementos de ArtificialStars.index ";
        errors += "y General.epsilons debe ser el mismo.\n";
      }
      artfIndexExpressions = lastCheck?
                             new std::string[maxIndex] :
                             NULL;
      for (unsigned int i(0); lastCheck and i < maxIndex; ++i) {
        isOK &= lastCheck = jsonArtificial_index[i].isString();
        if (!lastCheck) {
          errors += "- La expresión número ";
          errors += std::to_string(i+1);
          errors += " de ArtificialStars.index debe ser";
          errors += " una cadena de caracteres\n";
        }
        artfIndexExpressions[i] = jsonArtificial_index[i].asString();
        // 3.1.3.1 Pasar a minúsculas.
        std::transform(artfIndexExpressions[i].begin(),
                       artfIndexExpressions[i].end(),
                       artfIndexExpressions[i].begin(),
                       ::tolower);
        // 3.1.3.2 Comprobar que sea una expresión factible de evaluar.
        isOK &= lastCheck = ExpressionAnalyzer::isFactible(artfIndexExpressions[i],
                                                     artfMaxColumn,
                                                     std::string(argv[1]),
                                                     0);
        if (!lastCheck) {
          errors += "- La expresión (";
          errors += artfIndexExpressions[i];
          errors += ") de ArtificialStars.index no es factible.\n";
        }
      }
      // 3.1.3.3 Crear evaluador.
      artfEvaluador = new Evaluator(artfMaxColumn,
                                    maxIndex,
                                    artfIndexExpressions);
      // 3.1.3.4 Comprobar el análisis de la expresión.
      isOK &= lastCheck = artfEvaluador->ready();
      if (!lastCheck) {
        errors += "- Alguna de ArtificialStars.index no es factible.\n";
      }
    }
    // 3.2 Si las opciones personalizadas no están activas,
    else {
      // 3.2.1 Comprobar coherencia de datos.
      isOK &= lastCheck = artfMaxColumn >= 6;
      if (!lastCheck) {
        errors +=  "- Si General.customized es false, ";
        errors += "entonces ArtificialStars.columns debe tener ser >= 6.\n";
      }
    }
    // 4. Lanzar excepción si hubo algún error.
    if (!isOK) {
      errors = "+ Error en el objeto ArtificialStars.\n" + errors;
      throw excep;
    }
    // Opciones de estrellas reales
    // ============================
    errors = "";
    isOK = true;
    Json::Value jsonReal = jsonRoot["RealStars"];
    // 0. Elementos incluidos en el objeto.
    Json::Value jsonReal_file    = jsonReal["file"];
    Json::Value jsonReal_rows    = jsonReal["rows"];
    Json::Value jsonReal_columns = jsonReal["columns"];
    Json::Value jsonReal_index   = jsonReal["index"];
    // 1. Comprobar existencia del objeto y los elemtos.
    if (jsonReal.isNull()) {
      errors += "- Necesitas especificar el objeto RealStars.\n";
      throw excep;
    }
    if (jsonReal_file.isNull()) {
      errors += "- Necesitas especificar el elemento RealStars.file.\n";
      throw excep;
    }
    if (jsonReal_rows.isNull()) {
      errors += "- Necesitas especificar el elemento RealStars.rows.\n";
      throw excep;
    }
    if (jsonReal_columns.isNull()) {
      errors += "- Necesitas especificar el elemento RealStars.columns.\n";
      throw excep;
    }
    // 2. Comprobar tipos de datos.
    isOK &= lastCheck = jsonReal.isObject();
    if (!lastCheck) {
      errors += "- RealStars debe ser un objeto.\n";
    }
    isOK &= lastCheck = jsonReal_file.isString();
    if (!lastCheck) {
      errors += "- RealStars.file debe ser una ruta de fichero.\n";
    }
    isOK &= lastCheck = jsonReal_rows.isInt();
    if (!lastCheck) {
      errors += "- RealStars.rows debe ser un entero.\n";
    }
    isOK &= lastCheck = jsonReal_columns.isInt();
    if (!lastCheck) {
      errors += "- RealStars.columns debe ser un entero.\n";
    }
    // 3. Obtener datos.
    realName      = jsonReal_file.asString();
    realMaxRow    = jsonReal_rows.asInt();
    realMaxColumn = jsonReal_columns.asInt();
    // 3.0 Comprobar coherencia de algunos datos
    isOK &= lastCheck = realMaxRow > 0;
    if (!lastCheck) {
      errors += "- El valor RealStars.artfMaxRow debe ser mayor que 0.\n";
    }
    isOK &= lastCheck = realMaxColumn > 0;
    if (!lastCheck) {
      errors += "- El valor RealStars.artfMaxColumn debe ser mayor que 0.\n";
    }
    if (customized) {
      // 3.1 Si las opciones personalizadas están activas,
      // 3.1.1 Comprobar existencia de index.
      if (jsonReal_index.isNull()) {
        errors += "- Necesitas especificar el elemento RealStars.index.\n";
        throw excep;
      }
      // 3.1.2 Comprobar tipos de index.
      isOK &= lastCheck = jsonReal_index.isArray();
      if (!lastCheck) {
        errors += "- RealStars.index debe ser un vector.\n";
      }
      // 3.1.3 Obtener datos de index y comprobar coherencia.
      isOK &= maxIndex == jsonReal_index.size();
      if (!lastCheck) {
        errors += "- El número de elementos de RealStars.index ";
        errors += "y General.epsilons debe ser el mismo.\n";
      }
      realIndexExpressions = new std::string[maxIndex];
      for (unsigned int i(0); isOK and i < maxIndex; ++i) {
        isOK &= lastCheck = jsonReal_index[i].isString();
        if (!lastCheck) {
          errors += "- La expresión número ";
          errors += std::to_string(i+1);
          errors += " de RealStars.index debe ser";
          errors += " una cadena de caracteres\n";
        }
        realIndexExpressions[i] = jsonReal_index[i].asString();
        // 3.1.3.1 Pasar a minúsculas.
        std::transform(realIndexExpressions[i].begin(),
                       realIndexExpressions[i].end(),
                       realIndexExpressions[i].begin(),
                       ::tolower);
        // 3.1.3.2 Comprobar que sea una expresión factible de evaluar.
        isOK &= lastCheck = ExpressionAnalyzer::isFactible
                                  (realIndexExpressions[i],
                                   realMaxColumn,
                                   std::string(argv[1]),
                                   0);
        if (!lastCheck) {
          errors += "- La expresión (";
          errors += realIndexExpressions[i];
          errors += ") de RealStars.index no es factible.\n";
        }
      }
      // 3.1.3.3 Crear evaluador.
      realEvaluador = new Evaluator(realMaxColumn,
                                    maxIndex,
                                    realIndexExpressions);
      // 3.1.3.4 Comprobar el análisis de la expresión.
      isOK &= lastCheck = realEvaluador->ready();
      if (!lastCheck) {
        errors += "- Alguna de RealStars.index no es factible.\n";
      }
    }
    // 3.2 Si las opciones personalizadas no están activas,
    else {
      // 3.2.1 Comprobar coherencia de datos.
      isOK &= lastCheck = realMaxColumn >= 6;
      if (!lastCheck) {
        errors +=  "- Si General.customized es false, ";
        errors += "entonces RealStars.columns debe tener ser >= 6.\n";
      }
    }
    // 4. Lanzar excepción si hubo algún error.
    if (!isOK) {
      errors = "+ Error en el objeto RealStars.\n" + errors;
      throw excep;
    }
    // Opciones de estrellas dispersas
    // ===============================
    errors = "";
    isOK = true;
    Json::Value jsonDispersed = jsonRoot["DispersedStars"];
    // 0. Elementos incluidos en el objeto.
    Json::Value jsonDispersed_file    = jsonDispersed["file"];
    Json::Value jsonDispersed_columns = jsonDispersed["columns"];
    // 1. Comprobar existencia del objeto y los elemtos.
    if (jsonDispersed.isNull()) {
      errors += "- Necesitas especificar el objeto DispersedStars.\n";
      throw excep;
    }
    if (jsonDispersed_file.isNull()) {
      errors += "- Necesitas especificar el elemento DispersedStars.columns.\n";
      throw excep;
    }
    // 2. Comprobar tipos de datos.
    isOK &= lastCheck = jsonDispersed.isObject();
    if (!lastCheck) {
      errors += "- DispersedStars debe ser un objeto.\n";
    }
    isOK &= lastCheck = jsonDispersed_file.isString();
    if (!lastCheck) {
      errors += "- DispersedStars.file debe ser una ruta de fichero.\n";
    }
    isOK &= lastCheck = jsonDispersed_columns.isInt();
    if (!lastCheck) {
      errors += "- RealStars.columns debe ser un entero.\n";
    }
    // 3. Obtener datos.
    dispName      = jsonDispersed_file.asString();
    dispMaxColumn = jsonDispersed_columns.asInt();
    // 4. Comprobar coherencia de los datos.
    isOK &= lastCheck = realMaxColumn <= dispMaxColumn;
    if (!lastCheck) {
      errors += "- El valor DispersedStars.columns no puede ser menor ";
      errors += "que RealStars.columns.\n";
    }
    // 5. Lanzar excepción si hubo algún error.
    if (!isOK) {
      errors = "- Error en el objeto DispersedStars.\n" + errors;
      throw excep;
    }
    // Mostrar resultados obtenidos
    // ============================
    std::cout
      << "Parámetros cargados:\n"
      << " + General:----------------------\n"
      << "  - Opciones personalizadas:     " << std::boolalpha
                                             << customized << "\n"
      << "  - Número mínimo de estrellas:  " << minSetStars << "\n"
      << "  - Valor máximo:                " << maxValue << "\n"
      << "  - Número de índices:           " << maxIndex << "\n"
      << "  - Fichero de Log:              " << logName << "\n"
      << "  - Epsilons:                    [ ";
    unsigned int i(0);
    for (; i < maxIndex-1; ++i) {
      std::cout << epsilonsConst->index[i] << ", ";
    }
    std::cout << epsilonsConst->index[i] << " ]\n"
      << " + Estrellas artificiales:-------\n"
      << "  - Fichero datos (entrada):     " << artfName      << "\n"
      << "  - Filas:                       " << artfMaxRow    << "\n"
      << "  - Columnas                     " << artfMaxColumn << "\n"
      << "  - Índices:                     [ ";
    if (customized) {
      for (i = 0; i < maxIndex-1; ++i) {
        std::cout << artfIndexExpressions[i] << ", ";
      }
      std::cout << artfIndexExpressions[i];
    }
    else {
      std::cout << "c1, c1-c2, c5, c6";
    }
    std::cout << " ]\n"
      << "  - Fichero ordenado (salida):   " << artfSortName  << "\n"
      << " + Estrellas reales:-------------\n"
      << "  - Fichero:                     " << realName      << "\n"
      << "  - Filas:                       " << realMaxRow    << "\n"
      << "  - Columnas:                    " << realMaxColumn << "\n"
      << "  - Índices:                     [ ";
    if (customized) {
      for (i = 0; i < maxIndex-1; ++i) {
        std::cout << realIndexExpressions[i] << ", ";
      }
      std::cout << realIndexExpressions[i];
    }
    else {
      std::cout << "c1, c1-c2, c5, c6";
    }
    std::cout << " ]\n"
      << " + Estrellas dispersas:----------\n"
      << "  - Fichero:                     " << dispName  << "\n"
      << "  - Columnas:                    " << dispMaxColumn << "\n"
      << "\n"
      << "¿Estás conforme con los parámetros? (y/n) : ";
    std::cout.flush();
    char option;
    std::cin >> option;
    if (option == 'n' or option == 'N') {
      errors = "No estás conforme con los parámetros obtenidos\n";
      throw excep;
    }
    std::cout.flush();
    jsonDocument.close();
    // Comprobar existencia de los ficheros
    // ====================================
    errors = "";
    isOK = true;
    logFile.open(logName, std::ofstream::out | std::ofstream::trunc);
    realFile.open(realName, std::ifstream::in);
    artfFile.open(artfName, std::ifstream::in);
    artfSortFile.open(artfSortName, std::ofstream::out | std::ofstream::trunc);
    dispFile.open(dispName, std::ofstream::out | std::ofstream::trunc);
    if (!logFile) {
      errors = "- Error al crear el fichero de log: '"+logName+"'\n";
      isOK = false;
    } else {
      logFile
        << "//=============================================================\\\\\n"
        << "|| Puedes consultar los ficheros 'ExpressionAnalyzer.log' y/o  ||\n"
        << "|| 'StarAnalyzer.log' para un mayor detalle de los errores,    ||\n"
        << "|| siempre que hayan sido generados.                           ||\n"
        << "\\\\=============================================================//\n";
      logFile.flush();
    }
    if (!realFile) {
      errors += "- Error al abrir el fichero madre: '"+realName+"'\n";
      isOK = false;
    }
    if (!artfFile) {
      errors += "- Error al abrir el fichero crowding: '"+artfName+"'\n";
      isOK = false;
    }
    if (!artfSortFile) {
      errors += "- Error al abrir el fichero crowding: '"+artfSortName+"'\n";
      isOK = false;
    }
    if (!dispFile) {
      errors += "- Error al abrir el fichero de salida: '"+dispName+"'\n";
      isOK = false;
    }
    if (!isOK) {
      throw excep;
    }
    std::cout << " -> Ficheros abiertos correctamente.\n\n";
    std::cout.flush();
    // Comprobar número de líneas de los ficheros.
    // ===========================================
    errors = "";
    std::cout
      << "Comprobando el número de líneas/filas en los ficheros ...\n"
      << " NOTA:\n"
      << "  - Se asume que todas las líneas son del mismo tamaño.\n";
    std::cout.flush();
    unsigned int init, sizeLine, sizeFile, lines;
    std::string line;
    // 1.- Artificial Star File
    std::cout << " -> ArtificialStar.rows introducido: "<< artfMaxRow << "\n";
    std::cout.flush();
    init = artfFile.tellg();
    getline(artfFile, line);
    sizeLine = artfFile.tellg();
    artfFile.seekg (0, artfFile.end);
    sizeFile = artfFile.tellg();
    sizeFile++;
    lines = sizeFile/sizeLine;
    std::cout << " -> ArtificialStar.rows detectado: "<< lines << "\n";
    std::cout.flush();
    if (lines != artfMaxRow) {
      std::cout << "¿Deseas cambiar al valor detectado? (y/n) : ";
      std::cin >> option;
      if (option != 'n' and option != 'N') {
        artfMaxRow = lines;
      }
    }
    artfFile.seekg(init);
    // 2.- Real Star File
    std::cout << " -> RealStar.rows introducido: "<< realMaxRow << "\n";
    std::cout.flush();
    init = realFile.tellg();
    getline(realFile, line);
    sizeLine = realFile.tellg();
    realFile.seekg (0, realFile.end);
    sizeFile = realFile.tellg();
    sizeFile++;
    lines = sizeFile/sizeLine;
    std::cout << " -> RealStar.rows detectado: "<< lines << "\n";
    std::cout.flush();
    if (lines != realMaxRow) {
      std::cout << "¿Deseas cambiar al valor detectado? (y/n) : ";
      std::cin >> option;
      if (option != 'n' and option != 'N') {
        realMaxRow = lines;
      }
    }
    realFile.seekg(init);
  // Captura de excepción
  // ====================
  } catch (std::exception& e) {
    std::cerr
      << "<Error>: Ejecución abortada\n"
      << errors
      << e.what() << "\n"
      << "<Nota>: Quizás el fichero 'help-file.json' te sirva de ayuda.\n\n";
    std::cerr.flush();
    help();
    postProcess();
    delete[] start;
    start = NULL;
    delete interval;
    interval = NULL;
    exit(EXIT_FAILURE);
  }
}
//-----------------------------------------------------------------------------
/**
 * Asigna valores a la memoria reservada para las estrellas artificiales,
 * asumiendo que el usuario ha decidido ejecutar el programa sin características
 * personalizadas.
 *
 * @param line          Línea o registro en el que se representa la estrella.
 * @param star          Estrella artifical que se desea cargar.
 * @param indexValues   Vector lineal en memoria de tipo realNumber dónde se
 *                      almacenarán los valores que tendrán los índices.
 * @return  true        Si se ha podido cargar correcmante tanto las columnas
 *                      como los índices.
 *          false       En caso contrario.
 *
 * NOTAS:
 *  - Se asume como cierto lo siguiente:
 *   - artfMaxColumn, maxIndex, logFile, artfName, artfLines son variables
 *     globales.
 *   - artfMaxColumn == 6
 *   - maxIndex == 4
 *   - logFile is open.
 *
 */
inline bool loadNoCustomizedArtificialStars(const std::string &line,
                                            ArtificialStar *&star,
                                            realNumber *& indexValues) {
  std::istringstream iss(line);
  bool ok(true);
  // Rellenar columnas
  for (unsigned int column(0); ok and column < artfMaxColumn; ++column) {
    iss >> star->column[column];
    ok = (iss.good() or iss.eof());
  }
  if (!ok) {
    logFile <<"-"<<artfName<<"("<<artfLines<<")"
            <<": Estrella artifical contiene errores en las columnas.\n";
  }
  else {
    // Rellenar índices
    indexValues[0] = star->column[0];
    indexValues[1] = star->column[0] - star->column[1];
    indexValues[2] = star->column[4];
    indexValues[3] = star->column[5];
    // Comprobar validez del resultado
    ok &= !isnan(indexValues[0]) and !isinf(indexValues[0]);
    ok &= !isnan(indexValues[1]) and !isinf(indexValues[1]);
    ok &= !isnan(indexValues[2]) and !isinf(indexValues[2]);
    ok &= !isnan(indexValues[3]) and !isinf(indexValues[3]);
    if (!ok) {
      logFile << "-"<<realName<<"("<<realLines<<")"
              << ": Estrella artifical no ha producido resultados válidos "
              << "para los índices.\n";
    }
  }
  return ok;
}
//-----------------------------------------------------------------------------
/**
 * Asigna valores a la memoria reservada para las estrellas artificiales,
 * asumiendo que el usuario ha decidido ejecutar el programa con características
 * personalizadas.
 *
 * @param line          Línea o registro en el que se representa la estrella.
 * @param star          Estrella artifical que se desea cargar.
 * @param indexValues   Vector lineal en memoria de tipo realNumber dónde se
 *                      almacenarán los valores que tendrán los índices.
 * @return  true        Si se ha podido cargar correcmante tanto las columnas
 *                      como los índices.
 *          false       En caso contrario.
 *
 * NOTAS:
 *  - Se asume como cierto lo siguiente:
 *   - El puntero "artfEvaluador" fue inicializado previamente.
 *   - El método ready() ha devuelto true.
 *   - logFile is open.
 *
 */
inline bool loadCustomizedArtificialStars(const std::string &line,
                                            ArtificialStar *&star,
                                            realNumber *& indexValues) {
  bool ok(true);
  // Rellenar columnas
  ok &= StarAnalyzer::fillColumns(line,
                                  star->column,
                                  artfMaxColumn,
                                  artfName,
                                  star->id);
  if (!ok) {
    logFile <<"-"<<artfName<<"("<<artfLines<<")"
            <<": Estrella artifical contiene errores en las columnas.\n";
  } else {
    // Rellenar índices
    ok &= artfEvaluador->evaluate(star->column,
                                  indexValues);
    if (!ok) {
      logFile << "-"<<realName<<"("<<realLines<<")"
              << ": Estrella artifical no ha producido resultados válidos "
              << "para los índices.\n";
    }
  }
  return ok;
}
//-----------------------------------------------------------------------------
/**
 * Carga en memoria las estrellas artificiales y las añade al índice.
 */
inline void loadArtificialStars() {
  try {
    std::cout << " - Procesando estrellas artificiales ...\n";
    std::cout.flush();
    // Inicializar variables
    artfStar = new ArtificialStar[artfMaxRow];
    ArtificialStar *currentStar;
    std::string strLine;
    std::function<bool(const std::string &,
                       ArtificialStar *&,
                       realNumber *&)> fillAll;
    fillAll = customized?
              loadCustomizedArtificialStars :
              loadNoCustomizedArtificialStars;

    // Leer fichero y añadir valores los índices
    for (artfLines = 0;
        !artfFile.eof() and artfLines < artfMaxRow;
        artfLines++) {
      std::getline(artfFile, strLine);
      realNumber *artfIndexValues = new realNumber[maxIndex];
      if (!strLine.empty()) {
        currentStar = artfStar + artfLines;
        currentStar->id = artfLines + 1;
        if (!fillAll(strLine, currentStar, artfIndexValues)) {
          // Ocurrió algún error en las operacioens de rellenar.
          logFile <<"-"<<artfName<<"("<<artfLines<<")"
               <<": Estrella artifical contiene errores.\n";
        }
        else {
          // No ocurrió ningún error en las operaciones de rellenar.
          indexes->insertTree(artfIndexValues, currentStar);
        }
        /*
        // TEST -> DELETE BLOCK
        {
          //std::cout.precision(3);
          std::cout << currentStar->id << "\n";
          std::cout << "ArtificialStar.Columns:";
          for (unsigned int p = 0; p < artfMaxColumn; ++p) {
            std::cout << " " << std::fixed << currentStar->column[p];
          }
          std::cout << "\n";

          std::cout << "              .Index  :";
          for (unsigned int p = 0; p < maxIndex; ++p) {
            std::cout << " " << std::fixed << artfIndexValues[p];
          }
          std::cout << "\n";
        }
        */

      }
      delete[] artfIndexValues;
    }
    std::cout << " - Estrellas artificiales procesadas:\n"
              << "   -> " << artfLines << " líneas.\n";
    std::cout.flush();
  }
  catch (std::bad_alloc& ba) {
    std::cerr
      << "<Error>: Ejecución abortada\n"
      << " - Memoria RAM insuficente.\n"
      << " - Excepción bad_alloc capturada:\n"
      <<  ba.what()
      << '\n';
    std::cerr.flush();
    exit(EXIT_FAILURE);
  }
  catch (...) {
    std::cerr
      << "<Error>: Ejecución abortada\n"
      << " - Se ha producido un error en la función: loadArtificialStars()"
      << '\n';
    std::cerr.flush();
    exit(EXIT_FAILURE);
  }
}
//-----------------------------------------------------------------------------
/**
 * Reserva memoria para poder cargar el máximo número de estrellas reales
 * (realMaxRow). Si el estado actual del sistema (memoria RAM disponible) no
 * permite reservar el total de estrella a procesar, entonces se cogerá un
 * bloque para las estrellas reales sean procesadas por lotes.
 *
 * Esta función puede abortar la ejecución del programa si no existe memoria,
 * suficiente inicializar todos los elementos reservados de tipo RealStar y
 * DispersedStar
 *
 * NOTA:
 *  - get_temporary_buffer<T>(n): coge un bloque de memoria de tamaño n de tipo
 *  T, pero sin invocar a su constructor. La inicialización de los elementos es
 *  necesaria hacerla antes de utilizarlo, siempre en el momento de su uso se
 *  asuman unos valores por defecto. En nuestro caso, asumimos que los elementos
 *  de tipo RealStar tienen los punteros index y column inicializados, mientras
 *  que en los elementos de tipo DispersedStar asumimos que el puntero column
 *  fue inicializado.
 *
 */
inline void getMemoryToBatchProcess() {
  try {
    std::cout << " - Reservando memoria para procesado por lotes ...\n";
    std::cout.flush();
    // Inicializar variables
    bool ok(false);
    bufferMax = realMaxRow;
    bufferElements = 0;
    realBuffer = std::pair<RealStar*, std::ptrdiff_t>(NULL, 0);
    dispBuffer = std::pair<DispersedStar*, std::ptrdiff_t>(NULL, 0);
    // Reservar memoria.
    //  1.- Reservar la máxima cantidad de memoria disponible en el sistema,
    //      siempre que permita hacer la reserva del mismo tamaño para
    //      estrellas reales y dispersas.
    for ( ; bufferMax > 0 && !ok; ) {
      realBuffer = std::get_temporary_buffer<RealStar>(bufferMax);
      dispBuffer = std::get_temporary_buffer<DispersedStar>(bufferMax);
      ok = realBuffer.second == dispBuffer.second;
      if (ok) {
        break;
      }
      std::return_temporary_buffer(realBuffer.first);
      std::return_temporary_buffer(dispBuffer.first);
      bufferMax = (realBuffer.second + dispBuffer.second)/2;
    }
    //  2.- Comprobar si hay o no memoria suficiente para ambos vectores.
    if (!ok) {
      std::bad_alloc ba;
      throw ba;
    }
    //  3.- Obtener tamaño y punteros de la reserva de memoria.
    bufferMax = realBuffer.second;
    realStar = realBuffer.first;
    dispStar = dispBuffer.first;
    //  4.- Inicializar punteros internos de cada estructura de datos.
    RealStar      *currentRealStar(realStar);
    DispersedStar *currentDispStar(dispStar);
    for (unsigned int i(0); i < bufferMax; ++i) {
      currentRealStar->column = new realNumber[realMaxColumn];
      currentRealStar->index = new realNumber[maxIndex];
      currentRealStar++;
      currentDispStar->column = new realNumber[dispMaxColumn];
      currentDispStar++;
    }
    std::cout << " - Memoria reservada para el procesado por lotes.\n"
              << "   -> Las estrellas se procesarán en lotes de "
              << bufferMax << " unidades.\n";
    std::cout.flush();
  }
  catch (std::bad_alloc& ba) {
    std::cerr
      << "<Error>: Ejecución abortada\n"
      << " - Memoria RAM insuficente.\n"
      << " - Excepción bad_alloc capturada:\n"
      <<  ba.what()
      << '\n';
    std::cerr.flush();
    exit(EXIT_FAILURE);
  }
  catch (...) {
    std::cerr
      << "<Error>: Ejecución abortada\n"
      << " - Se ha producido un error en la función: getMemoryToBatchProcess()"
      << '\n';
    std::cerr.flush();
    exit(EXIT_FAILURE);
  }
}
//-----------------------------------------------------------------------------
/**
 * Asigna valores a la memoria reservada para las estrellas reales, asumiendo
 * que el usuario ha decidido ejecutar el programa sin características persona-
 * lizadas.
 *
 * @param   line  Línea o registro en el que se representa la estrella.
 * @param   star  Estrella reales que se desea cargar.
 * @return  true  Si se ha podido cargar correcmante tanto las columnas como los
 *                índices.
 *          false En caso contrario.
 *
 * NOTAS:
 *  - Se asume como cierto lo siguiente:
 *   - realMaxColumn, maxIndex, logFile, realName, realLines son variables
 *     globales.
 *   - realMaxColumn == 6
 *   - maxIndex == 4
 *   - logFile is open.
 *
 */
inline bool loadNoCustomizedRealStars(const std::string &line,
                                            RealStar   *&star) {
  std::istringstream iss(line);
  bool ok(true);
  // Rellenar columnas
  for (unsigned int column(0); ok and column < realMaxColumn; ++column) {
    iss >> star->column[column];
    ok = (iss.good() or iss.eof());
  }
  if (!ok) {
    logFile <<"-"<<realName<<"("<<realLines<<")"
            <<": Estrella real contiene errores en las columnas.\n";
  }
  else {
    // Rellenar índices
    star->index[0] = star->column[0];
    star->index[1] = star->column[0] - star->column[1];
    star->index[2] = star->column[4];
    star->index[3] = star->column[5];
    // Comprobar validez del resultado
    ok &= !isnan(star->index[0]) and !isinf(star->index[0]);
    ok &= !isnan(star->index[1]) and !isinf(star->index[1]);
    ok &= !isnan(star->index[2]) and !isinf(star->index[2]);
    ok &= !isnan(star->index[3]) and !isinf(star->index[3]);
    if (!ok) {
      logFile <<"-"<<realName<<"("<<realLines<<")"
              <<": Estrella real no ha producido resultados válidos "
              << "para los índices.\n";
    }
  }
  return ok;
}
//-----------------------------------------------------------------------------
/**
 * Asigna valores a la memoria reservada para las estrellas reales, asumiendo
 * que el usuario ha decidido ejecutar el programa con características persona-
 * lizadas.
 *
 * @param   line  Línea o registro en el que se representa la estrella.
 * @param   star  Estrella real que se desea cargar.
 * @return  true  Si se ha podido cargar correcmante tanto las columnas como los
 *                índices.
 *          false En caso contrario.
 *
 * NOTAS:
 *  -Se asume como cierto lo siguiente:
 *   - El puntero "realEvaluador" fue inicializado previamente.
 *   - El método ready() ha devuelto true.
 *   - logFile is open.
 *
 */
inline bool loadCustomizedRealStars(const std::string &line,
                                          RealStar   *&star) {
  bool ok(true);
  // Rellenar columnas
  ok &= StarAnalyzer::fillColumns(line,
                                  star->column,
                                  realMaxColumn,
                                  realName,
                                  star->id);
  if (!ok) {
    // Ocurrió algún error en las operacioens de rellenar columnas.
    logFile <<"-"<<realName<<"("<<realLines<<")"
            <<": Estrella real contiene errores en las columnas.\n";
  }
  else {
    // Rellenar índices
    ok &= realEvaluador->evaluate(star->column,
                                  star->index);
    if (!ok) {
      logFile <<"-"<<realName<<"("<<realLines<<")"
              <<": Estrella real ha producido un error al evaluar las "
              <<   "expresiones de los índices.\n";
    }
  }
  return ok;
}
//-----------------------------------------------------------------------------
/**
 * Carga en memoria los valores de un nuevo lote de estrellas reales.
 *
 * NOTAS:
 *  - Se asume como cierto lo siguiente:
 *   - 0 < bufferMax <= realMaxRow
 *   - realStar != NULL and size(realStar) == bufferMax
 *   - dispStar != NULL and size(dispStar) == bufferMax
 *   - realFile is open.
 *
 */
inline void loadRealStars() {
  std::cout << " - Cargando un lote de estrellas reales desde fichero ...\n";
  std::cout.flush();
  // Inicializar variables.
  RealStar *currentStar;
  std::string strLine;
  std::function<bool(const std::string &, RealStar *&)> fillAll;
  fillAll = customized?
            loadCustomizedRealStars :
            loadNoCustomizedRealStars;
  // Leer fichero y añadir valores los índices
  unsigned int i(0);
  for ( ;
        !realFile.eof() && i < bufferMax;
        i++, realLines++)
  {
    std::getline(realFile, strLine);
    if (!strLine.empty()) {
      currentStar = realStar + i;
      currentStar->id = realLines + 1;
      if (!fillAll(strLine, currentStar)) {
        // Ocurrió algún error en las operacioens de rellenar.
        i--; // Rellenar buffer con estrellas reales sin errores
      }
      else {
        // No ocurrió ningún error en las operaciones de rellenar.
        ;
      }
      // TEST -> DELETE BLOCK
      /*
      {
        std::cout << "RealStar.Columns:    ";
        for (unsigned int p = 0; p < realMaxColumn; ++p) {
          std::cout << " " << std::fixed << currentStar->column[p];
        }
        std::cout << "\n";

        std::cout << "RealStar.Index.(-):  ";
        for (unsigned int p = 0; p < maxIndex; ++p) {
          std::cout << " " << std::fixed << currentStar->index[p] - epsilonsConst->index[p];
        }
        std::cout << "\n";
      }
      */

    }
    else {
      // Línea vacía
      i--; // Rellenar buffer con estrellas reales
    }
  }
  bufferElements = i;
  std::cout << " - Lote de estrellas reales listo para ser procesado.\n";
  std::cout.flush();
}
//-----------------------------------------------------------------------------
/**
 * Prepocesado del algoritmo, que realiza las siguientes tareas:
 *  - Leer del fichero las estrellas artificiales/sintetizadas/crowding.
 *  - Crear una estructura indexada, que permita acelerar las operaciones de
 *    búsqueda utilizada en el algoritmo.
 *  - Reservar memoria para poder almacenar las estrellas reales/madre y las
 *    estrellas dispersas. Se intentará reservar el máximo de memoria posible
 *    para procesarlo en una única pasada del bucle principal de la función
 *    process, pero si no es posible reservar la memoria solicitada al sistema,
 *    se reservará una cantidad de memoria inferior intentando que sea lo más
 *    próxima a la solicitada para procesar las estrellas por lotes.
 *
 */
inline void preProcess() {
  startTime(start[1], "Iniciando fase de preprocesado ...");
  // Inicializar variables necesarias para esta fase
  indexes = new Index;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  // Cargar en memoria todas las estrellas artificiales.
  // 1.- Reservar memoria y añadir a la estructura dinámica (Árbol AVL)
  loadArtificialStars();
  // 2.- Finalizar de construir la estructura dinámica (Árbol AVL)
  /// indexes->buildTreeComplete();
  indexes->finishInsert();
  // Información de índices:
  std::cout << " - Información de ínices:\n";
  for (unsigned int i = 0; i < maxIndex; ++i) {
    std::cout << "   -> índice["<<i+1<<"] = " << counterIndexNodes[i]
              <<" valores diferentes\n";
  }
  // Cargar en memoria el máximo de estrellas reales posibles sin agotar la
  // memoria del sistema para procesarlas en lotes.
  getMemoryToBatchProcess();
  endTime(start[1], *interval, "Fase de preprocesado finalizada");
}
//-----------------------------------------------------------------------------
/**
 * Procesado del algoritmo, que realiza las siguiente tareas:
 *  - Procesado por lotes de las estrellas reales.
 *  - Apliacado el proceso de dispersión a cada una de las estrellas reales.
 *  - Volcado por lotes de las estrellas dispersas a fichero.
 *  - Volcado de la estructura ordenada (estrellas artificiales) a un fichero.
 */
inline
void process() {
  startTime(start[1], "Procesando estrellas reales ...");
  // Procesado por lotes
  realLines = 0;
  for (unsigned int realProcessed(0);
       realProcessed < realMaxRow and !realFile.eof();
       realProcessed += bufferElements)
    {
    // Cargar un lote de estrellas reales desde el fichero
    loadRealStars();
    // Procesado individual
    startTime(start[2], " - Procesando en paralelo ...");
    unsigned int i;
    /// OpenMP Application Program Interface Version 4.0 - July 2013
    #pragma omp parallel for default(shared), private(i)
    for (i = 0; i < bufferElements; ++i) {
      RealStar *real(realStar + i);
      DispersedStar *disp(dispStar + i);
      // Copiar datos a la estrella sintetizada
      for (unsigned int j(0); j < realMaxColumn; ++j) {
        disp->column[j] = real->column[j];
      }
      // Calcular rangos iniciales
      Epsilon *epsilon = new Epsilon(*epsilonsConst);
      FoundStars *foundStars = new FoundStars;
      unsigned int foundStarsSize(0);
      // Buscar estrellas artificiales con los rangos actuales
      unsigned int increase(1);
      for ( ; increase <= 2; ++increase) {
        /// indexes->findTree(foundStars, real, epsilon);
        indexes->findList(foundStars, real, epsilon);
        foundStarsSize = foundStars->size();
        if (minSetStars <= foundStarsSize) {
          break;
        }
        // Relajar rangos
        for (unsigned int j(0); j < maxIndex; ++j) {
          epsilon->index[j] += epsilon->index[j];
        }
      }
      if (minSetStars <= foundStarsSize) {
        // Elegir una estrella aleatoria
        ArtificialStar **foundStarsData = foundStars->data();
        ArtificialStar *randomStar = foundStarsData[random() % foundStarsSize];
        #pragma omp critical (increaseSelected)
        {
          randomStar->selected++;
        }
        // Comprobar que M1o y M2o son < maxValue
        if (randomStar->column[2] > maxValue
            or
            randomStar->column[3] > maxValue)
        {
          disp->column[0] = 99.999;
          disp->column[1] = 99.999;
        } else {
          disp->column[0] -= randomStar->column[0] - randomStar->column[2];
          disp->column[1] -= randomStar->column[1] - randomStar->column[3];
        }
        disp->column[dispMaxColumn-2] = increase;
        disp->column[dispMaxColumn-1] = foundStarsSize;
      }
      // Número mínimo de estrellas encontradas no satisfecho.
      else {
        disp->column[dispMaxColumn-2] = 0;
        disp->column[dispMaxColumn-1] = foundStarsSize;
      }
      delete epsilon;
      delete foundStars;
    }
    endTime(start[2], *interval, " - Procesamiento en paralelo finalizado");
    // Escribir resultados individualmente: Estrellas dispersas
    std::cout << " - Almacenar los resultados del proceso de dispersión ...\n";
    std::cout.flush();
    for (unsigned int i(0); i < bufferElements; ++i) {
      DispersedStar *disp(dispStar + i);
      for (unsigned int column(0); column < dispMaxColumn; ++column) {
        dispFile.width(10);
        dispFile << std::showpos << std::right << disp->column[column] << ' ';
      }
      dispFile << "\n";
      dispFile.flush();
    }
    std::cout << " - Almacenamiento de los resultados del proceso de dispersión finalizado.\n";
    std::cout.flush();
    // Escribir estrellas artificiales ordenadas
    std::cout << " - Almacenar las estrellas articiales ordenadas ...\n";
    std::cout.flush();
    artfSortFile << (*indexes);
    std::cout << " - Almacenamiento de las estrellas articiales ordenadas finalizado.\n";
    std::cout.flush();
  }
  endTime(start[1], *interval, "Estrellas reales procesadas:");
  std::cout << " -> " << realLines << " líneas.\n";
  std::cout.flush();
}
//-----------------------------------------------------------------------------
/**
 * Postprocesado del algoritmo: elimina la estructura indexada, utilizada para
 * las búsquedas en el algoritmo y cierra los ficheros abierto.
 */
inline void postProcess() {
  startTime(start[1], "Eliminando estructura...");
  if (epsilonsConst) {
    delete epsilonsConst;
    epsilonsConst = NULL;
  }
  if (artfIndexExpressions) {
    delete[] artfIndexExpressions;
    artfIndexExpressions = NULL;
  }
  if (artfEvaluador) {
    delete artfEvaluador;
    artfEvaluador = NULL;
  }
  if (artfStar) {
    delete[] artfStar;
    artfStar = NULL;
  }
  if (realIndexExpressions) {
    delete[] realIndexExpressions;
    realIndexExpressions = NULL;
  }
  if (realEvaluador) {
    delete realEvaluador;
    realEvaluador = NULL;
  }
  if (realStar) {
    for (unsigned int i(0); i < bufferMax; ++i) {
      delete[] realStar[i].index;
      realStar[i].index = NULL;
      delete[] realStar[i].column;
      realStar[i].column = NULL;
    }
    std::return_temporary_buffer(realBuffer.first);
    //delete[] realStar;
    realStar = NULL;
  }
  if (dispStar) {
    for (unsigned int i(0); i < bufferMax; ++i) {
      delete[] dispStar[i].column;
    }
    std::return_temporary_buffer(dispBuffer.first);
    //delete[] dispStar;
    dispStar = NULL;
  }
  if (counterIndexNodes) {
    delete[] counterIndexNodes;
    counterIndexNodes = NULL;
  }
  if (indexes) {
    delete indexes;
    indexes = NULL;
  }
  if (logFile.is_open()) {
    logFile.close();
  }
  if (realFile.is_open()) {
    realFile.close();
  }
  if (artfFile.is_open()) {
    artfFile.close();
  }
  if (artfSortFile.is_open()) {
    artfSortFile.close();
  }
  if (dispFile.is_open()) {
    dispFile.close();
  }
  endTime(start[1], *interval, "Estructura eliminanda.");
}
//==Main=======================================================================
/**
 * Función principal
 * @param argc  Número de argumentos.
 * @param argv  Argumentos.
 * @return      EXIT_SUCCESS, si la ejecución finalizó correctamente.
 *              EXIT_FAILURE, si hubo algún error en la ejecución.
 */
int main(int argc, const char *argv[]) {
  // Inicializar variables
  // - star: Variable para medir el tiempos de ejecución.
  //   - star[0]: Medir el tiempo de inicio y fin del programa.
  //   - star[1]: Medir el tiempo de bloques: preProcess, process y postProcess.
  //   - star[2]: Medir el tiempos de regiones paralelas
  start = new timePoint[3];
  // - interval: Variable para representar la medición de tiempos en minutos.
  interval = new intervalMin;
  // Comprobar parámetros
  checkParameters(argc, argv);
  // Inicio de medición de tiempo
  startTime(start[0], "Ejecución iniciada ...");
  // Preprocesado
  preProcess();
  // Algoritmo
  process();
  // Postprocesado
  postProcess();
  // Exit
  endTime(start[0], *interval, "Ejecución finalizada.");
  delete[] start;
  start = NULL;
  delete interval;
  interval = NULL;
  return EXIT_SUCCESS;
}
