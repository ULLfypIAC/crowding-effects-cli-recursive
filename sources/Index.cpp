/**
 * Index.cpp
 *
 * Fichero que define las implementaciones de los métodos de la clase índice
 * (Index).
 *
 * Esta clase es la encargada de almacenar de forma anidada las características
 * o índices de búsqueda. En primer lugar, se usa sobre una estructura de árbol
 * (Tree) que permite adaptarse sin problema a añadir nuevos elementos bajo
 * demanda. Una vez finalizada todas las inserciones se construye la estructura
 * de lista (List) que ofrece tiempos más reducidos en la operación de búsqueda.
 *
 */

//==Includes===================================================================
#include "Index.h"
#include <fstream>

//==Special members:[http://www.cplusplus.com/doc/tutorial/classes2/]===========
/**
 * Default constructor
 */
Index::Index() :
  topTreeIndex(new Tree(1)),
  topListIndex(NULL) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
Index::~Index() {
  if (topTreeIndex) {
    delete topTreeIndex;
    topTreeIndex = NULL;
  }
  if (topListIndex) {
    delete topListIndex;
    topListIndex = NULL;
  }
}

//==Public Methods=============================================================
/**
 * insertTree: permite realizar la inserción de nuevos valores en la estructura
 * de árbol. Una vez insertados los valores de todos los índices, se hace
 * referencia a la estrella que produjo dichos índices.
 * @param indexesValues Lista de valores a insertar en la estructura de árbol.
 * @param artfStar Estrella artificial que produjo los valores.
 */
void Index::insertTree(realNumber *&indexesValues, ArtificialStar *&artfStar) {
  Node *node;
  bool isNewNode = false;
  Tree *currentTree = topTreeIndex;
  for (unsigned int i(1); i <= maxIndex; ++i) {
    isNewNode = currentTree->insert(indexesValues[i-1], node);
    if (i == maxIndex) {                    // Último nivel
      break;
    }
    if (isNewNode) {                        // Nodo nuevo
      currentTree = new Tree(i+1);
      node->content = currentTree;
    } else {                                // Nodo existente
      currentTree = (Tree*) node->content;
    }
  }
  if (isNewNode) {
    FoundStars *vectorArtfStar = new FoundStars;
    vectorArtfStar->push_back(artfStar);
    node->content = vectorArtfStar;
  } else {
    // La estrellas crowding, localizadas en las líneas
    // ((ArtificialStar*) node->content)->id y artfStar->id, son iguales
    // std::cout << "estrella repetida : " << artfStar->id << "\n";
    FoundStars *content = (FoundStars*) node->content;
    content->push_back(artfStar);
  }
}
//-----------------------------------------------------------------------------
/**
 * finishInsert: determina el fin de la inserción en la estructura de árbol,
 * la transforma en la estructura de lista y elimina la estructura de árbol.
 */
void Index::finishInsert() {
  buildTreeComplete();
  if (topTreeIndex) {
    topListIndex = new List(topTreeIndex);
    delete topTreeIndex;
    topTreeIndex = NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * findTree: permite realizar búsquedas en la estructura de árbol.
 * @param foundStars Conjunto de estrellas que cumplen los parámetros. Argumento
 * de salida.
 * @param realStar Estrella real, de dicha estrella se tomarán los valores base
 * para realizar la búsqueda.
 * @param epsilon Conjunto de márgenes de errores que permiten calcular los
 * valores mínimo y máximo permitidos en las búsquedas.
 */
void Index::findTree(FoundStars *&foundStars,
                     RealStar *&realStar,
                     Epsilon *&epsilon) {
  if (!foundStars) {
    std::cerr << "Es necesario una instancia de 'foundStars'\n";
    return;
  }
  if (!realStar) {
    std::cerr << "Es necesario una instancia de 'realStar'\n";
    return;
  }
  if (!epsilon) {
    std::cerr << "Es necesario una instancia de 'epsilon'\n";
    return;
  }
  foundStars->clear();
  Range *range = new Range[maxIndex];
  for (unsigned int i(0); i < maxIndex; ++i) {
    range[i].min = realStar->index[i] - epsilon->index[i];
    range[i].max = realStar->index[i] + epsilon->index[i];
  }
  findRecursive(foundStars, topTreeIndex, range);
  delete[] range;
}
//-----------------------------------------------------------------------------
/**
 * findList: permite realizar búsquedas en la estructura de lista.
 * @param foundStars Conjunto de estrellas que cumplen los parámetros. Argumento
 * de salida.
 * @param realStar Estrella real, de dicha estrella se tomarán los valores base
 * para realizar la búsqueda.
 * @param epsilon Conjunto de márgenes de errores que permiten calcular los
 * valores mínimo y máximo permitidos en las búsquedas.
 */
void Index::findList(FoundStars *&foundStars,
                     RealStar *&realStar,
                     Epsilon *&epsilon) {
  if (!foundStars) {
    std::cerr << "Es necesario una instancia de 'foundStars'\n";
    return;
  }
  if (!realStar) {
    std::cerr << "Es necesario una instancia de 'realStar'\n";
    return;
  }
  if (!epsilon) {
    std::cerr << "Es necesario una instancia de 'epsilon'\n";
    return;
  }
  foundStars->clear();
  Range *range = new Range[maxIndex];
  for (unsigned int i(0); i < maxIndex; ++i) {
    range[i].min = realStar->index[i] - epsilon->index[i];
    range[i].max = realStar->index[i] + epsilon->index[i];
  }
  findRecursive(foundStars, topListIndex, range);
  delete[] range;
}
//-----------------------------------------------------------------------------
/**
 * buildTreeComplete: método que recorre la estructura de árbol de los valores
 * menores a los valores mayores profundizando en las estructuras anidadas.
 * Este método es una capa de abstracción de un método recursivo que permite al
 * usuario no preocuparse por la correcta invocación del método recursivo.
 */
void Index::buildTreeComplete() {
  buildCompleteRecursive(topTreeIndex);
}
//==Private Methods============================================================
/**
 *
 * @param foundStars
 * @param currentTree
 * @param range
 */
void Index::findRecursive(FoundStars *&foundStars,
                          Tree *&currentTree,
                          Range *&range) {
  unsigned int i(currentTree->depth);
  Node *it, *end;
  currentTree->findGreaterEqual(range[i-1].min, it);
  currentTree->findGreater(range[i-1].max, end);
  if (i == maxIndex) {
    FoundStars *vectorArtfStar;
    for (; it and it != end; it = it->next) {
      vectorArtfStar = (FoundStars*) it->content;
      foundStars->insert(foundStars->end(),
    		             vectorArtfStar->begin(),
						 vectorArtfStar->end());
    }
  } else {
    Tree *nextIndex;
    for (; it and it != end; it = it->next) {
      nextIndex = (Tree*) it->content;
      findRecursive(foundStars, nextIndex, range);
    }
  }
}
//-----------------------------------------------------------------------------
/**
 *
 * @param foundStars
 * @param currentList
 * @param range
 */
void Index::findRecursive (FoundStars *&foundStars,
                           List *&currentList,
                           Range *&range) {
  unsigned int i(currentList->depth);
  Element *it(NULL);
  unsigned int it_index(0), end_index(0);
  it_index = currentList->findGreaterEqual(range[i-1].min);
  end_index = currentList->findGreater(range[i-1].max);
  if (i == maxIndex) {
    FoundStars *vectorArtfStar(NULL);
    for ( it = currentList->at(it_index);
          it_index < currentList->size and it_index != end_index;
          it = currentList->at(++it_index)
        )
    {
      vectorArtfStar = (FoundStars*) it->content;
      foundStars->insert(foundStars->end(),
                         vectorArtfStar->begin(),
                         vectorArtfStar->end());
    }
  } else {
    List *nextIndex(NULL);
    for ( it = currentList->at(it_index);
          it_index < currentList->size and it_index != end_index;
          it = currentList->at(++it_index)
        )
    {
      nextIndex = (List*) it->content;
      findRecursive(foundStars, nextIndex, range);
    }
  }
  //*/
}
//-----------------------------------------------------------------------------
/**
 * buildCompleteRecursive: método que recorre la estructura de árbol de los
 * valores menores a los valores mayores profundizando en las estructuras
 * anidadas.
 * Este método es recursivo, que debe ser invocado de forma adecuada para
 * su correcto funcionamiento.
 * @param currentTree Árbol a explorar.
 */
void Index::buildCompleteRecursive(Tree *&currentTree) {
  currentTree->buildComplete();
  if (currentTree->depth < maxIndex) {
    Node *it(currentTree->first);
    Node *end(currentTree->last);
    if (end) {
      end = end->next;
      Tree * nextIndex;
      for (; it != end; it = it->next) {
        nextIndex = (Tree*) it->content;
        buildCompleteRecursive(nextIndex);
      }
    }
  }
}
//-----------------------------------------------------------------------------
//==Overloaded operators:[http://www.cplusplus.com/doc/tutorial/templates/]====
/**
 * Sobrecarga del operador del flujo estándar de salida para el índice.
 * Primero intenta enviar el contenido de la estructura de lista, pero si ésta
 * está vacía, entoces lo intenta con la estructura de árbol.
 * Invoca de forma adecuada métodos recursivos.
 * @param out FLujo estándar de salida.
 * @param index Índice.
 * @return FLujo estándar de salida (out).
 */
std::ostream& operator<<(std::ostream &out, const Index &index) {
  // Format out:[http://www.cplusplus.com/reference/ios/ios_base/flags/]
  //std::ios_base::fmtflags flags;
  //flags |= std::ios::showpoint;
  //flags |= std::ios::showpos;
  //flags |= std::ios::dec;
  //flags |= std::ios::scientific;
  //flags |= std::ios::right;
  //out.flags(flags);
  if (index.topListIndex) {
    List * currentList(index.topListIndex);
    index.ostreamRecursive(out, currentList);
  }
  else {
    if (index.topTreeIndex) {
      Tree * currentTree(index.topTreeIndex);
      index.ostreamRecursive(out, currentTree);
    }
  }
  return out;
}
//-----------------------------------------------------------------------------
/**
 * ostreamRecursive: método recursivo que permite añadir al flujo de salida
 * estándar la representación de la estructura de árbol del índice.
 * @param out FLujo estándar de salida.
 * @param currentTree Árbol que añadirá información al flujo estandar de salida.
 * @return FLujo estándar de salida (out)
 */
std::ostream& Index::ostreamRecursive(std::ostream& out,
                                      Tree *& currentTree) const {
  Node *it(currentTree->first);
  Node *end(currentTree->last);
  if (end) {
    end = end->next;
    if (currentTree->depth < maxIndex) {
      Tree * nextIndex;
      for (; it != end; it = it->next) {
        nextIndex = (Tree*) it->content;
        ostreamRecursive(out, nextIndex);
      }
    }
    else {
      FoundStars *vectorArtfStars;
      ArtificialStar *artfStar;
      for (; it != end; it = it->next) {
        vectorArtfStars = (FoundStars*) it->content;
        for (auto itV = vectorArtfStars->begin(); itV != vectorArtfStars->end(); ++itV) {
          artfStar = (*itV);
          for (unsigned int i(0); i <artfMaxColumn; ++i) {
            out.fill(' ');
            out.width(15);
            out << (artfStar->column[i]);
          }
          out.fill(' ');
          out.width(15);
          out << (artfStar->selected);
          out << "\n";
          out.flush();
        }
      }
    }
  }
  return out;
}
//-----------------------------------------------------------------------------
/**
 * ostreamRecursive: método recursivo que permite añadir al flujo de salida
 * estándar la representación de la estructura de lista del índice.
 * @param out FLujo estándar de salida.
 * @param currentList Lista que añadirá información al flujo estandar de salida.
 * @return FLujo estándar de salida (out)
 */
std::ostream& Index::ostreamRecursive(std::ostream& out,
                                      List *& currentList) const {
  Element *it(currentList->first());
  Element *end(currentList->last());
  end++;
  if (currentList->depth < maxIndex) {
    List * nextIndex;
    for (; it != end; it++) {
      nextIndex = (List*) it->content;
      ostreamRecursive(out, nextIndex);
    }
  }
  else {
    FoundStars *vectorArtfStars;
    ArtificialStar *artfStar;
    for (; it != end; it++) {
      vectorArtfStars = (FoundStars*) it->content;
      for (auto itV = vectorArtfStars->begin(); itV != vectorArtfStars->end(); ++itV) {
        artfStar = (*itV);
        out << (*artfStar);
        out.flush();
      }
    }
  }
  return out;
}
//-----------------------------------------------------------------------------
