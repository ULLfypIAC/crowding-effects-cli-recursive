/**
 * traits.cpp
 *
 * Fichero que define los métodos de las clases base, las fucniones de
 * comparación de números reales y sobrecarga de los operadores del flujo
 * estándar de salida.
 */

//==Includes===================================================================
#include "traits.h"
// Uncomment NDEBUG to disable assert()
// [http://en.cppreference.com/w/cpp/error/assert]
// #define NDEBUG
#include <cassert>
#include <cstdlib>
#include <cmath>

//==Classes====================================================================
//-----------------------------------------------------------------------------
/**
 * Default constructor
 */
_Epsilon::_Epsilon() :
    index(new realNumber[maxIndex]) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
_Epsilon::~_Epsilon() {
  this->destroy();
}
//-----------------------------------------------------------------------------
/**
 * Copy constructor:
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *
 *  - Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar = foo;       // copy constructor
 *
 * @param other
 */
_Epsilon::_Epsilon(const _Epsilon& other) :
    // Paso 2.a
    index(new realNumber[maxIndex])
{
  // Paso 2.b
  for (unsigned int i(0); i < maxIndex; ++i) {
    index[i] = other.index[i];
  }
}
//-----------------------------------------------------------------------------
/**
 * Copy assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar los punteros poniendolos a NULL.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *    4. return *this
 *
 *  -Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar;             // default constructor
 *    foo = bar;               // copy assignment
 *
 * @param other
 * @return
 */
_Epsilon& _Epsilon::operator=(const _Epsilon& other)
{
  // Paso 1
  this->destroy();
  // Paso 3
  index = new realNumber[maxIndex];
  for (unsigned int i(0); i < maxIndex; ++i) {
    index[i] = other.index[i];
  }
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Move constructor
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente un objeto que no se conservará porque es un rvalue.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *
 *  - Ejemplo:
 *     MyClass fn();            // function returning a MyClass object
 *     MyClass foo = fn();      // move constructor
 *
 * @param other
 */
_Epsilon::_Epsilon(_Epsilon&& other) :
    // Paso 2.a
    index(other.index)
{
  // Paso 2.b
  other.index = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Move assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente un objeto que no se conservará porque es un
 *  rvalue.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar (poner a NULL) los punteros.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *    4. return *this
 *
 *  - Ejemplo:
 *     MyClass foo;             // default constructor
 *     foo = MyClass();         // move assignment
 *
 * @param other
 * @return
 */
_Epsilon& _Epsilon::operator=(_Epsilon&& other)
{
  // Paso 1
  this->destroy();
  // Paso 3
  index = other.index;
  other.index = NULL;
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Destruye el objeto, eliminando la memoria reservada a la que referencia y
 * dereferenciando los punteros.
 */
void _Epsilon::destroy() {
  if (index) {
    delete [] index;
    index = NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Default constructor
 */
_ArtificialStar::_ArtificialStar() :
    id(0),
    selected(0),
    column(new realNumber[artfMaxColumn]) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
_ArtificialStar::~_ArtificialStar() {
  this->destroy();
}
//-----------------------------------------------------------------------------
/**
 * Copy constructor:
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *
 *  - Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar = foo;       // copy constructor
 *
 * @param other
 */
_ArtificialStar::_ArtificialStar(const _ArtificialStar& other) :
    // Paso 1
    id(other.id),
    selected(other.selected),
    // Paso 2.a
    column(new realNumber[artfMaxColumn])
{
  // Paso 2.b
  for (unsigned int i(0); i < artfMaxColumn; ++i) {
    column[i] = other.column[i];
  }
}
//-----------------------------------------------------------------------------
/**
 * Copy assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar los punteros poniendolos a NULL.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *    4. return *this
 *
 *  -Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar;             // default constructor
 *    foo = bar;               // copy assignment
 *
 * @param other
 * @return
 */
_ArtificialStar& _ArtificialStar::operator=(const _ArtificialStar& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  selected = other.selected;
  // Paso 3
  column = new realNumber[artfMaxColumn];
  for (unsigned int i(0); i < artfMaxColumn; ++i) {
    column[i] = other.column[i];
  }
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Move constructor
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente un objeto que no se conservará porque es un rvalue.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *
 *  - Ejemplo:
 *     MyClass fn();            // function returning a MyClass object
 *     MyClass foo = fn();      // move constructor
 *
 * @param other
 */
_ArtificialStar::_ArtificialStar(_ArtificialStar&& other) :
    // Paso 1
    id(other.id),
    selected(other.selected),
    // Paso 2.a
    column(other.column)
{
  // Paso 2.b
  other.column = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Move assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente un objeto que no se conservará porque es un
 *  rvalue.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar (poner a NULL) los punteros.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *    4. return *this
 *
 *  - Ejemplo:
 *     MyClass foo;             // default constructor
 *     foo = MyClass();         // move assignment
 *
 * @param other
 * @return
 */
_ArtificialStar& _ArtificialStar::operator=(_ArtificialStar&& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  selected = other.selected;
  // Paso 3
  column = other.column;
  other.column = NULL;
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Destruye el objeto, eliminando la memoria reservada a la que referencia y
 * dereferenciando los punteros.
 */
void _ArtificialStar::destroy()
{
  if (column) {
    delete[] column;
    column = NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Default constructor
 */
_RealStar::_RealStar() :
    id(0),
    index(new realNumber[maxIndex]),
    column(new realNumber[realMaxColumn]) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
_RealStar::~_RealStar() {
  this->destroy();
}
//-----------------------------------------------------------------------------
/**
 * Copy constructor:
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *
 *  - Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar = foo;       // copy constructor
 *
 * @param other
 */
_RealStar::_RealStar(const _RealStar& other) :
    // Paso 1
    id(other.id),
    // Paso 2.a
    index(new realNumber[maxIndex]),
    column(new realNumber[realMaxColumn])
{
  // Paso 2.b
  for (unsigned int i(0); i < maxIndex; ++i) {
    index[i] = other.index[i];
  }
  for (unsigned int i(0); i < realMaxColumn; ++i) {
    column[i] = other.column[i];
  }
}
//-----------------------------------------------------------------------------
/**
 * Copy assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar los punteros poniendolos a NULL.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *    4. return *this
 *
 *  -Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar;             // default constructor
 *    foo = bar;               // copy assignment
 *
 * @param other
 * @return
 */
_RealStar& _RealStar::operator=(const _RealStar& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  // Paso 3.a
  index = new realNumber[maxIndex];
  column = new realNumber[realMaxColumn];
  // Paso 3.b
  for (unsigned int i(0); i < maxIndex; ++i) {
    index[i] = other.index[i];
  }
  for (unsigned int i(0); i < realMaxColumn; ++i) {
    column[i] = other.column[i];
  }
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Move constructor
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente un objeto que no se conservará porque es un rvalue.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *
 *  - Ejemplo:
 *     MyClass fn();            // function returning a MyClass object
 *     MyClass foo = fn();      // move constructor
 *
 * @param other
 */
_RealStar::_RealStar(_RealStar&& other) :
    // Paso 1
    id(other.id),
    // Paso 2.a
    index(other.index),
    column(other.column)
{
  // Paso 2.b
  other.index = NULL;
  other.column = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Move assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente un objeto que no se conservará porque es un
 *  rvalue.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar (poner a NULL) los punteros.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *    4. return *this
 *
 *  - Ejemplo:
 *     MyClass foo;             // default constructor
 *     foo = MyClass();         // move assignment
 *
 * @param other
 * @return
 */
_RealStar& _RealStar::operator=(_RealStar&& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  // Paso 3.a
  index = other.index;
  column = other.column;
  // Paso 3.b
  other.index = NULL;
  other.column = NULL;
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Destruye el objeto, eliminando la memoria reservada a la que referencia y
 * dereferenciando los punteros.
 */
void _RealStar::destroy()
{
  if (index) {
    delete[] index;
    index = NULL;
  }
  if (column) {
    delete[] column;
    column = NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Default constructor
 */
_DispersedStar::_DispersedStar() :
    id(0),
    column(new realNumber[dispMaxColumn]) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
_DispersedStar::~_DispersedStar() {
  this->destroy();
}
//-----------------------------------------------------------------------------
/**
 * Copy constructor:
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *
 *  - Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar = foo;       // copy constructor
 *
 * @param other
 */
_DispersedStar::_DispersedStar(const _DispersedStar& other) :
    // Paso 1
    id(other.id),
    // Paso 2.a
    column(new realNumber[dispMaxColumn])
{
  // Paso 2.b
  for (unsigned int i(0); i < dispMaxColumn; ++i) {
    column[i] = other.column[i];
  }
}
//-----------------------------------------------------------------------------
/**
 * Copy assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar los punteros poniendolos a NULL.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *    4. return *this
 *
 *  -Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar;             // default constructor
 *    foo = bar;               // copy assignment
 *
 * @param other
 * @return
 */
_DispersedStar& _DispersedStar::operator=(const _DispersedStar& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  // Paso 3
  column = new realNumber[dispMaxColumn];
  for (unsigned int i(0); i < dispMaxColumn; ++i) {
    column[i] = other.column[i];
  }
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Move constructor
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente un objeto que no se conservará porque es un rvalue.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *
 *  - Ejemplo:
 *     MyClass fn();            // function returning a MyClass object
 *     MyClass foo = fn();      // move constructor
 *
 * @param other
 */
_DispersedStar::_DispersedStar(_DispersedStar&& other) :
    // Paso 1
    id(other.id),
    // Paso 2.a
    column(other.column)
{
  // Paso 2.b
  other.column = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Move assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente un objeto que no se conservará porque es un
 *  rvalue.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar (poner a NULL) los punteros.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *    4. return *this
 *
 *  - Ejemplo:
 *     MyClass foo;             // default constructor
 *     foo = MyClass();         // move assignment
 *
 * @param other
 * @return
 */
_DispersedStar& _DispersedStar::operator=(_DispersedStar&& other)
{
  // Paso 1
  this->destroy();
  // Paso 2
  id = other.id;
  // Paso 3
  column = other.column;
  other.column = NULL;
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Destruir las columnas de las estrellas dispersas.
 */
void _DispersedStar::destroy()
{
  if (column) {
    delete[] column;
    column = NULL;
  }
}
//-----------------------------------------------------------------------------
//==Functions==================================================================
/**
 * Comparing floating point numbers as integers numbers
 *
 * Source:
 * - http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
 * - ftp://ftp.cygnus-software.com/pub/comparecode.zip
 *
 * @param A
 * @param B
 * @param epsilon
 * @return <0: If A  <  B
 *          0: If A ==  B
 *         >0: If A  >  B
 */
signed char compareAsInt(const realNumber& A,
                         const realNumber& B,
                         realNumber epsilon) {
    // Make sure float point numbers can use integer representation.
    assert(sizeof(realNumber) == sizeof(int));
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    int maxUlps = *(int*)&epsilon;
    assert(maxUlps > 0 && maxUlps < 0x400000); // 0x400000 = 4*1024*1024
    int aInt = *(int*)&A;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x80000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int bInt = *(int*)&B;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;
    int intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return 0;
    if (A > B)
      return +1;
    else
      return -1;
}
//-----------------------------------------------------------------------------
/**
 * Comparing floating point numbers as integers numbers
 *
 * Source:
 * - http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
 * - ftp://ftp.cygnus-software.com/pub/comparecode.zip
 *
 * @param A
 * @param B
 * @param operation
 * @param epsilon
 * @return <0: If A  <  B
 *          0: If A ==  B
 *         >0: If A  >  B
 */
bool compareNumbers(const realNumber& A,
                          std::string operation,
                    const realNumber& B,
                          realNumber  error) {
  int result = -2;
  // /*
  if (A == B) {
    result = 0;
    goto Operation;
  }
  if (fabs(A - B) < error) {
      result = 0;
  }
  // */

  /*
  if (A == B) {
    result = 0;
    goto Operation;
  }
  float relativeError;
  if (fabs(B) > fabs(A))
      relativeError = fabs((A - B) / B);
  else
      relativeError = fabs((A - B) / A);
  if (relativeError <= error) {
      result = 0;
  }
  // */

  /*
  // Make sure float point numbers can use integer representation.
  assert(sizeof(realNumber) == sizeof(int));
  // Make sure maxUlps is non-negative and small enough that the
  // default NAN won't compare as equal to anything.
  int maxUlps = *(int*)&error;
  std::cout << "error = " << error << "\n";
  std::cout << "maxUlps = " << maxUlps << "\n";
  std::cout << "4 * 1024 * 1024 = " << 4 * 1024 * 1024 << "\n";
  std::cout << "0x400000 = " << 0x400000 << "\n";
  assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024); // 0x400000 = 4*1024*1024
  int aInt = *(int*)&A;
  // Make aInt lexicographically ordered as a twos-complement int
  if (aInt < 0)
      aInt = 0x80000000 - aInt;
  // Make bInt lexicographically ordered as a twos-complement int
  int bInt = *(int*)&B;
  if (bInt < 0)
      bInt = 0x80000000 - bInt;
  int intDiff = abs(aInt - bInt);
  if (intDiff <= maxUlps) {
    result =  0;
  }
  // */
  else {
    if (A > B) {
     result = +1;
    }
    else {
     result = -1;
    }
  }
  Operation:
  if (operation == "<") {
    return (result == -1);
  }
  if (operation == "<=") {
    return (result == -1 or result == 0);
  }
  if (operation == ">") {
    return (result == +1);
  }
  if (operation == ">=") {
    return (result == +1 or result == 0);
  }
  if (operation == "==") {
    return (result == 0);
  }
  if (operation == "!=") {
    return (result != 0);
  }
  std::cerr << "La operación: '"<< operation <<"' no está soportada\n";
  exit(EXIT_FAILURE);
  return false;
}
//-----------------------------------------------------------------------------
/**
 * Sobrecarga del operador del flujo estándar de salida para las estrellas
 * artificiales.
 * @param out FLujo estándar de salida.
 * @param artfStar Estrella artificial.
 * @return out Flujo estándar de salida (out).
 */
std::ostream& operator<<(std::ostream& out, const ArtificialStar& artfStar) {
  std::ios_base::fmtflags flags;
  flags |= std::ios::showpoint;
  flags |= std::ios::showpos;
  flags |= std::ios::dec;
  flags |= std::ios::scientific;
  flags |= std::ios::right;
  out.flags(flags);
  for (unsigned int i = 0; i < artfMaxColumn; ++i) {
    out << " ";
    out.fill(' ');
    out.width(13);
    out << artfStar.column[i];
  }
  out << " ";
  out.fill(' ');
  out.width(13);
  out << artfStar.selected << "\n";
  return out;
}
//-----------------------------------------------------------------------------
/**
 * Sobrecarga del operador del flujo estándar de salida para las estrellas
 * reales.
 * @param out FLujo estándar de salida.
 * @param realStar Estrella real.
 * @return FLujo estándar de salida (out).
 */
std::ostream& operator<<(std::ostream& out, const RealStar& realStar) {
  std::ios_base::fmtflags flags;
  flags |= std::ios::showpoint;
  flags |= std::ios::showpos;
  flags |= std::ios::dec;
  flags |= std::ios::scientific;
  flags |= std::ios::right;
  out.flags(flags);
  for (unsigned int i = 0; i < artfMaxColumn; ++i) {
    out << " ";
    out.fill(' ');
    out.width(13);
    out << realStar.column[i];
  }
  out << "\n";
  return out;
}
//-----------------------------------------------------------------------------
