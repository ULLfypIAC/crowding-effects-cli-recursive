/**
 * Element.cpp
 *
 * Fichero que define los métodos de la clase elemento (Element).
 *
 */

//==Includes===================================================================
#include "Element.h"
#include "List.h"

//==Special members:[http://www.cplusplus.com/doc/tutorial/classes2/]==========
/**
 * Default constructor
 */
Element::Element() :
    value(0),
    depth(1),
    content(NULL)
{
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
Element::~Element()
{
  this->destroy();
}
//-----------------------------------------------------------------------------
/**
 * Copy constructor:
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *
 *  - Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar = foo;       // copy constructor
 *
 * @param other
 */
Element::Element(const Element& other) :
    // Paso 1
    value(other.value),
    depth(other.depth),
    content(NULL)
{
    // Paso 2
    if (depth < maxIndex) {
      content = new List(*((List *) other.content));
    } else {
      content = new FoundStars(*((FoundStars *) other.content));
    }
}
//-----------------------------------------------------------------------------
/**
 * Copy assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente otro objeto ya construido, que se conservará.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar los punteros poniendolos a NULL.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Evitar que los punteros referencien a las mismas direcciones de
 *       memoria porque se desea hacer una copia:
 *     a. Reservar nueva memoria para los punteros de this.
 *     b. Asegurarnos que el contenido de la memoria reservada para this, sean
 *        una copia de los punteros a los que referencia other, evitando
 *        nuevamente si existen punteros en estas estructuras, referencien a
 *        las mismas direcciones de memoria.
 *    4. return *this
 *
 *  -Ejemplo:
 *    MyClass foo;             // default constructor
 *    MyClass bar;             // default constructor
 *    foo = bar;               // copy assignment
 *
 * @param other
 * @return
 */
Element& Element::operator=(const Element& other)
{
  // Paso 1
  this->destroy();
  content = NULL;
  // Paso 2
  value = other.value;
  depth = other.depth;
  // Paso 3
  if (depth < maxIndex) {
    content = new List(*((List *) other.content));
  } else {
    content = new FoundStars(*((FoundStars *) other.content));
  }
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Move constructor
 *  - El objeto no se ha construido o se está declarando: se desea inicializar
 *  tomando como fuente un objeto que no se conservará porque es un rvalue.
 *
 *  - Pasos:
 *    1. Copiar el contenido que no sean punteros.
 *    2. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *
 *  - Ejemplo:
 *     MyClass fn();            // function returning a MyClass object
 *     MyClass foo = fn();      // move constructor
 *
 * @param other
 */
Element::Element(Element&& other) :
    // Paso 1
    value(other.value),
    depth(other.depth),
    // Paso 2.a
    content(other.content)
{
  // Paso 2.b
  other.content = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Move assignment
 *  - El objeto ya está construido y ha sido declarado previamente: se desea
 *  igualar tomando como fuente un objeto que no se conservará porque es un
 *  rvalue.
 *
 *  - Pasos:
 *    1. Destruir los objetos a los que se referencia this:
 *     a. Destruir la memoria reservada.
 *     b. Dereferenciar (poner a NULL) los punteros.
 *    2. Copiar el contenido que no sean punteros.
 *    3. Referenciar los punteros a la misma dirección de memoria, porque el
 *       contenido de other se destruirá por ser un rvalue y se desean mover
 *       los datos.
 *     a. Hacer que los punteros de this referencien a las mismas direcciones
 *        de memoria que los punteros de other.
 *     b. Dereferenciar (poner a NULL) los punteros de other para evitar que
 *        la memoria reservada por other y ahora referenciada por this, sea
 *        destruida cuando other sea destruido.
 *    4. return *this
 *
 *  - Ejemplo:
 *     MyClass foo;             // default constructor
 *     foo = MyClass();         // move assignment
 *
 * @param other
 * @return
 */
Element& Element::operator=(Element&& other)
{
  // Paso 1
  this->destroy();
  this->content = NULL;
  // Paso 2
  this->value = other.value;
  this->depth = other.depth;
  // Paso 3
  this->content = other.content;
  other.content = NULL;
  // Paso 4
  return *this;
}
//-----------------------------------------------------------------------------
/**
 * Custom constructor:
 *  - Construcción de un objeto Element a partir de un valor de tipo
 *  realNumber.
 *
 * @param value
 */
Element::Element(const realNumber& value) :
    value(value),
    depth(1),
    content(NULL)
{
}
//-----------------------------------------------------------------------------
/**
 * Custom constructor:
 *  - Construcción de un obejto Element a partir de un puntero a Node.
 *
 *  - En el caso de que el puntero al objeto Node esté en el último nivel del
 *  anidamiento de índices, dereferencia (pone a NULL) del atributo content.
 *  Esto es debido a que la estructura en forma de árbol AVL, se eliminará en
 *  favor, de la rapidez que brinda una estructura indexada frente a una
 *  estructura en forma de árbol para realizar las búsquedas y el último nivel
 *  de índices siempre apuntará a los datos y no se desea que los datos sean
 *  eliminados cuando el índice en forma de árbol sea eliminado.
 *
 *  - Muy similar al método void Element::copyFromNode(Node *&node)
 *
 * @param node  Nodo que contiene la información parcial de un índice.
 */
Element::Element(Node*& node) :
    value(0),
    depth(1),
    content(NULL)
{
  if (node) {
    value = node->value;
    depth = node->depth;
    if (node->content) {
      if (depth < maxIndex) {
        Tree *tree = (Tree*) node->content;
        List *list = new List(tree);
        content = list;
      }
      else {
        content = node->content;
        node->content = NULL;
      }
    }
  }
}
//==Public Methods=============================================================
/**
 * - Construye un objeto Element desde un puntero a un objeto Node.
 *
 * - En el caso de que el puntero al objeto Node esté en el último nivel del
 * anidamiento de índices, dereferencia (pone a NULL) del atributo content.
 * Esto es debido a que la estructura en forma de árbol AVL, se eliminará en
 * favor, de la rapidez que brinda una estructura indexada frente a una
 * estructura en forma de árbol para realizar las búsquedas y el último nivel
 * de índices siempre apuntará a los datos y no se desea que los datos sean
 * eliminados cuando el índice en forma de árbol sea eliminado.
 *
 * - Muy similar al constructor Element::Element(Node*& node)
 *
 * @param node  Nodo que contiene la información parcial de un índice.
 */
void Element::copyFromNode(Node *&node) {
  this->destroy();
  if (node) {
    value = node->value;
    depth = node->depth;
    if (node->content) {
      if (depth < maxIndex) {
        Tree *tree = (Tree*) node->content;
        List *list = new List(tree);
        content = list;
        tree = NULL;
        list = NULL;
      }
      else {
        content = node->content;
        node->content = NULL;
      }
    }
  }
}
//-----------------------------------------------------------------------------
//==Private Methods============================================================
/**
 * Destruye el objeto, eliminando la memoria reservada a la que referencia y
 * dereferenciando los punteros.
 */
void Element::destroy()
{
  if (content) {
    if (depth < maxIndex) {
      delete ((List *) content);
    }
    else {
      FoundStars *tmp = (FoundStars *) content;
      tmp->clear();
      delete tmp;
      tmp = NULL;
    }
    content = NULL;
  }
}
//-----------------------------------------------------------------------------
//==Overloaded operators:[http://www.cplusplus.com/doc/tutorial/templates/]====
