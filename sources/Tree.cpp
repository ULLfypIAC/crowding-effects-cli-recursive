/**
 * Tree.cpp
 *
 * Fichero que define los métodos de la clase árbol (Tree).
 *
 */

//==Includes===================================================================
#include "Tree.h"

//==Special public members:[http://www.cplusplus.com/doc/tutorial/classes2/]===
/**
 * Default constructor
 */
Tree::Tree() :
    depth(1),
    root(NULL),
    first(NULL),
    last(NULL) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
Tree::~Tree() {
  trunctate(root);
  depth = unsigned1Byte(0);
  root = NULL;
  first = NULL;
  last = NULL;
}
//-----------------------------------------------------------------------------
/**
 * Constructor
 * @param depth Indica el nivel de anidamiento en el que está el arbol que se
 * desea construir.
 */
Tree::Tree(const unsigned1Byte &depth) :
    depth(depth),
    root(NULL),
    first(NULL),
    last(NULL) {
}
//==Public Methods=============================================================
/**
 * Indicar que las inserciones han finalizado.
 * Permite inicializar las variables first, next y last.
 */
void Tree::buildComplete() {
  first = NULL;
  last = NULL;
  if (root) {
    buildFirst(root, first);
    Node *tmp = new Node;
    last = tmp;
    buildNext(root, last);
    //buildLast(root, last);
    delete tmp;
  }
}
//-----------------------------------------------------------------------------
/**
 * Inicializar el campo first.
 * @param node Nodo de referencia sobre el que construir el campo first.
 * @param modify Nodo de salida que contedrá el campo first.
 */
void Tree::buildFirst(Node *node, Node *&modify) {
  if (node && node->left) {
    buildFirst(node->left, modify);
  } else {
    modify = node;
  }
}
//-----------------------------------------------------------------------------
/**
 * Inicializar el campo last.
 * @param node Nodo de referencia sobre el que construir el campo last.
 * @param modify Nodo de salida que contedrá el campo last.
 */
void Tree::buildLast(Node *node, Node*& modify) {
  if (node && node->right) {
    buildFirst(node->right, modify);
  } else {
    modify = node;
  }
}
//-----------------------------------------------------------------------------
/**
 * Inicializar el campo next.
 * @param node Nodo de referencia.
 * @param previous Nodo con el valor anterior al nodo de referencia.
 */
void Tree::buildNext(Node *node, Node *&previous) {
  if (node) {
    // Left recursivity
    if (node->left) {
      buildNext(node->left, previous);
    }
    // Solve
    previous->next = node;
    previous = node;
    // Right recursivity
    if (node->right) {
      buildNext(node->right, previous);
    }
  }
}
//-----------------------------------------------------------------------------
/**
 * Insertar un dato en el árbol AVL
 * @param value Valor a insertar.
 * @param node Nodo de referencia.
 * @return true, si el valor fue insertado. false, en caso contrario.
 */
bool Tree::insert(const realNumber &value, Node *&node) {
  Node* parent = NULL;
  Node *current = root;
  // Buscar el dato en el árbol, manteniendo un puntero al nodo padre
  while (current && value != current->value) {
    parent = current;
    if (value > current->value) {
      current = current->right;
    } else {
      if (value < current->value) {
        current = current->left;
      }
    }
  }
  // Si se ha encontrado el elemento, regresar sin insertar
  if (current) {
    node = current;
    return false;
  }
  // Si padre es NULL, entonces el árbol estaba vacío, el nuevo nodo será
  // el nodo raiz
  if (!parent) {
    root = new Node(value, depth);
    node = root;
    counterIndexNodes[depth-1]++;
    return true;
  }
  // Si el dato es menor que el que contiene el nodo padre, lo insertamos
  // en la rama izquierda
  else {
    if (value < parent->value) {
      parent->left = new Node(value, depth, parent);
      node = parent->left;
      balance(parent, Left, true);
      counterIndexNodes[depth-1]++;
      return true;
    }
  // Si el dato es mayor que el que contiene el nodo padre, lo insertamos
  // en la rama derecha
    else {
      if (value > parent->value) {
        parent->right = new Node(value, depth, parent);
        node = parent->right;
        balance(parent, Rigth, true);
        counterIndexNodes[depth-1]++;
        return true;
      }
      else {
        return false;
      }
    }
  }
}
//-----------------------------------------------------------------------------
/**
 * Busca el primer elemento que es mayor o igual al valor deseado.
 * @param value Valor a encontrar.
 * @param node Nodo de referencia.
 */
void Tree::findGreaterEqual(const realNumber &value, Node *&node) {
  if (!root) {
    node = NULL;
    return;
  }
  realNumber tmp;
  /*
  tmp = first->value;
  if (value < tmp || value == tmp) {
    node = first;
    return;
  }
  tmp = last->value;
  if (value > tmp) {
    node = last->next;
    return;
  }
  else {
    if (value == tmp) {
      node = last;
      return;
    }
  }
  */
  Node *current = root, *previous(NULL);
  while (current) {
    previous = current;
    tmp = current->value;
    if (value > tmp) {
      current = current->right;
    }
    else {
      if (value < tmp) {
        current = current->left;
      }
      else {
        break;
      }
    }
  }
  if (value <= tmp) {
    node = previous;
  }
  else {
    node = previous->next;
  }
}
//-----------------------------------------------------------------------------
/**
 * Busca el primer elemento que es estrictamente mayor al valor deseado.
 * @param value Valor a encontrar
 * @param node Nodo de referencia.
 */
void Tree::findGreater(const realNumber &value, Node *&node) {
  if (!root) {
    node = NULL;
    return;
  }
  realNumber tmp;
  /*
  tmp = first->value;
  if (value < tmp) {
    node = first;
    return;
  }
  tmp = last->value;
  if (value > tmp) {
    node = last->next;
    return;
  }
  */
  Node *current = root, *previous;
  while (current) {
    previous = current;
    tmp = current->value;
    if (value > tmp) {
      current = current->right;
    }
    else {
      if (value < tmp) {
        current = current->left;
      }
      else {
        break;
      }
    }
  }
  if (value < tmp) {
    node = previous;
  }
  else {
    node = previous->next;
  }
}
//-----------------------------------------------------------------------------
/**
 * Contar el número de nodos en el árbol.
 * @return número de nodos.
 */
const unsigned int Tree::counterNodes() {
  unsigned int counter(0);
  if (root) {
    counterNodesRecursive(root, counter);
  }
  return counter;
}
//==Private Methods============================================================
/**
 * Equilibrar árbol AVL partiendo del nodo nuevo
 * @param node Nodo de referencia.
 * @param rama Rama donde se aplicará el balanceo.
 * @param isNew indica si el nodo es insertado o no.
 */
void Tree::balance(Node *node, Orientation rama, bool isNew) {
  bool exit = false;
  // Recorrer camino inverso actualizando valores de FE:
  while (node && !exit) {
    if (isNew)
      if (rama == Left)
        node->FE--; // Depende de si añadimos ...
      else
        node->FE++;
    else if (rama == Left)
      node->FE++; // ... o borramos
    else
      node->FE--;
    if (node->FE == 0)
      exit = true; // La altura de las rama que
                   // empieza en nodo no ha variado,
                   // salir de Equilibrar
    else if (node->FE == -2) { // Rotar a derechas y salir:
      if (node->left->FE == 1)
        RDD(node); // Rotación doble
      else
        RSD(node);                         // Rotación simple
      exit = true;
    } else if (node->FE == 2) {  // Rotar a izquierdas y salir:
      if (node->right->FE == -1)
        RDI(node); // Rotación doble
      else
        RSI(node);                        // Rotación simple
      exit = true;
    }
    if (node->parent) {
      if (node->parent->right == node)
        rama = Rigth;
      else
        rama = Left;
    }
    node = node->parent; // Calcular FE, siguiente nodo del camino.
  }
}
//-----------------------------------------------------------------------------
/**
 * Rotación simple a izquierdas.
 * @param node Nodo de referencia.
 */
void Tree::RSI(Node *node) {
  Node *parent = node->parent;
  Node *P = node;
  Node *Q = P->right;
  Node *B = Q->left;
  if (parent)
    if (parent->right == P)
      parent->right = Q;
    else
      parent->left = Q;
  else
    root = Q;
  // Reconstruir árbol:
  P->right = B;
  Q->left = P;
  // Reasignar padres:
  P->parent = Q;
  if (B)
    B->parent = P;
  Q->parent = parent;
  // Ajustar valores de FE:
  P->FE = 0;
  Q->FE = 0;
}
//-----------------------------------------------------------------------------
/**
 * Rotación simple a derechas.
 * @param node Nodo de referencia.
 */
void Tree::RSD(Node *node) {
  Node *parent = node->parent;
  Node *P = node;
  Node *Q = P->left;
  Node *B = Q->right;
  if (parent)
    if (parent->right == P)
      parent->right = Q;
    else
      parent->left = Q;
  else
    root = Q;
  // Reconstruir árbol:
  P->left = B;
  Q->right = P;
  // Reasignar padres:
  P->parent = Q;
  if (B)
    B->parent = P;
  Q->parent = parent;
  // Ajustar valores de FE:
  P->FE = 0;
  Q->FE = 0;
}
//-----------------------------------------------------------------------------
/**
 * Rotación doble a izquierdas.
 * @param node Nodo de referencia.
 */
void Tree::RDI(Node *node) {
  Node *parent = node->parent;
  Node *P = node;
  Node *Q = P->right;
  Node *R = Q->left;
  Node *B = R->left;
  Node *C = R->right;
  if (parent)
    if (parent->right == node)
      parent->right = R;
    else
      parent->left = R;
  else
    root = R;
  // Reconstruir árbol:
  P->right = B;
  Q->left = C;
  R->left = P;
  R->right = Q;
  // Reasignar padres:
  R->parent = parent;
  P->parent = Q->parent = R;
  if (B)
    B->parent = P;
  if (C)
    C->parent = Q;
  // Ajustar valores de FE:
  switch (R->FE) {
    case -1:
      P->FE = 0;
      Q->FE = 1;
      break;
    case 0:
      P->FE = 0;
      Q->FE = 0;
      break;
    case 1:
      P->FE = -1;
      Q->FE = 0;
      break;
  }
  R->FE = 0;
}
//-----------------------------------------------------------------------------
/**
 * Rotación doble a derechas.
 * @param node Nodo de referencia.
 */
void Tree::RDD(Node *node) {
  Node *parent = node->parent;
  Node *P = node;
  Node *Q = P->left;
  Node *R = Q->right;
  Node *B = R->left;
  Node *C = R->right;
  if (parent)
    if (parent->right == node)
      parent->right = R;
    else
      parent->left = R;
  else
    root = R;
  // Reconstruir árbol:
  Q->right = B;
  P->left = C;
  R->left = Q;
  R->right = P;
  // Reasignar padres:
  R->parent = parent;
  P->parent = Q->parent = R;
  if (B)
    B->parent = Q;
  if (C)
    C->parent = P;
  // Ajustar valores de FE:
  switch (R->FE) {
    case -1:
      Q->FE = 0;
      P->FE = 1;
      break;
    case 0:
      Q->FE = 0;
      P->FE = 0;
      break;
    case 1:
      Q->FE = -1;
      P->FE = 0;
      break;
  }
  R->FE = 0;
}
//-----------------------------------------------------------------------------
/**
 * Borrar todos los nodos a partir de uno, incluido éste.
 * @param node Nodo que se desea truncar.
 */
void Tree::trunctate(Node *&node) {
  // Algoritmo recursivo, recorrido en postorden
  if (node) {
    trunctate(node->left);  // Podar izquierdo
    trunctate(node->right); // Podar derecho
    delete node;            // Eliminar nodo
    node = NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Función auxiliar para contar nodos. Función recursiva de recorrido en
 * preorden, el proceso es aumentar el contador
 * @param nodo
 */
void Tree::counterNodesRecursive(Node *node, unsigned int &counter) {
  counter++;
  if (node->left)
    counterNodesRecursive(node->left, counter);
  if (node->right)
    counterNodesRecursive(node->right, counter);
}
//-----------------------------------------------------------------------------
