/**
 * traits.h
 *
 * Fichero que define las cabeceras de las clases base y las variables export
 * utilizadas en el resto de esta implementación.
 *
 */

#ifndef TRAITS_H_
#define TRAITS_H_

//==Defines====================================================================
//#define UNIT_TESTS

//==Includes===================================================================
#include <iostream>
#include <chrono>
#include <vector>

//==Data types=================================================================
// Chrono
using intervalMin = std::chrono::duration<float, std::ratio<60,1>>;
using timePoint=std::chrono::time_point<std::chrono::steady_clock,intervalMin>;
// Standar
using naturalNumber = unsigned int;
using integerNumber = signed int;
using realNumber = double;
using unsigned1Byte = unsigned char;
using signed1Byte = signed char;
using nodeContent = void;
// Struct
using Epsilon = struct _Epsilon {
  realNumber *index;
  _Epsilon();                             // Default constructor
  ~_Epsilon();                            // Destructor
  _Epsilon(const _Epsilon&);              // Copy constructor
  _Epsilon& operator= (const _Epsilon&);  // Copy assignment
  _Epsilon (_Epsilon&&);                  // Move constructor
  _Epsilon& operator= (_Epsilon&&);       // Move assignment
  void destroy();
};
using Range = struct _Range {
  realNumber min, max;
};
using ArtificialStar = struct _ArtificialStar {
  unsigned int id;
  unsigned int selected;
  realNumber *column;
  _ArtificialStar();                                    // Default constructor
  ~_ArtificialStar();                                   // Destructor
  _ArtificialStar (const _ArtificialStar&);             // Copy constructor
  _ArtificialStar& operator= (const _ArtificialStar&);  // Copy assignment
  _ArtificialStar (_ArtificialStar&&);                  // Move constructor
  _ArtificialStar& operator= (_ArtificialStar&&);       // Move assignment
  void destroy();
};
using RealStar = struct _RealStar {
  unsigned int id;
  realNumber *index;
  realNumber *column;
  _RealStar();                              // Default constructor
  ~_RealStar();                             // Destructor
  _RealStar (const _RealStar&);             // Copy constructor
  _RealStar& operator= (const _RealStar&);  // Copy assignment
  _RealStar (_RealStar&&);                  // Move constructor
  _RealStar& operator= (_RealStar&&);       // Move assignment
  void destroy();
};
using DispersedStar = struct _DispersedStar {
  unsigned int id;
  realNumber *column;
  _DispersedStar();                                   // Default constructor
  ~_DispersedStar();                                  // Destructor
  _DispersedStar (const _DispersedStar&);             // Copy constructor
  _DispersedStar& operator= (const _DispersedStar&);  // Copy assignment
  _DispersedStar (_DispersedStar&&);                  // Move constructor
  _DispersedStar& operator= (_DispersedStar&&);       // Move assignment
  void destroy();
};
// Set
using FoundStars = std::vector<ArtificialStar*>;

//==Gobal variables============================================================
extern unsigned int  minSetStars;    //  10
extern unsigned int  maxSetStars;    //   ?
extern realNumber    maxValue;       //  25.0
extern unsigned int  maxIndex;       //   4
extern unsigned int  artfMaxColumn;  //   6
extern unsigned int  artfMaxRow;     //   ?
extern unsigned int  realMaxColumn;  //   6
extern unsigned int  realMaxRow;     //   ?
extern unsigned int  dispMaxColumn;  //   8
extern unsigned int  *counterIndexNodes;
//==Prototype functions========================================================
signed char compareAsInt(const realNumber& A,
                         const realNumber& B,
                               realNumber epsilon=1e-4);

bool compareNumbers(const realNumber& A,
                          std::string operation,
                    const realNumber& B,
                          realNumber  error=1e-6);

std::ostream& operator<<(std::ostream& out, const ArtificialStar& artfStar);
std::ostream& operator<<(std::ostream& out, const RealStar& realStar);

#endif /* TRAITS_H_ */
