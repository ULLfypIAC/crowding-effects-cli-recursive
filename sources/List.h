/**
 * List.h
 *
 * Fichero que define la estructura lista (List).
 * Esta estructura ofrece resultados más eficientes que la estructura de árbol
 * (Tree) en tiempos de búsqueda.
 *
 */

#ifndef LIST_H_
#define LIST_H_

//==Includes===================================================================
#include "traits.h"
#include "Element.h"

//==Classes====================================================================
class Tree;
//-----------------------------------------------------------------------------
class List {
  public: // Members
    unsigned1Byte depth;
    unsigned int size;
    unsigned int current;
    Element *elements;
  public:   // Special methods
    List();                           // Default constructor
    virtual ~List();                  // Destructor
    List(const unsigned1Byte &depth); // Constructor
    List(Tree *&tree);                // Constructor
  public:   // Public methods
    void findGreaterEqual(const realNumber &value, Element *&element);
    void findGreater(const realNumber &value, Element *&element);
    unsigned int findGreaterEqual(const realNumber &value);
    unsigned int findGreater(const realNumber &value);
    Element* next();
    Element* next(unsigned int& i);
    Element* at(const unsigned int& i);
    Element* first();
    Element* last();
  private:  // Private methods
    void destroy();
};
//-----------------------------------------------------------------------------
#endif /* LIST_H_ */
