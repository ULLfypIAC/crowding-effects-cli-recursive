/**
 * List.cpp
 *
 * Fichero que define los métodos de la estructura lista (List).
 * Esta estructura ofrece resultados más eficientes que la estructura de árbol
 * (Tree) en tiempos de búsqueda.
 *
 */

//==Includes===================================================================
#include "List.h"
#include "Tree.h"

//==Special members:[http://www.cplusplus.com/doc/tutorial/classes2/]==========
/**
 * Default constructor
 */
List::List() :
    depth(1),
    size(0),
    current(0),
    elements(NULL) {
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
List::~List() {
  this->destroy();
  size = 0;
  current = 0;
  depth = 1;
}
//-----------------------------------------------------------------------------
/**
 * Constructor que crea la estructura de lista con un valor de profundidad
 * específico.
 * @param depth Profundidad que define nivel de anidamiento de esta estructura.
 */
List::List(const unsigned1Byte &depth) :
    depth(depth),
    size(0),
    current(0),
    elements(NULL) {
}
//-----------------------------------------------------------------------------
/**
 * Constructor que crea la estructura de lista a partir de una estructura de
 * árbol.
 * @param tree Árbol a partir del cual se creará la estructura de lista.
 */
List::List(Tree *&tree) :
    depth(0),
    size(0),
    current(0),
    elements(NULL)
{
  if (tree) {
    depth = tree->depth;
    tree->buildComplete();
    if (tree->root) {
      size = tree->counterNodes();
      elements = new Element[size];
      Node *it = tree->first;
      Node *end = tree->last->next;
      for (unsigned int i(0); it and it != end; ++i, it = it->next) {
        elements[i].copyFromNode(it);
      }
      it = NULL;
      end = NULL;
    }
  }
}
//==Public Methods=============================================================
/**
 * Búsqueda de un elemento cuyo valor era mayor o igual al valor especificado.
 * @param value Valor deseado de la búsqueda.
 * @param element Lista de elementos sobre la que buscar.
 */
void List::findGreaterEqual(const realNumber &value, Element *&element) {
  if (!elements) {
    element = NULL;
    current = 0;
    return;
  }
  // Comprobar si el valor desado es menor o igual que el valor mínimo
  if (value <= elements[0].value) {
    current = 0;
    element = &elements[0];
    return;
  }
  // Comprobar si el valor desado es mayor que el valor máximo
  if (value > elements[size-1].value) {
    current = size;
    element = NULL;
    return;
  }
  // El valor deseado está incluido entre el valor mínimo y máximo
  unsigned int i(0), j(size-1), k(0);
  realNumber pivot;
  while (i <= j) {
    k = ((i + (j - i))/ 2);
    pivot = elements[k].value;
    if (value > pivot) {
      i = k + 1;  // right set
    } else {
      if (value < pivot) {
        j = k;    // left set
      } else {
        i = k;
        j = k-1;
      }
    }
  }
  current = i;
  element = &elements[i];
  return;
}
//-----------------------------------------------------------------------------
/**
 * Búsqueda de un elemento cuyo valor era mayor al valor especificado.
 * @param value Valor deseado de la búsqueda.
 * @param element Lista de elementos sobre la que buscar.
 */
void List::findGreater(const realNumber &value, Element *&element) {
  if (!elements) {
    element = NULL;
    return;
  }
  // Comprobar si el valor desado es menor que el valor mínimo
  if (value < elements[0].value) {
    element = &elements[0];
    return;
  }
  // Comprobar si el valor desado es mayor que el valor máximo
  if (value >= elements[size-1].value) {
    element = NULL;
    return;
  }
  // El valor deseado está incluido entre el valor mínimo y máximo
  unsigned int i(0), j(size-1), k(0);
  realNumber pivot;
  while (i <= j) {
    k = ((i + (j - i))/ 2);
    pivot = elements[k].value;
    if (value > pivot) {
      i = k + 1;  // right set
    } else {
      if (value < pivot) {
        j = k;    // left set
      } else {
        i = k+1;
        j = k;
      }
    }
  }
  element = &elements[i];
  return;
}
//-----------------------------------------------------------------------------
/**
 * Búsqueda de un elemento cuyo valor era mayor o igual al valor especificado.
 * @param value Valor deseado de la búsqueda.
 * @return el número del índice donde se encuentra el elemento buscado.
 */
unsigned int List::findGreaterEqual(const realNumber &value) {
  if (!elements) {
    return size;
  }
  // Comprobar si el valor desado es menor o igual que el valor mínimo
  /// if (value <= elements[0].value) {
  if (compareNumbers(value, "<=", elements[0].value)) {
    return 0;
  }
  // Comprobar si el valor desado es mayor que el valor máximo
  /// if (value > elements[size-1].value) {
  if (compareNumbers(value, ">", elements[size-1].value)) {
    return size;
  }
  // El valor deseado está incluido entre el valor mínimo y máximo
  unsigned int i(0), k(0), count(size-1), step(0);
  while(count > 0) {
    k = i;
    step = count/2;
    k += step;
    /// if (elements[k].value < value) {
    if (compareNumbers(elements[k].value, "<", value)) {
      i = ++k;
      count -= step + 1;
    }
    else {
      count = step;
    }
  }
  return i;
}
//-----------------------------------------------------------------------------
/**
 * Búsqueda de un elemento cuyo valor era mayor al valor especificado.
 * @param value Valor deseado de la búsqueda.
 * @return el número del índice donde se encuentra el elemento buscado.
 */
unsigned int List::findGreater(const realNumber &value) {
  if (!elements) {
    return size;
  }
  // Comprobar si el valor desado es menor que el valor mínimo
  /// if (value < elements[0].value) {
  if (compareNumbers(value, "<", elements[0].value)) {
    return 0;
  }
  // Comprobar si el valor desado es mayor que el valor máximo
  /// if (value >= elements[size-1].value) {
  if (compareNumbers(value, ">=", elements[size-1].value)) {
    return size;
  }
  // El valor deseado está incluido entre el valor mínimo y máximo
  unsigned int i(0), k(0), count(size-1), step(0);
  while(count > 0) {
    k = i;
    step = count/2;
    k += step;
    /// if (!(value < elements[k].value)) {
    if (!(compareNumbers(value, "<", elements[k].value))) {
      i = ++k;
      count -= step + 1;
    }
    else {
      count = step;
    }
  }
  return i;
}
//-----------------------------------------------------------------------------
/**
 * Busca el siguiente elemento con el valor superior al actual.
 * @return elemento siguiente al actual.
 */
Element* List::next() {
  ++current;
  return at(current);
}
//-----------------------------------------------------------------------------
/**
 * Busca el siguiente elemento al especificado.
 * @param i El índice del elemento deseado.
 * @return Elemento siguiente al especificado.
 */
Element* List::next(unsigned int& i) {
  ++i;
  return at(i);
}
//-----------------------------------------------------------------------------
/**
 * Busca el elemento especificado.
 * @param i El índice del elemento deseado.
 * @return Elemento especificado.
 */
Element* List::at(const unsigned int& i) {
  if (i < size) {
    return &elements[i];
  } else {
    return NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Busca el elemento con el valor menor de los elementos almacenados.
 * @return Elemento con el valor menor a los almacenados.
 */
Element* List::first() {
  if (size > 0) {
    return &elements[0];
  } else {
    return NULL;
  }
}
//-----------------------------------------------------------------------------
/**
 * Busca el elemento con el valor mayor de los elementos almacenados.
 * @return Elemento con el valor mayor a los almacenados.
 */
Element* List::last() {
  if (size > 0) {
    return &elements[size-1];
  } else {
    return NULL;
  }
}
//-----------------------------------------------------------------------------
//==Private Methods============================================================
/**
 * Destruye el objeto, eliminando la memoria reservada a la que referencia y
 * dereferenciando los punteros.
 */
void List::destroy() {
  if (elements) {
    delete[] elements;
    elements = NULL;
  }
}
//-----------------------------------------------------------------------------
