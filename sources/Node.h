/**
 * Node.h
 *
 * Fichero que define las cabeceras de la clase nodo (Node), que es el elemento
 * básico de la estructura de árbol (Tree).
 *
 */

#ifndef NODE_H_
#define NODE_H_

//==Includes===================================================================
#include <iostream>
#include "traits.h"

//==Classes====================================================================
class Tree;
//-----------------------------------------------------------------------------
class Node {
  friend class Tree;
  public: // Members
    realNumber value;
    unsigned1Byte depth;
    signed1Byte FE;
    Node *parent;
    Node *left;
    Node *right;
    Node *next;
    nodeContent *content;
  public: // Special members
    Node();                         // Default constructor
    virtual ~Node();                // Destructor
  //Node(const Node&);              // Copy constructor
  //Node& operator=(const Node&);   // Copy assignment
  //Node(Node&&);                   // Move constructor
  //Node& operator=(Node&&);        // Move assignment
    Node(const realNumber &value, const unsigned1Byte &depth);
    Node(const realNumber &value, const unsigned1Byte &depth, Node *parent);
  private: // Private methods
    void destroy();
  public: // Overloaded operators
    friend bool operator <(const Node&, const Node&);
    friend std::ostream& operator<<(std::ostream&, const Node&);
};
//-----------------------------------------------------------------------------

#endif /* NODE_H_ */
