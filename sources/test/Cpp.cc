/*
 * others.cc
 *
 * Unit Test for file operations
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include <iostream>
#include <string>
#include <vector>
#include <functional>
#include <limits>
#include "gtest/gtest.h"
#include "traits.h"

//==Functions==================================================================
inline bool True()  { /* std::cout << "[T] ";*/ return true; }
inline bool False() { /* std::cout << "[F] ";*/ return false; }

//==Unit Tests=================================================================
/**
 * Testea que el operador booleano AND deja de evaluar el resto de expresiones
 * a partir de que el valor de una expresión sea falsa.
 *
 */
TEST(Standard, OperatorAnd) {
  bool r;
  //std::cout << "---------------------\n";
  r = !(True() && True()); // [T] [T]
  //std::cout << "condition: !(t & t) : " << std::boolalpha << r << "\n";
  ASSERT_FALSE(r);
  //std::cout << "---------------------\n";
  r = !(True() && False()); // [T] [F]
  //std::cout << "condition: !(t & f) : " << std::boolalpha << r << "\n";
  ASSERT_TRUE(r);
  //std::cout << "---------------------\n";
  r = !(False() && True()); // [F]
  //std::cout << "condition: !(f & t) : " << std::boolalpha << r << "\n";
  ASSERT_TRUE(r);
  //std::cout << "---------------------\n";
  r = !(False() && False());// [F]
  //std::cout << "condition: !(f & f) : " << std::boolalpha << r << "\n";
  ASSERT_TRUE(r);
}
//-----------------------------------------------------------------------------
/**
 * Teste que un puntero sigue apuntando a la misma dirección de memoria después
 * de la instrucción delete.
 *
 * Esto muestra lo importante que es eliminar la dirección de memoria a la que
 * apunta un puntero después de utilizar el operator delete.
 *
 * El operador delete solo invoca al destructor del objeto al que apunta el
 * puntero, pero el puntero sigue haciendo referencia a la zona de memoria
 * antes del delete.
 *
 */
TEST(Standard, PointerSameAddress) {
  int *p1 = new int(10), *p2 = p1;
  EXPECT_EQ(p1, p2);
  EXPECT_EQ(10, *p1);
  EXPECT_EQ(10, *p2);
  delete p1;
  EXPECT_EQ(p1, p2);
}
//-----------------------------------------------------------------------------
/**
 * Testea el uso de la instrucción continue en un bucle.
 */
TEST(Standard, Continue) {
  int tmp(0);
  for (int i(0); i < 10; ++i) {
    continue;
    tmp = i;
  }
  ASSERT_EQ(0, tmp);
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo poder utilizar un puntero generico (void *)
 *
 * Para mayor información, puedes consultar la sección "void pointers" del
 * siguiente enlace: [http://www.cplusplus.com/doc/tutorial/pointers/]
 */
TEST(Standard, CastVoidPointer) {
  void *p1 = new float(1.0f);
  float *p2;
  p2 = (float*) p1;
  EXPECT_FLOAT_EQ(1.0f, *p2);
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo poder utilizar un puntero a una función
 *
 * Para mayor información, puedes consultar la sección "void pointers" del
 * siguiente enlace: [http://www.cplusplus.com/doc/tutorial/pointers/]
 */
TEST(Standard, PointersToFunctions1) {
  bool (*function)(void);
  function = True;
  ASSERT_TRUE((*function)());
  function = False;
  ASSERT_FALSE((*function)());
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo poder utilizar un puntero a una función con una clase de C++.
 *
 * Para mayor información, puedes consultar el siguiente enlace:
 * [http://www.cplusplus.com/reference/functional/function/]
 */
TEST(Functional, Function) {
  std::function<bool(void)> function;
  function = True;
  ASSERT_TRUE(function());
  function = False;
  ASSERT_FALSE(function());
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo se debe utilizar correctamente el método data de la clase vector
 * cuando su contenido es un puntero de una clase.
 */
TEST(Vector, data) {
  maxIndex = 1;
  FoundStars *foundStars = new FoundStars;
  ArtificialStar *crowdStar = new ArtificialStar;
  crowdStar->id = 10;
  foundStars->push_back(crowdStar);
  ASSERT_EQ(1, foundStars->size());
  ArtificialStar **data(foundStars->data());
  ASSERT_EQ(10, data[0]->id);
  delete crowdStar;   crowdStar = NULL;
  delete foundStars;  foundStars=NULL;
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo utilizar correctamente un String Stream de C++, leyendo todos
 * los valores numéricos que están contenidos en una línea.
 */
TEST(SStream, GetLineOneLineAllFields) {
  std::string strLine;
  std::istringstream rawLines("5.991 4.949 5.873 4.853 1210.0 14.0\n");
  unsigned int numLines;
  unsigned int numField;
  for (numLines = 0; true; numLines++) {
    getline (rawLines,strLine);
    if (strLine.empty()) {
      if (rawLines.eof()) {
        break;
      }
    } else {
      EXPECT_STREQ("5.991 4.949 5.873 4.853 1210.0 14.0", strLine.data());
      std::istringstream fields(strLine);
      realNumber field;
      for (numField = 0; true; numField++) {
        if (fields.eof()) {
          break;
        } // if - eof
        fields >> field;
      }
    }
  }
  EXPECT_EQ(1, numLines); // line number
  EXPECT_EQ(6, numField); // field number
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo utilizar correctamente un String Stream de C++, leyendo los dos
 * primeros valores numéricos que están contenidos en una línea.
 */
TEST(SStream, GetLineOneLineTwoFields) {
  std::string strLine;
  std::istringstream rawLines("5.991 4.949 5.873 4.853 1210.0 14.0\n");
  unsigned int numLines;
  unsigned int numField;
  realNumber field;
  for (numLines = 0; true; numLines++) {
    getline (rawLines,strLine);
    if (strLine.empty()) {
      if (rawLines.eof()) {
        break;
      }
    } else {
      EXPECT_STREQ("5.991 4.949 5.873 4.853 1210.0 14.0", strLine.data());
      std::istringstream fields(strLine);
      for (numField = 0; numField < 2; numField++) {
        if (fields.eof()) {
          break;
        }
        fields >> field;
      }
    }
  }
  EXPECT_EQ(1, numLines); // line number
  EXPECT_EQ(2, numField); // field number
  EXPECT_FLOAT_EQ(4.949, field); // last read field
}
//-----------------------------------------------------------------------------
/**
 * Testea cómo utilizar correctamente un String Stream de C++, leyendo sólo
 * los valores de líneas no vacías.
 */
TEST(SStream, GetLineMultipleNotEmptyLines) {
  std::string strLine;
  std::istringstream rawLines("1\n2\n3\n4\n5\n6\n7\n\n8\n\n\n9");
  unsigned int numLines;
  for (numLines = 0; !rawLines.eof(); ) {
    getline (rawLines,strLine);
    if (!strLine.empty()) {
      numLines++;
    }
  }
  EXPECT_EQ(9, numLines); // line number
}
//-----------------------------------------------------------------------------
/**
 * Testea la correcta extración desde un stringstream de la representación de
 * valor normal de tipo float.
 */
TEST(SStream, FloatNormalValue) {
  std::stringstream iss;
  float number;
  iss << 1.0f;
  iss >> number;
  ASSERT_TRUE(iss.good() or iss.eof());
}
//-----------------------------------------------------------------------------
/**
 * Testea que la extración desde un stringstream de la representación de un
 * valor infinito de tipo float es erronea.
 */
TEST(SStream, FloatInfinityValue) {
  ASSERT_TRUE(std::numeric_limits<float>::has_infinity);
  std::stringstream iss;
  float number;
  iss << std::numeric_limits<float>::infinity();
  iss >> number;
  ASSERT_FALSE(iss.good() or iss.eof());
}
//-----------------------------------------------------------------------------
/**
 * Testea que la extración desde un stringstream de la representación de un
 * NaN (Not-a-Number) es erronea.
 */
TEST(SStream, FloatNaNValue) {
  ASSERT_TRUE(std::numeric_limits<float>::has_quiet_NaN);
  std::stringstream iss;
  float number;
  iss << std::numeric_limits<float>::quiet_NaN();
  iss >> number;
  ASSERT_FALSE(iss.good() or iss.eof());
}
//-----------------------------------------------------------------------------
