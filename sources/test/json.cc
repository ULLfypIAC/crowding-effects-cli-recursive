/*
 * json.cc
 */

//==Includes===================================================================
#include "gtest/gtest.h"
#include "json/json.h"
#include <string>
#include <fstream>

//==Unit Tests=================================================================
TEST(Json, readFromStringEmptyString) {
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse("", parsedFromString);
  ASSERT_FALSE(parsingSuccessful);
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringInvalidObject) {
  std::string JDocument = "{";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_FALSE(parsingSuccessful);
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidEmptyObject) {
  std::string JDocument = "{}";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidNullValue) {
  std::string JDocument = "{ \"key\" : null }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isNull());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidBoolValue) {
  std::string JDocument = "{ \"key\" : true }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isBool());
  ASSERT_TRUE(getValue.asBool());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidStringValue) {
  std::string JDocument = "{ \"key\" : \"string\" }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isString());
  ASSERT_STREQ("string", getValue.asCString());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidIntValue) {
  std::string JDocument = "{ \"key\" : 1 }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isInt());
  ASSERT_EQ(1, getValue.asInt());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidDoubleValue) {
  std::string JDocument = "{ \"key\" : 1.0 }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isDouble());
  ASSERT_FLOAT_EQ(1.0f, getValue.asFloat());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidStringArrayValue) {
  std::string JDocument = "{ \"key\" : [\"str1\", \"str2\"] }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isArray());
  for (unsigned int index(0); index < getValue.size(); ++index) {
    ASSERT_TRUE(getValue[index].isString());
  }
  ASSERT_STREQ("str1", getValue[0].asCString());
  ASSERT_STREQ("str2", getValue[1].asCString());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringValidIntArrayValue) {
  std::string JDocument = "{ \"key\" : [1, 2] }";
  Json::Value parsedFromString;
  Json::Reader reader;
  bool parsingSuccessful = reader.parse(JDocument, parsedFromString);
  ASSERT_TRUE(parsingSuccessful);
  Json::Value getValue = parsedFromString["key"];
  ASSERT_TRUE(getValue.isArray());
  for (unsigned int index(0); index < getValue.size(); ++index) {
    ASSERT_TRUE(getValue[index].isInt());
  }
  ASSERT_EQ(1, getValue[0].asInt());
  ASSERT_EQ(2, getValue[1].asInt());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromStringCompleExample) {
  std::string JDocument;
  JDocument =  "{";
  JDocument += "\"general\":{\"minSetSize\":10,\"maxValue\":25.0,\"customized\":true,\"indexes\":2},";
  JDocument += "\"crowdStars\":{\"file\":\"fC.txt\",\"rows\":10,\"columns\":6,\"index\":[\"1\",\"1-2\"]},";
  JDocument += "\"realStars\": {\"file\":\"fM.txt\",\"rows\":10,\"columns\":6,\"index\":[\"1\",\"1-2\"]},";
  JDocument += "\"dispersedStars\":{\"file\":\"fOut.txt\"}";
  JDocument += "}";
  Json::Value jRoot;
  Json::Reader reader(Json::Features::all());
  bool isOK = reader.parse(JDocument, jRoot);
  ASSERT_TRUE(isOK);
  // General
  Json::Value JGeneral = jRoot["general"];
  ASSERT_TRUE(JGeneral.isObject());
  EXPECT_TRUE(JGeneral["minSetSize"].isInt());
  EXPECT_EQ(10, JGeneral["minSetSize"].asInt());
  EXPECT_TRUE(JGeneral["maxValue"].isDouble());
  EXPECT_FLOAT_EQ(25.0f, JGeneral["maxValue"].asFloat());
  EXPECT_TRUE(JGeneral["customized"].isBool());
  EXPECT_TRUE(JGeneral["customized"].asBool());
  EXPECT_TRUE(JGeneral["indexes"].isInt());
  EXPECT_EQ(2, JGeneral["indexes"].asInt());
  // CrowdStars
  Json::Value JCrowd = jRoot["crowdStars"];
  ASSERT_TRUE(JCrowd.isObject());
  EXPECT_TRUE(JCrowd["file"].isString());
  EXPECT_STREQ("fC.txt", JCrowd["file"].asCString());
  EXPECT_TRUE(JCrowd["rows"].isInt());
  EXPECT_EQ(10, JCrowd["rows"].asInt());
  EXPECT_TRUE(JCrowd["columns"].isInt());
  EXPECT_EQ(6, JCrowd["columns"].asInt());
  EXPECT_TRUE(JCrowd["index"].isArray());
  EXPECT_TRUE(JCrowd["index"].isValidIndex(0));
  EXPECT_TRUE(JCrowd["index"][0].isString());
  EXPECT_STREQ("1", JCrowd["index"][0].asCString());
  EXPECT_TRUE(JCrowd["index"].isValidIndex(1));
  EXPECT_TRUE(JCrowd["index"][1].isString());
  EXPECT_STREQ("1-2", JCrowd["index"][1].asCString());
  // RealStars
  Json::Value JReal = jRoot["realStars"];
  ASSERT_TRUE(JReal.isObject());
  EXPECT_TRUE(JReal["file"].isString());
  EXPECT_STREQ("fM.txt", JReal["file"].asCString());
  EXPECT_TRUE(JReal["rows"].isInt());
  EXPECT_EQ(10, JReal["rows"].asInt());
  EXPECT_TRUE(JReal["columns"].isInt());
  EXPECT_EQ(6, JReal["columns"].asInt());
  EXPECT_TRUE(JReal["index"].isArray());
  EXPECT_TRUE(JReal["index"].isValidIndex(0));
  EXPECT_TRUE(JReal["index"][0].isString());
  EXPECT_STREQ("1", JReal["index"][0].asCString());
  EXPECT_TRUE(JReal["index"].isValidIndex(1));
  EXPECT_TRUE(JReal["index"][1].isString());
  EXPECT_STREQ("1-2", JReal["index"][1].asCString());
  // DispersedStars
  Json::Value JDispersed = jRoot["dispersedStars"];
  ASSERT_TRUE(JDispersed.isObject());
  EXPECT_TRUE(JDispersed["file"].isString());
  EXPECT_STREQ("fOut.txt", JDispersed["file"].asCString());
}
//-----------------------------------------------------------------------------
TEST(Json, readFromInputFileStreamCompleExample) {
  std::string fileName("./files/unit-test.json");
  std::fstream JDocument;
  JDocument.open(fileName.c_str(), std::fstream::out | std::fstream::trunc);
  ASSERT_TRUE(JDocument.is_open());
  JDocument << "{ // Grammar -> http://www.json.org/\n"
            << " \"general\":{\n"
            << "  \"minSetSize\":10,\n"
            << "  \"maxValue\":25.0,\n"
            << "  \"customized\":true,\n"
            << "  \"epsilons\": [\n"
            << "   0.1,\n"
            << "   0.04,\n"
            << "   300,\n"
            << "   300\n"
            << "  ]\n"
            << " },\n"
            << " \"crowdStars\":{\n"
            << "  \"file\":\"fC.txt\",\n"
            << "  \"rows\":10,\n"
            << "  \"columns\":6,\n"
            << "  \"index\":[\n"
            << "   \"C1\",\n"
            << "   \"C1-C2\",\n"
            << "   \"C5\",\n"
            << "   \"C6\"\n"
            << "  ],\n"
            << "  \"sort_file\":\"sort_fC.txt\"\n"
            << " },\n"
            << " \"realStars\": {\n"
            << "  \"file\":\"fM.txt\",\n"
            << "  \"rows\":10,\n"
            << "  \"columns\":6,\n"
            << "  \"index\":[\n"
            << "   \"C1\",\n"
            << "   \"C1-C2\",\n"
            << "   \"C5\",\n"
            << "   \"C6\"\n"
            << "  ]\n"
            << " },\n"
            << " \"dispersedStars\":{\n"
            << "  \"file\":\"fOut.txt\"\n"
            << " }\n"
            << "}\n";
  JDocument.close();
  JDocument.open(fileName.c_str(), std::ifstream::in);
  ASSERT_TRUE(JDocument.is_open());
  Json::Value jRoot;
  Json::Reader reader(Json::Features::all());
  bool isOK = reader.parse(JDocument, jRoot);
  ASSERT_TRUE(isOK) << reader.getFormattedErrorMessages();
  // General
  Json::Value JGeneral = jRoot["general"];
  ASSERT_TRUE(JGeneral.isObject());
  EXPECT_TRUE(JGeneral["minSetSize"].isInt());
  EXPECT_EQ(10, JGeneral["minSetSize"].asInt());
  EXPECT_TRUE(JGeneral["maxValue"].isDouble());
  EXPECT_FLOAT_EQ(25.0f, JGeneral["maxValue"].asFloat());
  EXPECT_TRUE(JGeneral["epsilons"].isArray());
  EXPECT_TRUE(JGeneral["epsilons"].isValidIndex(0));
  EXPECT_TRUE(JGeneral["epsilons"][0].isDouble());
  EXPECT_FLOAT_EQ(0.1f, JGeneral["epsilons"][0].asFloat());
  EXPECT_TRUE(JGeneral["epsilons"].isValidIndex(1));
  EXPECT_TRUE(JGeneral["epsilons"][1].isDouble());
  EXPECT_FLOAT_EQ(0.04f, JGeneral["epsilons"][1].asFloat());
  EXPECT_TRUE(JGeneral["epsilons"].isValidIndex(2));
  EXPECT_TRUE(JGeneral["epsilons"][2].isDouble());
  EXPECT_FLOAT_EQ(300.0f, JGeneral["epsilons"][2].asFloat());
  EXPECT_TRUE(JGeneral["epsilons"].isValidIndex(3));
  EXPECT_TRUE(JGeneral["epsilons"][3].isDouble());
  EXPECT_FLOAT_EQ(300.0f, JGeneral["epsilons"][3].asFloat());
  // CrowdStars
  Json::Value JCrowd = jRoot["crowdStars"];
  ASSERT_TRUE(JCrowd.isObject());
  EXPECT_TRUE(JCrowd["file"].isString());
  EXPECT_STREQ("fC.txt", JCrowd["file"].asCString());
  EXPECT_TRUE(JCrowd["sort_file"].isString());
  EXPECT_STREQ("sort_fC.txt", JCrowd["sort_file"].asCString());
  EXPECT_TRUE(JCrowd["rows"].isInt());
  EXPECT_EQ(10, JCrowd["rows"].asInt());
  EXPECT_TRUE(JCrowd["columns"].isInt());
  EXPECT_EQ(6, JCrowd["columns"].asInt());
  EXPECT_TRUE(JCrowd["index"].isArray());
  EXPECT_TRUE(JCrowd["index"].isValidIndex(0));
  EXPECT_TRUE(JCrowd["index"][0].isString());
  EXPECT_STREQ("C1", JCrowd["index"][0].asCString());
  EXPECT_TRUE(JCrowd["index"].isValidIndex(1));
  EXPECT_TRUE(JCrowd["index"][1].isString());
  EXPECT_STREQ("C1-C2", JCrowd["index"][1].asCString());
  // RealStars
  Json::Value JReal = jRoot["realStars"];
  ASSERT_TRUE(JReal.isObject());
  EXPECT_TRUE(JReal["file"].isString());
  EXPECT_STREQ("fM.txt", JReal["file"].asCString());
  EXPECT_TRUE(JReal["rows"].isInt());
  EXPECT_EQ(10, JReal["rows"].asInt());
  EXPECT_TRUE(JReal["columns"].isInt());
  EXPECT_EQ(6, JReal["columns"].asInt());
  EXPECT_TRUE(JReal["index"].isArray());
  EXPECT_TRUE(JReal["index"].isValidIndex(0));
  EXPECT_TRUE(JReal["index"][0].isString());
  EXPECT_STREQ("C1", JReal["index"][0].asCString());
  EXPECT_TRUE(JReal["index"].isValidIndex(1));
  EXPECT_TRUE(JReal["index"][1].isString());
  EXPECT_STREQ("C1-C2", JReal["index"][1].asCString());
  // DispersedStars
  Json::Value JDispersed = jRoot["dispersedStars"];
  ASSERT_TRUE(JDispersed.isObject());
  EXPECT_TRUE(JDispersed["file"].isString());
  EXPECT_STREQ("fOut.txt", JDispersed["file"].asCString());
}
//-----------------------------------------------------------------------------
