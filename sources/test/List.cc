/*
 * List.cc
 *
 * Unit Test for file operations
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include "gtest/gtest.h"
#include "List.h"

//==Unit Tests=================================================================
TEST(List, DefaultConstructor) {
  List *list = new List;
  EXPECT_EQ(1, list->depth);
  EXPECT_EQ(0, list->size);
  EXPECT_EQ(NULL, list->elements);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, Destructor) {
  List *list = new List;
  EXPECT_EQ(1, list->depth);
  EXPECT_EQ(0, list->size);
  EXPECT_EQ(NULL, list->elements);

  unsigned int nElements = 10;
  Element *elements = new Element[nElements];
  list->elements = elements;
  list->size = nElements;
  EXPECT_EQ(nElements, list->size);
  EXPECT_EQ(elements, list->elements);

  delete list;
  EXPECT_EQ(0, list->size);
  EXPECT_EQ(NULL, list->elements);
}
//-----------------------------------------------------------------------------
TEST(List, findGreaterEqual0Node) {
  List *list = new List;
  Element *element(NULL);
  list->findGreaterEqual(realNumber(0), element);
  EXPECT_EQ(NULL, element);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreaterEqual1NodeLess) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreaterEqual(realNumber(0), tmp);
  EXPECT_EQ(element, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreaterEqual1NodeEqual) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreaterEqual(realNumber(1), tmp);
  EXPECT_EQ(element, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreaterEqual1NodeGreater) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreaterEqual(realNumber(2), tmp);
  EXPECT_NE(element, tmp);
  EXPECT_EQ(NULL, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreaterEqual10Node) {
  maxIndex = 1;
  unsigned int nElements = 10;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  unsigned int i(0);
  for (; i < nElements; ++i) {
    list->elements[i].depth = 1;
    list->elements[i].value = realNumber(i+1);
  }
  //
  Element *element(NULL);
  realNumber number(5);
  list->findGreaterEqual(number, element);
  EXPECT_FLOAT_EQ(number, element->value);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreater0Node) {
  List *list = new List;
  Element *element(NULL);
  list->findGreater(realNumber(0), element);
  EXPECT_EQ(NULL, element);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreater1NodeLess) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreater(realNumber(0), tmp);
  EXPECT_EQ(element, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreater1NodeEqual) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreater(number, tmp);
  EXPECT_NE(element, tmp);
  EXPECT_EQ(NULL, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreater1NodeGreater) {
  maxIndex = 1;
  unsigned int nElements = 1;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  realNumber number(1);
  list->elements[0].value = number;
  Element *element(&list->elements[0]);
  Element *tmp(NULL);
  list->findGreater(realNumber(2), tmp);
  EXPECT_NE(element, tmp);
  EXPECT_EQ(NULL, tmp);
  delete list;
}
//-----------------------------------------------------------------------------
TEST(List, findGreater10Node) {
  maxIndex = 1;
  unsigned int nElements = 10;
  // Crear estructura sobre la que se buscará
  List *list = new List(1);
  list->elements = new Element[nElements];
  list->size = nElements;
  unsigned int i(0);
  for (; i < nElements; ++i) {
    list->elements[i].depth = 1;
    list->elements[i].value = realNumber(i+1);
  }
  //
  Element *element(NULL);
  realNumber numberThis(5), numberNext(6);
  list->findGreater(numberThis, element);
  EXPECT_FLOAT_EQ(numberNext, element->value);
  delete list;
}
//-----------------------------------------------------------------------------
