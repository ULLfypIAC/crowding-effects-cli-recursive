/*
 * Node.cc
 *
 * Unit Test for Node class
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include "gtest/gtest.h"
#include "Node.h"

//==Unit Tests=================================================================
TEST(Node, DefaultConstructor) {
  Node *node = new Node;
  EXPECT_FLOAT_EQ(0.0f, node->value); // Note: index field is realNumber=float
  EXPECT_EQ(0, node->FE);
  EXPECT_EQ(NULL, node->parent);
  EXPECT_EQ(NULL, node->left);
  EXPECT_EQ(NULL, node->right);
  EXPECT_EQ(NULL, node->next);
  EXPECT_EQ(NULL, node->content);
  delete node;
}
//-----------------------------------------------------------------------------
TEST(Node, ConstuctorUser1) {
  Node *node = new Node(1.0f, 1);
  EXPECT_FLOAT_EQ(1.0f, node->value); // Note: index field is realNumber=float
  EXPECT_EQ(0, node->FE);
  EXPECT_EQ(NULL, node->parent);
  EXPECT_EQ(NULL, node->left);
  EXPECT_EQ(NULL, node->right);
  EXPECT_EQ(NULL, node->next);
  EXPECT_EQ(NULL, node->content);
  delete node;
}
//-----------------------------------------------------------------------------
TEST(Node, ConstuctorUser2) {
  Node *node1 = new Node(1.0f, 1);
  Node *node2 = new Node(2.0f, 1, node1);
  EXPECT_FLOAT_EQ(2.0f, node2->value); // Note: index field is realNumber=float
  EXPECT_EQ(0, node2->FE);
  EXPECT_EQ(node1, node2->parent);
  EXPECT_EQ(NULL, node2->left);
  EXPECT_EQ(NULL, node2->right);
  EXPECT_EQ(NULL, node2->next);
  EXPECT_EQ(NULL, node2->content);
  delete node2;
  delete node1;
}
//-----------------------------------------------------------------------------
TEST(Node, DestructorLastIndex) {
  maxIndex = 1;
  Node *node = new Node(1.0f, maxIndex);
  ArtificialStar *crowStar = new ArtificialStar;
  node->content = (void*) crowStar;
  EXPECT_EQ(0, node->FE);
  EXPECT_EQ(NULL, node->parent);
  EXPECT_EQ(NULL, node->left);
  EXPECT_EQ(NULL, node->right);
  EXPECT_EQ(NULL, node->next);
  EXPECT_EQ(crowStar, node->content);
  delete node;
}
//-----------------------------------------------------------------------------
