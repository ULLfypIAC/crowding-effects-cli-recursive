/*
 * runUnitTests.cc
 *
 * Unit Test
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

#define UNIT_TESTS
#ifdef UNIT_TESTS

//==Includes===================================================================
#include <iostream>
#include "traits.h"
#include "gtest/gtest.h"

//==Gobal variables============================================================
unsigned int  minSetStars;    //  10
unsigned int  maxSetStars;    //   ?
realNumber    maxValue;       //  25.0
unsigned int  maxIndex;       //   4
unsigned int  artfMaxColumn;  //   6
unsigned int  artfMaxRow;     //   ?
unsigned int  realMaxColumn;  //   6
unsigned int  realMaxRow;     //   ?
unsigned int  dispMaxColumn;  //   8
unsigned int  *counterIndexNodes;
//==Functions==================================================================
int main(int argc, char **argv) {
  std::cout << "RECORDATORIO:\n"
            << "  - La ruta desde donde se ejecutan los tests unitarios\n"
            << "    debe tener un directorio llamado 'files'.\n"
            << "  - Se aconseja ejecutar los tests unitarios en el\n"
            << "    directorio raíz del proyecto.\n"
            << "\n";
  std::cout.flush();
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}// main

#endif /* UNIT_TESTS */
