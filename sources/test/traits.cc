/*
 * traits.cc
 *
 * Unit Test for Node class
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include <iostream>
#include "gtest/gtest.h"
#include "traits.h"

//==Unit Tests=================================================================
TEST(Traits, RangeCalcule) {
  maxIndex = 4;
  Epsilon *epsilon = new Epsilon;
  epsilon->index = new realNumber[maxIndex];
  RealStar *realStar = new RealStar;
  realStar->index = new realNumber[maxIndex];
  Range *range = new Range[maxIndex];
  for (unsigned int i(0); i < maxIndex; ++i) {
    realStar->index[i] = (realNumber) i;
    epsilon->index[i] = 0.25f;
    range[i].min = realStar->index[i] - epsilon->index[i];
    range[i].max = realStar->index[i] + epsilon->index[i];
  }
  EXPECT_FLOAT_EQ(-0.25f, range[0].min);
  EXPECT_FLOAT_EQ( 0.25f, range[0].max);
  EXPECT_FLOAT_EQ( 0.75f, range[1].min);
  EXPECT_FLOAT_EQ( 1.25, range[1].max);
  EXPECT_FLOAT_EQ( 1.75, range[2].min);
  EXPECT_FLOAT_EQ( 2.25, range[2].max);
  EXPECT_FLOAT_EQ( 2.75, range[3].min);
  EXPECT_FLOAT_EQ( 3.25, range[3].max);
}
//-----------------------------------------------------------------------------
/*
TEST(Traits, SizeOf) {
  dispMaxColumn = 8;
  std::cout << "sizeof(DispersedStar): " << sizeof(DispersedStar) << '\n';
  std::cout << "sizeof(unsigned int):" << sizeof(unsigned int) << '\n';
  std::cout << "sizeof(unsigned int):" << sizeof(realNumber *) << '\n';
  ASSERT_EQ(8, dispMaxColumn);
}
*/
//-----------------------------------------------------------------------------
