/*
 * Index.cc
 *
 * Unit Test for Index class
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include "gtest/gtest.h"
#include "Index.h"

//==Unit Tests=================================================================
TEST(Index, DefaultConstructor) {
  Index *index = new Index;
  EXPECT_EQ(1, index->topTreeIndex->depth);
  EXPECT_EQ(NULL, index->topTreeIndex->root);
  EXPECT_EQ(NULL, index->topTreeIndex->first);
  EXPECT_EQ(NULL, index->topTreeIndex->last);
  delete index;
  index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, Destructor) {
  Index *index = new Index;
  delete index;
  EXPECT_EQ(NULL, index->topTreeIndex);
  index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert1Star1Indexes) {
  Index *index = new Index;
  maxIndex = 1;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  for (unsigned int i(0); i < maxIndex; ++i) {
    EXPECT_EQ(1, currentTree->counterNodes());
    currentTree = (Tree*) currentTree->root->content;
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert1Star2Indexes) {
  Index *index = new Index;
  maxIndex = 2;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  for (unsigned int i(0); i < maxIndex; ++i) {
    EXPECT_EQ(1, currentTree->counterNodes());
    currentTree = (Tree*) currentTree->root->content;
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert1Star4Indexes) {
  maxIndex = 4;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  indexesValues[2] = 3.0f;
  indexesValues[3] = 4.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  for (unsigned int i(0); i < maxIndex; ++i) {
    EXPECT_EQ(1, currentTree->counterNodes());
    currentTree = (Tree*) currentTree->root->content;
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert2SameStars1Index) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  indexesValues[0] = 1.0f;
  artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  for (unsigned int i(0); i < maxIndex; ++i) {
    EXPECT_EQ(1, currentTree->counterNodes());
    currentTree = (Tree*) currentTree->root->content;
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert2DifferentStars1Index) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  indexesValues[0] = 2.0f;
  artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  for (unsigned int i(0); i < maxIndex; ++i) {
    EXPECT_EQ(2, currentTree->counterNodes());
    currentTree = (Tree*) currentTree->root->content;
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert2DifferentStars2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  indexesValues[0] = 3.0f;
  indexesValues[1] = 4.0f;
  artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  EXPECT_EQ(2, currentTree->counterNodes());
  currentTree->buildComplete();
  Node *it(currentTree->first);
  Node *end(currentTree->last->next);
  ASSERT_FLOAT_EQ(1.0f, currentTree->first->value);
  ASSERT_FLOAT_EQ(3.0f, currentTree->last->value);
  for (; it != end; it = it->next) {
    if (it->value == 1.0f || it->value == 3.0f) {
      SUCCEED();
    } else {
      FAIL();
    }
    currentTree = (Tree*)it->content;
    EXPECT_EQ(1, currentTree->counterNodes());
  }
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, insert2SimilarStars2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  indexesValues[0] = 1.0f;
  indexesValues[1] = 3.0f;
  artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  Tree *currentTree = index->topTreeIndex;
  EXPECT_EQ(1, currentTree->counterNodes());
  currentTree->buildComplete();
  Node *it(currentTree->first);
  Node *end(currentTree->last->next);
  ASSERT_FLOAT_EQ(1.0f, currentTree->first->value);
  ASSERT_FLOAT_EQ(1.0f, currentTree->last->value);
  for (; it != end; it = it->next) {
    if (it->value == 1.0f) {
      SUCCEED();
    } else {
      FAIL();
    }
    currentTree = (Tree*)it->content;
    EXPECT_EQ(2, currentTree->counterNodes());
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------

TEST(Index, buildComplete0Nodes1Indexes) {
  Index *index = new Index;
  maxIndex = 1;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  index->buildTreeComplete();
  EXPECT_EQ(NULL, index->topTreeIndex->root);
  EXPECT_EQ(NULL, index->topTreeIndex->first);
  EXPECT_EQ(NULL, index->topTreeIndex->last);
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, buildComplete0Nodes2Indexes) {
  Index *index = new Index;
  maxIndex = 2;
  index->buildTreeComplete();
  EXPECT_EQ(NULL, index->topTreeIndex->root);
  EXPECT_EQ(NULL, index->topTreeIndex->first);
  EXPECT_EQ(NULL, index->topTreeIndex->last);
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, buildComplete1Nodes1Indexes) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar = new ArtificialStar;
  index->insertTree(indexesValues, artfStar);
  index->buildTreeComplete();
  EXPECT_FLOAT_EQ(indexesValues[0], index->topTreeIndex->root->value);
  EXPECT_FLOAT_EQ(indexesValues[0], index->topTreeIndex->first->value);
  EXPECT_FLOAT_EQ(indexesValues[0], index->topTreeIndex->last->value);
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, buildComplete3Nodes1Indexes) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar1 = new ArtificialStar;
  index->insertTree(indexesValues, artfStar1);
  indexesValues[0] = 2.0f;
  ArtificialStar *artfStar2 = new ArtificialStar;
  index->insertTree(indexesValues, artfStar2);
  indexesValues[0] = 3.0f;
  ArtificialStar *artfStar3 = new ArtificialStar;
  index->insertTree(indexesValues, artfStar3);
  index->buildTreeComplete();
  EXPECT_FLOAT_EQ(2.0f, index->topTreeIndex->root->value);
  EXPECT_FLOAT_EQ(1.0f, index->topTreeIndex->first->value);
  EXPECT_FLOAT_EQ(3.0f, index->topTreeIndex->last->value);
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, buildComplete9Nodes2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  ArtificialStar *artfStar;
  for (unsigned int i(1); i <= 3; ++i) {
    for (unsigned int j(1); j <= 3; ++j) {
      indexesValues[0] = realNumber(i);
      indexesValues[1] = realNumber(j);
      artfStar = new ArtificialStar;
      index->insertTree(indexesValues, artfStar);
    }
  }
  index->buildTreeComplete();
  ASSERT_FLOAT_EQ(2.0f, index->topTreeIndex->root->value);
  ASSERT_FLOAT_EQ(1.0f, index->topTreeIndex->first->value);
  ASSERT_FLOAT_EQ(3.0f, index->topTreeIndex->last->value);
  Node *it(index->topTreeIndex->first);
  Node *end(index->topTreeIndex->last);
  if (end) {
    end = end->next;
    for (; it != end; it = it->next) {
      for (unsigned int j(0); j < 3; ++j) {
        Tree *tree = (Tree*) it->content;
        ASSERT_FLOAT_EQ(2.0f, tree->root->value);
        ASSERT_FLOAT_EQ(1.0f, tree->first->value);
        ASSERT_FLOAT_EQ(3.0f, tree->last->value);
      }
    }
  } else {
    FAIL();
  }
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, findTree1Indexes) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  RealStar *realStar = new RealStar;
  realStar->index[0] = realNumber(2);
  Epsilon *epsilon = new Epsilon;
  epsilon->index[0] = realNumber(0.25);
  ArtificialStar *artfStar;
  for (unsigned int i(1); i <= 3; ++i) {
    artfStar = new ArtificialStar;
    artfStar->id = i;
    indexesValues[0] = realNumber(i);
    index->insertTree(indexesValues, artfStar);
  }
  index->buildTreeComplete();
  FoundStars *foundStars = new FoundStars;
  index->findTree(foundStars, realStar, epsilon);
  ArtificialStar **data(foundStars->data());
  ASSERT_EQ(1, foundStars->size());
  ASSERT_EQ(2, data[0]->id);
  delete[] indexesValues;   indexesValues = NULL;
  delete foundStars;        foundStars = NULL;
  delete[] realStar->index; realStar->index = NULL;
  delete realStar;          realStar = NULL;
  delete[] epsilon->index;  epsilon->index = NULL;
  delete epsilon;           epsilon = NULL;
  delete index;             index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, findTree2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  RealStar *realStar = new RealStar;
  realStar->index[0] = realNumber(2);
  realStar->index[1] = realNumber(2);
  Epsilon *epsilon = new Epsilon;
  epsilon->index[0] = realNumber(0.25);
  epsilon->index[1] = realNumber(0.25);
  ArtificialStar *artfStar;
  unsigned int tmp(1);
  for (unsigned int i(1); i <= 3; ++i) {
    for (unsigned int j(1); j <= 3; ++j, ++tmp) {
      artfStar = new ArtificialStar;
      artfStar->id = tmp;
      indexesValues[0] = realNumber(i);
      indexesValues[1] = realNumber(j);
      index->insertTree(indexesValues, artfStar);
    }
  }
  index->buildTreeComplete();
  FoundStars *foundStars = new FoundStars;
  index->findTree(foundStars, realStar, epsilon);
  ArtificialStar **data(foundStars->data());
  ASSERT_EQ(1, foundStars->size());
  ASSERT_EQ(5, data[0]->id);
  delete[] indexesValues;   indexesValues = NULL;
  delete foundStars;        foundStars = NULL;
  delete[] realStar->index; realStar->index = NULL;
  delete realStar;          realStar = NULL;
  delete[] epsilon->index;  epsilon->index = NULL;
  delete epsilon;           epsilon = NULL;
  delete index;             index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, finishInsert0Stars) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  index->finishInsert();
  ASSERT_TRUE(NULL != index->topListIndex);
  ASSERT_EQ(0, index->topListIndex->size);
  ASSERT_TRUE(NULL == index->topListIndex->elements);
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, finishInsert2SameStars2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar1 = new ArtificialStar;
  artfStar1->id = 1;
  index->insertTree(indexesValues, artfStar1);
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar2 = new ArtificialStar;
  artfStar2->id = 2;
  index->insertTree(indexesValues, artfStar2);
  index->finishInsert();
  List * list;
  list = index->topListIndex;
  ASSERT_TRUE(NULL != list);
  ASSERT_EQ(1, list->size);
  ASSERT_FLOAT_EQ(1.0f, list->elements[0].value);
  list = (List *) index->topListIndex->elements[0].content;
  ASSERT_EQ(1, list->size);
  ASSERT_FLOAT_EQ(2.0f, list->elements[0].value);
  FoundStars *vectorArtfStars = (FoundStars*) list->elements[0].content;
  auto itV = vectorArtfStars->begin();
  ASSERT_TRUE(artfStar1 == (*itV));  // Compare pointers
  ++itV;
  ASSERT_TRUE(artfStar2 == (*itV));  // Compare pointers
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
  //ASSERT_ANY_THROW({delete artfStar1;}); Segmentation fault is not catchable.
  ASSERT_NO_THROW({delete artfStar2;});
}
//-----------------------------------------------------------------------------
TEST(Index, finishInsert2DifferentStars2Indexes) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.0f;
  ArtificialStar *artfStar1 = new ArtificialStar;
  index->insertTree(indexesValues, artfStar1);
  indexesValues[0] = 3.0f;
  indexesValues[1] = 4.0f;
  ArtificialStar *artfStar2 = new ArtificialStar;
  index->insertTree(indexesValues, artfStar2);
  index->finishInsert();
  ASSERT_TRUE(NULL != index->topListIndex);
  ASSERT_EQ(2, index->topListIndex->size);
  ASSERT_FLOAT_EQ(1.0f, index->topListIndex->elements[0].value);
  ASSERT_FLOAT_EQ(3.0f, index->topListIndex->elements[1].value);
  ASSERT_EQ(1, ((List *) index->topListIndex->elements[0].content)->size);
  ASSERT_EQ(1, ((List *) index->topListIndex->elements[1].content)->size);
  List *list;
  list = (List *) index->topListIndex->elements[0].content;
  ASSERT_FLOAT_EQ(2.0f, list->elements[0].value);
  list = (List *) index->topListIndex->elements[1].content;
  ASSERT_FLOAT_EQ(4.0f, list->elements[0].value);
  FoundStars *vectorArtfStars;
  list = (List *) index->topListIndex->elements[0].content;
  vectorArtfStars = (FoundStars*) list->elements[0].content;
  auto itV = vectorArtfStars->begin();
  ASSERT_TRUE(artfStar1 == (*itV));  // Compare pointers
  list = (List *) index->topListIndex->elements[1].content;
  vectorArtfStars = (FoundStars*) list->elements[0].content;
  itV = vectorArtfStars->begin();
  ASSERT_TRUE(artfStar2 == (*itV));  // Compare pointers
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, findList1Indexes1Stars) {
  maxIndex = 1;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  ArtificialStar *artfStar1 = new ArtificialStar;
  artfStar1->id = 1;
  index->insertTree(indexesValues, artfStar1);
  index->finishInsert();
  RealStar *realStar = new RealStar;
  realStar->index[0] = 1.5f;
  Epsilon *epsilons = new Epsilon;
  epsilons->index[0] = 0.5f;
  FoundStars *foundStars = new FoundStars;
  index->findList(foundStars, realStar, epsilons);
  ASSERT_EQ(1, foundStars->size());
  ArtificialStar **data = foundStars->data();
  ASSERT_EQ(1, data[0]->id);
  ASSERT_EQ(artfStar1, data[0]);
  delete foundStars; foundStars = NULL;
  delete epsilons; epsilons = NULL;
  delete realStar; realStar = NULL;
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
  artfStar1 = NULL;
}
//-----------------------------------------------------------------------------
TEST(Index, findList2Indexes3Stars) {
  maxIndex = 2;
  artfMaxColumn = 0;
  realMaxColumn = 0;
  counterIndexNodes = new unsigned int[maxIndex];
  for (unsigned int i = 0; i < maxIndex; ++i) {
    counterIndexNodes[i] = 0;
  }
  Index *index = new Index;
  realNumber *indexesValues = new realNumber[maxIndex];
  indexesValues[0] = 1.0f;
  indexesValues[1] = 2.5f;
  ArtificialStar *artfStar1 = new ArtificialStar;
  artfStar1->id = 1;
  index->insertTree(indexesValues, artfStar1);
  indexesValues[0] = 1.0f;
  indexesValues[1] = 3.5f;
  ArtificialStar *artfStar2 = new ArtificialStar;
  artfStar2->id = 2;
  index->insertTree(indexesValues, artfStar2);
  indexesValues[0] = 2.0f;
  indexesValues[1] = 2.5f;
  ArtificialStar *artfStar3 = new ArtificialStar;
  artfStar3->id = 3;
  index->insertTree(indexesValues, artfStar3);
  index->finishInsert();
  RealStar *realStar = new RealStar;
  realStar->index[0] = 1.5f;
  realStar->index[1] = 3.0f;
  Epsilon *epsilons = new Epsilon;
  epsilons->index[0] = 0.5f;
  epsilons->index[1] = 0.5f;
  FoundStars *foundStars = new FoundStars;
  index->findList(foundStars, realStar, epsilons);
  ASSERT_EQ(3, foundStars->size());
  ArtificialStar **data = foundStars->data();
  ASSERT_EQ(1, data[0]->id);
  ASSERT_EQ(2, data[1]->id);
  ASSERT_EQ(3, data[2]->id);
  ASSERT_EQ(artfStar1, data[0]);
  ASSERT_EQ(artfStar2, data[1]);
  ASSERT_EQ(artfStar3, data[2]);
  delete foundStars; foundStars = NULL;
  delete epsilons; epsilons = NULL;
  delete realStar; realStar = NULL;
  delete[] indexesValues; indexesValues = NULL;
  delete index; index = NULL;
  artfStar1 = NULL;
  artfStar2 = NULL;
  artfStar3 = NULL;
}
//-----------------------------------------------------------------------------
