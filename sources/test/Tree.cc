/*
 * Tree.cc
 *
 * Unit Test for Node class
 * Using Google [C++ Testing Framework] with Test build target
 *
 * [C++ Testing Framework]:https://code.google.com/p/googletest/
 */

//==Includes===================================================================
#include "gtest/gtest.h"
#include "Tree.h"
//==Unit Tests=================================================================
TEST(Tree, DefaultConstructor) {
  Tree * tree = new Tree;
  EXPECT_EQ(1, tree->depth);
  EXPECT_EQ(NULL, tree->root);
  EXPECT_EQ(NULL, tree->first);
  EXPECT_EQ(NULL, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, Constructor) {
  unsigned1Byte depth = 4;
  Tree * tree = new Tree(depth);
  EXPECT_EQ(depth, tree->depth);
  EXPECT_EQ(NULL, tree->root);
  EXPECT_EQ(NULL, tree->first);
  EXPECT_EQ(NULL, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, Destructor) {
  unsigned1Byte depth = 1;
  Tree * tree = new Tree(depth);
  Node * node;
  realNumber value = 2.0f;
  tree->insert(value, node);
  EXPECT_EQ(depth, tree->depth);
  EXPECT_EQ(node, tree->root);
  EXPECT_FLOAT_EQ(value, tree->root->value);
  EXPECT_EQ(NULL, tree->first);
  EXPECT_EQ(NULL, tree->last);
  delete tree;
  EXPECT_EQ(NULL, tree->root);
}
//-----------------------------------------------------------------------------
TEST(Tree, insertDifferentValues) {
  unsigned1Byte depth = 1;
  Tree * tree = new Tree(depth);
  EXPECT_EQ(depth, tree->depth);
  Node * node;
  realNumber value = 1.0f;
  tree->insert(value, node);
  EXPECT_EQ(node, tree->root);
  EXPECT_FLOAT_EQ(value, tree->root->value);
  value = 2.0f;
  tree->insert(value, node);
  EXPECT_FLOAT_EQ(1.0f, tree->root->value);
  value = 3.0f;
  tree->insert(value, node);
  EXPECT_FLOAT_EQ(2.0f, tree->root->value);
  EXPECT_EQ(3, tree->counterNodes());
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, insertSameValue) {
  unsigned1Byte depth = 1;
  Tree * tree = new Tree(depth);
  EXPECT_EQ(depth, tree->depth);
  Node * node;
  realNumber value(1.154);
  tree->insert(value, node);
  EXPECT_EQ(node, tree->root);
  tree->insert(value, node);
  tree->insert(value, node);
  EXPECT_EQ(1, tree->counterNodes());
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildFirst0Nodes) {
  Tree * tree = new Tree;
  ASSERT_EQ(NULL, tree->root);
  ASSERT_EQ(NULL, tree->first);
  tree->buildFirst(tree->root, tree->first);
  EXPECT_EQ(NULL, tree->first);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildFirst3Nodes) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildFirst(tree->root, tree->first);
  EXPECT_EQ(node1, tree->first);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildLast0Nodes) {
  Tree * tree = new Tree;
  ASSERT_EQ(NULL, tree->root);
  ASSERT_EQ(NULL, tree->last);
  tree->buildLast(tree->root, tree->last);
  EXPECT_EQ(NULL, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildLast3Nodes) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildLast(tree->root, tree->last);
  EXPECT_EQ(node3, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildNext0Nodes) {
  Tree * tree = new Tree;
  tree->buildNext(tree->root, tree->last);
  EXPECT_EQ(NULL, tree->root);
  EXPECT_EQ(NULL, tree->first);
  EXPECT_EQ(NULL, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildNext3Nodes) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tmp = new Node;
  tree->last = tmp;
  tree->buildNext(tree->root, tree->last);
  delete tmp;
  EXPECT_EQ(node2, node1->next);
  EXPECT_EQ(node3, node2->next);
  EXPECT_EQ(node3, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, buildComplete) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  EXPECT_EQ(node1, tree->first);
  EXPECT_EQ(node2, node1->next);
  EXPECT_EQ(node3, node2->next);
  EXPECT_EQ(node3, tree->last);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual0Node) {
  Tree * tree = new Tree;
  Node *node;
  tree->buildComplete();
  tree->findGreaterEqual(0.0f, node);
  EXPECT_EQ(NULL, node);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual1NodeLess) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreaterEqual(0.0f, tmp);
  EXPECT_EQ(node1, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual1NodeEqual) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreaterEqual(value1, tmp);
  EXPECT_EQ(node1, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual1NodeGreater) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreaterEqual(2.0f, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual3NodeLess) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreaterEqual(0.0f, tmp);
  EXPECT_EQ(node1, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual3NodeEqual) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreaterEqual(value1, tmp);
  EXPECT_EQ(node1, tmp);
  tree->findGreaterEqual(value2, tmp);
  EXPECT_EQ(node2, tmp);
  tree->findGreaterEqual(value3, tmp);
    EXPECT_EQ(node3, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual3NodeIn) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreaterEqual(1.5f, tmp);
  EXPECT_EQ(node2, tmp);
  tree->findGreaterEqual(2.5f, tmp);
  EXPECT_EQ(node3, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreaterEqual3NodeGreater) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreaterEqual(3.5f, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater0Node) {
  Tree * tree = new Tree;
  Node *node;
  tree->buildComplete();
  tree->findGreater(0.0f, node);
  EXPECT_EQ(NULL, node);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater1NodeLess) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreater(0.0f, tmp);
  EXPECT_EQ(node1, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater1NodeEqual) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreater(value1, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater1NodeGreater) {
  Tree * tree = new Tree;
  Node *node1, *tmp;
  realNumber value1=1.0f;
  tree->insert(value1, node1);
  tree->buildComplete();
  tree->findGreater(2.0f, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater3NodeLess) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreater(0.0f, tmp);
  EXPECT_EQ(node1, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater3NodeEqual) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreater(value1, tmp);
  EXPECT_EQ(node2, tmp);
  tree->findGreater(value2, tmp);
  EXPECT_EQ(node3, tmp);
  tree->findGreater(value3, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater3NodeIn) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreater(1.5f, tmp);
  EXPECT_EQ(node2, tmp);
  tree->findGreater(2.5f, tmp);
  EXPECT_EQ(node3, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
TEST(Tree, findGreater3NodeGreater) {
  Tree * tree = new Tree;
  Node *node1, *node2, *node3, *tmp;
  realNumber value1=1.0f, value2=2.0f, value3=3.0f;
  tree->insert(value1, node1);
  tree->insert(value2, node2);
  tree->insert(value3, node3);
  tree->buildComplete();
  tree->findGreater(3.5f, tmp);
  EXPECT_EQ(NULL, tmp);
  delete tree;
}
//-----------------------------------------------------------------------------
