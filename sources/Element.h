/**
 * Element.h
 *
 * Fichero que define la declaración de la clase elemento (Element) de una
 * estructura de lista (List).
 *
 */

#ifndef ELEMENT_H_
#define ELEMENT_H_

//==Includes===================================================================
#include "traits.h"
#include "Node.h"

//==Classes====================================================================
class List;
//-----------------------------------------------------------------------------
class Element {
  friend class List;
  public: // Members
    realNumber value;
    unsigned1Byte depth;
    nodeContent *content;
  public: // Special members
    Element();                          // Default constructor
    virtual ~Element();                 // Destructor
    Element(const Element&);            // Copy constructor
    Element& operator=(const Element&); // Copy assignment
    Element(Element&&);                 // Move constructor
    Element& operator=(Element&&);      // Move assignment
    Element(const realNumber&);         // Constructor
    Element(Node *&node);               // Constructor
    void copyFromNode(Node *&node);
  public: // Overloaded operators
    friend inline bool operator<(const Element &A, const Element &B)
    {
      return A.value < B.value;
    }

    friend inline bool operator>(const Element &A, const Element &B)
    {
      return A.value > B.value;
    }

    friend inline bool operator<=(const Element &A, const Element &B)
    {
      return A.value <= B.value;
    }

    friend inline bool operator>=(const Element &A, const Element &B)
    {
      return A.value >= B.value;
    }

    friend inline bool operator==(const Element &A, const Element &B)
    {
      return A.value == B.value;
    }

    friend inline bool operator!=(const Element &A, const Element &B)
    {
      return A.value != B.value;
    }

  private:
    void destroy();
};
//-----------------------------------------------------------------------------
#endif /* ELEMENT_H_ */
