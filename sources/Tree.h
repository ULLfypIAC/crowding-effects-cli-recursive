/**
 * Tree.h
 *
 * Fichero que define las cabeceras de la clase árbol (Tree).
 * Utiliza como clase amiga, la clase lista (List). De esta manera puede acceder
 * a los atributos privados de ésta si en el futuro de decide cambiar el ámbito
 * propuesto en esta implementación.
 *
 */

#ifndef TREE_H_
#define TREE_H_

//==Includes===================================================================
#include "traits.h"
#include "Node.h"

//==Classes====================================================================
class List;
//-----------------------------------------------------------------------------
enum Orientation {Left, Rigth};
//-----------------------------------------------------------------------------
class Tree {
  friend class List;
  public: // Members
    unsigned1Byte depth;
    Node *root;
    Node *first;
    Node *last;
  public: // Special methods
   Tree();                            // Default constructor
   virtual ~Tree();                   // Destructor
   Tree(const unsigned1Byte &depth);  // Constructor
  public: // Public methods
   bool insert(const realNumber &value, Node *&node);
   void findGreaterEqual(const realNumber &value, Node *&node);
   void findGreater(const realNumber &value, Node *&node);
   const unsigned int counterNodes();
   void buildComplete();
   void buildFirst(Node *node, Node *&modify);
   void buildLast(Node *node, Node *&modify);
   void buildNext(Node *node, Node *&previous);
  private: // Private methods
   void balance(Node *node, Orientation, bool);
   void RSI(Node *node);
   void RSD(Node *node);
   void RDI(Node *node);
   void RDD(Node *node);
   void trunctate(Node* &);
   void counterNodesRecursive(Node *node, unsigned int &counter);
};
//-----------------------------------------------------------------------------
#endif /* TREE_H_ */
