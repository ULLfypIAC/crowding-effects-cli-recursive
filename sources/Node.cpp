/**
 * Node.cpp
 *
 * Fichero que define los métodos de la clase nodo (Node), que es el elemento
 * básico de la estructura de árbol (Tree).
 *
 */

//==Includes===================================================================
#include "Node.h"
#include "Tree.h"

//==Special members:[http://www.cplusplus.com/doc/tutorial/classes2/]===========
/**
 * Default constructor
 */
Node::Node() :
    value(0),
    depth(0),
    FE(0),
    parent(NULL),
    left(NULL),
    right(NULL),
    next(NULL),
    content(NULL) {
  //std::cout << "Node[" << this << "]\n";
}
//-----------------------------------------------------------------------------
/**
 * Destructor
 */
Node::~Node() {
  //std::cout << "~Node[" << this << "]\n";
  destroy();
  value = realNumber(0);
  depth = unsigned1Byte(0);
  FE = signed1Byte(0);
  parent = NULL;
  left = NULL;
  right = NULL;
  next = NULL;
}
////-----------------------------------------------------------------------------
///**
// * Copy constructor
// * @param node
// */
//Node::Node(const Node &other) :
//    value(node.value),
//    depth(node.depth),
//    FE(node.FE),
//    parent(NULL),
//    left(NULL),
//    right(NULL),
//    next(NULL) {
//  if (node.content) {
//    if (depth < maxIndex) {
//      content = new Tree(*((Tree *) node.content));
//    } else {
//      content = new ArtificialStar(*((ArtificialStar *) node.content));
//    }
//  }
//}
////-----------------------------------------------------------------------------
///**
// * Copy assignment
// * @param node
// * @return
// */
//Node& Node::operator=(const Node& other) :
//    value(node.value),
//    depth(node.depth),
//    FE(node.FE) {
//  destroy();
//  parent = new Node(node.parent);
//  left = new Node(node.left);
//  right = new Node(node.right);
//  next = new Node(node.next);
//  // content
//  if (depth < maxIndex) {
//    content = new Tree(*((Tree*) node.content));
//  }
//  else {
//    content = new ArtificialStar(*((ArtificialStar *) node.content));
//  }
//  return *this;
//}
////-----------------------------------------------------------------------------
///**
// * Move constructor
// * @param node
// */
//Node::Node(Node&& other) :
//    value(node.value),
//    depth(node.depth),
//    FE(node.FE),
//    parent(node.parent),
//    left(node.parent),
//    right(node.parent),
//    next(node.next) {
//  destroy();
//  content = node.content;
//  node.content = NULL;
//  node.parent = NULL;
//  node.left = NULL;
//  node.right = NULL;
//  node.next = NULL;
//}
////-----------------------------------------------------------------------------
///**
// * Move assignment
// * @param node
// * @return
// */
//Node& Node::operator=(Node&& other) :
//    value(node.value),
//    depth(node.depth),
//    FE(node.FE),
//    parent(node.parent),
//    left(node.parent),
//    right(node.parent),
//    next(node.next) {
//  destroy();
//  content = node.content;
//  node.content = NULL;
//  node.parent = NULL;
//  node.left = NULL;
//  node.right = NULL;
//  node.next = NULL;
//  return *this;
//}
////-----------------------------------------------------------------------------
/**
 * Constructor que crea el nodo con un valor y una profundidad específico.
 * @param value Valor del nodo.
 * @param depth Profundidad del nodo.
 */
Node::Node(const realNumber& value,
           const unsigned1Byte& depth) :
    value(value),
    depth(depth),
    FE(0),
    parent(NULL),
    left(NULL),
    right(NULL),
    next(NULL),
    content(NULL) {
}
//-----------------------------------------------------------------------------
/**
 * Constructor que crea el nodo con un valor, profundidad y padre específico.
 * @param value Valor del nodo.
 * @param depth Profundidad del nodo.
 * @param parent Padre del nodo.
 */
Node::Node(const realNumber& value,
           const unsigned1Byte& depth,
           Node *parent) :
    value(value),
    depth(depth),
    FE(0),
    parent(parent),
    left(NULL),
    right(NULL),
    next(NULL),
    content(NULL) {
}
//==Private Methods============================================================
/**
 * Liberación del campo content.
 * El campo depth indica el tipo de destructor invocar.
 */
void Node::destroy() {
  if (content) {
    // /*
    // Destruir siguiente nivel de índice.
    if (depth < maxIndex) {
      delete ((Tree *) content);
    }
    // */
    ///*
    // Destruir estrella apuntada.
    else {
      FoundStars *tmp = (FoundStars *) content;
      tmp->clear();
      delete tmp;
      tmp = NULL;
    }
    // */
    content = NULL;
  }
}
//==Overloaded operators:[http://www.cplusplus.com/doc/tutorial/templates/]====
/**
 * Sobrecarga del operador <.
 * En el futuro será mejor utilizar traits::compareNumbers(...)
 *
 * @param nodeA Nodo A.
 * @param nodeB Nodo B.
 * @return true, si nodeA < nodeB. false, en otro caso.
 */
bool operator <(const Node& nodeA, const Node& nodeB) {
  return nodeA.value < nodeB.value;
}
//-----------------------------------------------------------------------------
/**
 * Sobrecarga del operador del flujo estándar de salida para un nodo.
 * @param out FLujo estándar de salida.
 * @param node Nodo.
 * @return FLujo estándar de salida (out).
 */
std::ostream& operator<<(std::ostream &out, const Node &node) {
  out << "Node[" << &node << "]:"
      << "v(" << node.value << "),"
      << "d(" << node.depth << "),"
      << "c(" << node.content << ");\n";
  return out;
}
//-----------------------------------------------------------------------------
