# Google C++ Testing Framework #
----
  - [Home]
  - [Readme]
  - [Download]
  - [Wiki]

You can read this file in http://dillinger.io/

[Home]:     http://code.google.com/p/googletest/
[Readme]:   http://code.google.com/p/googletest/source/browse/trunk/README
[Download]: https://code.google.com/p/googletest/downloads/list
[Wiki]:     https://code.google.com/p/googletest/w/list

## Getting started with Google Test in Eclipse CDT on Linux ##

Setting up your development tools is an important first step in TDD, and its not always as easy as one could think.

I found that setting up my C++ development environment a little bit troublesome, and there wasn’t that many good how-to:s out there..

Here is How i get up and running with Eclipse CDT and google test.

### Install Eclipse with Unit test support ###
 1. Install Eclipse CDT from https://www.eclipse.org/cdt/
 2. In Eclipse go to Help -> Install New Software...
 3. Select default Eclipse Update site in the Work with field, for me its Luna - http://download.eclipse.org/releases/luna
 4. Install package **C/C++ Unit Testing Support** under Programming Languages, you will need to restart your IDE once the installation is done. 
    This package is in _org.eclipse.cdt.testsrunner.feature.feature.group_

### Get and compile Google Test ###
 1. Download google test from https://code.google.com/p/googletest/
 2. Unpack google test somewhere on your computer. Suppose you put Google Test in directory **${GTEST_DIR}**
 ```sh
 $ unzip gtest-*.zip -d ${GTEST_DIR}
 ```
 3. Create a directory to hold the build output. Suppose you create directory **${GTEST_BUILD}**.
 ```sh
 $ mkdir ${GTEST_BUILD}
 ```
 4. Go into your build directory **${GTEST_BUILD}**
 ```sh
 $ cd ${GTEST_BUILD}
 ```
 5. Run [CMake] build script to generate native build scripts.  
```sh
 $ cmake ${GTEST_DIR}  # Generate native build scripts.
 ```
 6. Compile native scripts.
 ```sh
 $ make
 ```
[CMake]:http://www.cmake.org/

### Configure a new C++ Project ###
 1. Create a new C++ project - **${ProjName}** - in Eclipse using *Linux GCC*: *`File > New > C++ Project`*.
 2. Create a new build configuration: 
  - *Right-click* on **${ProjName}** folder.
  - Go to *`Properties`*.
  - Click on the *`C/C++ Build`*.
  - Click the *`Manage Configurations…`* button.
  - Click *`New…`* button.
  - Name it **Test**.
  - _Copy setting from: Exist configuration_: **Debug**.
  - *`Set Active`* **Test** build configuration.
 3. Compile with ISO C++11:
  -  Go to *`${ProjName} > Properties > C/C++ Build > Settings`*.
  - Click on the *`Tool Settings`* tab.
  - Click on the *`CCC C++ Compiler > Miscellaneous`*.
  - Append **-std=c++11** into *`Other flags`* field.
 4. Add Symbols to C++11:
  -  Go to *`${ProjName} > Properties > C/C++ General > Paths and Symbols`*.
  -  Click on the *`Symbols`* tab.
  -  Select *`GNU C++`* language.
  -  *`Add…`* new symbol: *Name* = **__cplusplus**, *Value* = **201103L**.
 5. Enable Indexer:
  -  Go to *`${ProjName} > Properties > C/C++ General > Indexer`*.
  -  Check *`Enable indexer`*.

### Use Google C++ Testing Framework in **${ProjName}** ###
 1. Create a source folder - called **test/src** - to hold your tests:
  -  Go to *`${ProjName} > New > Source Folder > test/src`*
 2. It is usually a good idea to not include your tests in your *Debug* and *Release* build:
  -  *Right-click* on *`${ProjName}/test/src > Resource Configurations > Exclude from Build…`*
  -  Check exclude the folder from **Debug** and **Release** build configuration.
  -  _Note_ that your **${ProjName}/test/src** folder _is not displayed as source folder_. This is normal and don't affect **Test** build configuration.
 3. Avoid multiple _main()_ problem:
  -  Go to *`${ProjName}/src`*.
  -  *Right-click* on *`${file with main function} > Resource Configurations > Exclude from Build…`*
  -  Check exclude the folder from **Test** build configuration.
 4. Move **${GTEST_BUILD}** into **${ProjName}**:
```sh
$ mkdir -p ${ProjName}/test/build/gtest
$ mv ${GTEST_BUILD}/* ${ProjName}/test/build/gtest/
```
 5. Copy **${GTEST_BUILD}**_/include_ into **${ProjName}**:
```sh
$ cp -R ${GTEST_BUILD}/include ${ProjName}/test/
```
 6. Add Google C++ Testing Framework _headers_ and _sources_ in **${ProjName}/test/src** folder:
  -  Go to *`${ProjName}/test/src > Properties > C/C++ Build > Settings`*.
  - Click on the *`Tool Settings`* tab.
  - Click on the *`CCC C++ Compiler > Includes`*.
  - Add new *`Include paths (-I) > Workspace…`*.
  - Find _${workspace_loc:/**${ProjName}/test/include**}_.
  - Add new *`Include paths (-I) > Workspace…`*.
  - Find _${workspace_loc:/**${ProjName}/src**}_.
 7. Setting up **GCC C++ Linker**:
  -  Go to *`${ProjName} > Properties > C/C++ Build > Settings`*.
  - Click on the *`Tool Settings`* tab.
  - Click on the *`CCC C++ Compiler > Includes`*.
  - Add new *`Include paths (-I) > Workspace…`*.
  - Find _${workspace_loc:/**${ProjName}/test/include**}_.
 8. Setting up **GCC C++ Linker**:
  -  Go to *`${ProjName} > Properties > C/C++ Build > Settings`*.
  - Click on the *`Tool Settings`* tab.
  - Click on the *`CCC C++ Linker > Libraries`*.
  - Add new *`Library search paths (-L) > Workspace…`*.
  - Find _${workspace_loc:/**${ProjName}/test/build/gtest**}_.
  - Add new *`Libraries (-l) > Workspace…`*.
  - Type **pthread**. Google Test uses pthread.
  - Add new *`Libraries (-l) > Workspace…`*.
  - Type **gtest**.
 9. If Eclipse still shows errors, you can try to rebuild your index.

### Create a launch configuration with _C/C++ Unit Testing Support_ ###
 1.  Write your first test into **${ProjName}/test/src** folder, like this:
  -  **${ProjName}/test/src**/mainTest.cpp
  ```cpp
  #include "gtest/gtest.h"
  int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
  }// main
  ```
 2. Active **Test** build configuration.
 3. Build your project.
 4. Setting up Run configurations:
  - Go into the *`Run > Run configurations…`* menu.
  - Double click on *`C/C++ Unit`* to create a unit test configuration.
  - In *`Main`* tab select your project and the binary you just built.
  - Go to *`C/C++ Testing`* tab.
  - Select the *`Google Tests Runner`* option.
 5. Finally, click *`Run`*. Your tests should now run.
----

## Resources ##
 1. [How to get started with Google Test in Eclipse CDT on Linux].
 2. [How to avoid multiple _main()_ problem with GTest].

[How to get started with Google Test in Eclipse CDT on Linux]: https://bjornarnelid.wordpress.com/2014/03/10/how-to-get-started-with-google-test-in-eclipse-cdt-on-linux/
[How to avoid multiple _main()_ problem with GTest]: http://pezad-games.googlecode.com/svn/trunk/GTestTest/HOWTO