var NAVTREE =
[
  [ "Crowding Effects CLI CPU", "index.html", [
    [ "Clases", null, [
      [ "Lista de clases", "annotated.html", "annotated" ],
      [ "Índice de clases", "classes.html", null ],
      [ "Miembros de las clases", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ],
        [ "Funciones relacionadas", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Archivos", null, [
      [ "Lista de archivos", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';