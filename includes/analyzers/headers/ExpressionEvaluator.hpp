/*
 * ExpressionEvaluator.hpp
 */

#ifndef EXPRESSION_EVALUATOR_H_
#define EXPRESSION_EVALUATOR_H_

//==Includes===================================================================
#include <string>
#include <type_traits>
#include "exprtk.hpp"


//==Classes====================================================================
 /*
  * Evaluador de expresiones, utilizado en tres tiempos para optimizar el tiempo
  * de las evaluaciones.
  *
  * En un primer lugar, se debe crear el objeto que servirá para realizar
  * multiples evalucaciones de las mismas expresiones con diferentes valores
  * de las variables.
  *
  * En segundo lugar, se de comprobar que las expresiones son factibles de ser
  * evaluadas.
  *
  * Finalmente, se debe proporcionar el valor de las variables de las expresiones
  * y la ubicación donde se almacenarán los resultados de evaluar las expresiones.
  *
  */
template <class T>
class ExpressionEvaluator {
  private: // Members
    /** Número máximo de columnas permitidas en la expresión */
    unsigned int             maxVariables;
    /** Puntero a un vector que debe ser lineal en memoria de tipo T,
     *  que se usará para indicar la ubicación de donde se encuentran
     *  los valores variables de las expresiones (c#).
     */
    T                       *column;
    /** Número máximo de expresiones */
    unsigned int             maxExpresions;
    /** Puntero a un vector que debe ser lineal en memoria de tipo string,
     *  donde se almacenarán las expresiones a evaluar */
    std::string             *expression_string;
    /** Tabla de símbolos de las posibles variables que puedan contener las
     *  expresiones. Utiliza las librería:
     *  http://www.partow.net/programming/exprtk/
     */
    exprtk::symbol_table<T>  symbol_table;
    /** Puntero a un vector que debe ser lineal en memoria de tipo
     *  exprtk::expression<T>, en que se deben almacenar las expresiones listas
     *  para ser evaluadas, usando la librería:
     *  http://www.partow.net/programming/exprtk/
     */
    exprtk::expression<T>   *expression;
    /** Puntero a un vector que debe ser lineal en memoria de tipo
     *  exprtk::parser<T>, que utilizará para analizar y compilar cada una
     *  de las expresiones contenidas en la cadena de caracteres (std::string)
     *  y asignadas a la expresión (exprtk::expression<T>).
     *  Uso de la librería:
     *  http://www.partow.net/programming/exprtk/
     */
    exprtk::parser<T>       *parser;
    /** Indicador booleano que indica si todas las expresiones son o no
     *  factibles de ser utilizadas.
     */
    bool                     compiled;
  public: // Special members
    //==Special members:[http://www.cplusplus.com/doc/tutorial/classes2/]==========
    /**
     * Destructor que elimina la estructura utilizada.
     */
    virtual ~ExpressionEvaluator() {
      delete[] parser;
      delete[] expression;
      delete[] column;
    }
    //-----------------------------------------------------------------------------
    /**
     * Constructor que establece los elementos imprescindibles para evaluar una
     * lista de expresiones.
     *
     * Tiene como tipo de dato, nombrado como T, que puede ser de tipo:
     * float, double o long double.
     *
     * @param  max              Número máximo de columnas permitidas en la expresión.
     * @param  expressionString Vector lineal en memoria de tipo string, donde se
     *                          encuentran las expresiones a evaluar.
     *
     */
    ExpressionEvaluator(const unsigned int  &maxVariables,
                        const unsigned int  &maxExpresions,
                        std::string  *&expression_string)
          /* Se debe comprobar que el tipo de dato T:
             sea float, double o long double y no constante.
        :
          max               (max),
          column            (new T[max]),
          expression_string (expression_string),
          expression        (new exprtk::expression<T>[max]),
          parser            (new exprtk::parser<T>[max]), 
          compiled          (true)
          */
    {
      // Comprobar tipo de dato de la plantilla
      bool type = std::is_floating_point<T>::value && !std::is_const<int>::value;
      if (!type) {
        // Inicializar atributos vacíos.
        this->maxVariables      = 0;
        this->column            = NULL;
        this->maxExpresions     = 0;
        this->expression_string = NULL;
        this->expression        = NULL;
        this->parser            = NULL;
        this->compiled          = false;
      }
      else {
        // Inicializar atributos correctamente.
        this->maxVariables      = maxVariables;
        this->column            = new T[maxVariables];
        this->maxExpresions     = maxExpresions;
        this->expression_string = expression_string;
        this->expression        = new exprtk::expression<T>[maxExpresions];
        this->parser            = new exprtk::parser<T>[maxExpresions];
        this->compiled          = true;
        // Registrar variables y constantes.
        for (unsigned int i(0); i < maxVariables; i++) {
          symbol_table.add_variable("c" + std::to_string(i+1), column[i], false);
        }
        symbol_table.add_constants();
        // Añadir tabla de símbolos y compilar expresión
        for (unsigned int i(0); i < maxExpresions; i++) {
          expression[i].register_symbol_table(symbol_table);
          compiled &= parser[i].compile(expression_string[i], expression[i]);
        }
      }
    }
    //-----------------------------------------------------------------------------
    //==Public Methods=============================================================
    /**
     * Indica si todas las expresiones son o no factibles de ser evaluadas.
     *
     * @return true     Si todas las expresiones son factibles de ser evaluadas.
     *         false    En caso contrario.
     */
    bool ready() const {
      return compiled;
    }
    //-----------------------------------------------------------------------------
    /**
     * Evalua la lista de expresiones contenida en el atributo expression_string,
     * cuyas variables están contenidas en el parámetro column.
     * Asigna los valores obtenidos de evaluar las expresiones si todos los
     * resultados dieron un resultado válido.
     *  Indica si la operación se ha podido realizar o no con éxito.
     *
     * La función tiene un tipo de dato nombrado como T, que puede ser de tipo:
     * float, double o long double.
     *
     * @param  column          Puntero a un vector lineal en memoria de tipo T,
     *                         donde se encuentran los valores de las variable 'c#'
     *                         de las expresiones.
     * @param  index           Puntero a un vector lineal en memoria de tipo T,
     *                         donde se desea almacenar los resultados de las 
     *                         expresiones.
     * @return true            Si todas las expresiones dan un resultado válido.
     *         false           En caso contrario.
     */
    bool evaluate(T *&column, T *&index) {
      bool retVal(true);
      T value;
      // Registrar nuevas variables.
      for (unsigned int i(0); i < maxVariables; i++) {
        this->column[i] = column[i];
      }
      // Evaluar cada expresión.
      for (unsigned int i(0); retVal && i < maxExpresions; i++) {
        value = expression[i].value();
        retVal &= exprtk::details::numeric::is_valid(value);
        if (retVal) {
          index[i] = value;
        }
      }
      return retVal;
    }
};
//==Other Functions============================================================
/*
// Ejemplo de uso:
#include <iostream>
int main() {
  // Crear estructura necesaria.
  unsigned int max(4);
  float       *column = new float[max];
  float       *index  = new float[max];
  std::string *strExp = new std::string[max];
  
  // Inicializar y visualizar en terminal estructura creada.
  std::cout << "column : ";
  for (unsigned int i(0); i < max; i++) {
    column[i] = float(i);
    std::cout << column[i]<< " ";
  }
  std::cout << std::endl;
  std::cout << "expressions: ";
  for (unsigned int i(0); i < max; i++) {
    strExp[i] = "c" + std::to_string(i+1) + " + 10";
    std::cout << "\"" << strExp[i] << "\" ";
  }
  std::cout << std::endl;

  // Evaluador de expresiones en 3 tiempos:
  // 1.- Crear evaluador.
  ExpressionEvaluator<float> evalator(max, strExp);
  // 2.- Comprobar que está listo para evaluar.
  bool r2, r1 = evalator.ready();
  std::cout << "ready: " << std::boolalpha << r1 << std::endl;
  // 3.- Realizar evalución de todas las expresiones
  if (r1) {
    r2 = evalator.evaluate(column, index);
    std::cout << "evaluate: " << std::boolalpha << r2 << std::endl;
  }

  // Mostrar resultados
  std::cout << "index : ";
  for (unsigned int i(0); r2 && i < max; i++) {
    std::cout << index[i] << " ";
  }
  std::cout << std::endl;
  
  return 0;
}
//*/
//-----------------------------------------------------------------------------
#endif /* EXPRESSION_EVALUATOR_H_ */
