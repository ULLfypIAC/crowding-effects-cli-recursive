/**
|-----------------------------------------------------------------------------|
| Overall ANTLR Grammar File Structure                                        |
| [https://theantlrguy.atlassian.net/wiki/display/ANTLR3/Grammars]            |
|-----------------------------------------------------------------------------|
| «grammarType» grammar name;                                                 |
|   «optionsSpec»     = options { ... }                                       |
|   «tokensSpec»      = tokens { ... }                                        |
|   «attributeScopes» = «@action-scope-name::action-name» { ... }             |
|   «actions»                                                                 |
|   «rules»                                                                   |
|_____________________________________________________________________________|
|   «grammarType»         = ( lexer | parser | tree | <empty> )               |
|   «optionsSpec»         = options { ... }                                   |
|   «tokensSpec»          = tokens { ... }                                    |
|   «attributeScopes»     =                                                   |
|   «actions»             = @«action-scope-name»::«action-name» { ... }       |
|    «action-scope-name» = ( lexer | parser | tree | <empty> = parser )       |
|    «action-name»       = ( header | members | rulecatch | synpredgate | ...)|
|-----------------------------------------------------------------------------|
*/
grammar ExpressionAnalyzer;

//= Options ==================================================================|
options {
  language = Cpp;
  //backtrack = true;
  //k = 2;
}// options

//= Lexer Sections ===========================================================|
@lexer::traits {
  /**
   * Esta sección es necesaria cuando el lenguaje generado es C++.
   */
  class ExpressionAnalyzerLexer;
  class ExpressionAnalyzerParser;
  /**
   * Objeto que permite modificar el comportamiento por defecto
   * del ANTLR para C++.
   *
   * Si no se desea modificar el comportamiento por defecto, del 
   * analizador basta con borrar esta clase del 'typedef' de más
   * abajo.
   *
   * Para mayor información debes ver el fichero "antlr3traits.hpp".
   */
  template<class ImplTraits>
  class UserTraits : public antlr3::CustomTraitsBase<ImplTraits> {
    public:
      /**
       * Función invocada cuando existe algún error léxico.
       * 
       */
      static void displayRecognitionError(const std::string &str) {
        std::ofstream out("ExpressionAnalyzer.log",
                          std::ofstream::out | std::ofstream::app);
        out << str;
        out.close();
      }
  };
  typedef antlr3::Traits<ExpressionAnalyzerLexer,
                         ExpressionAnalyzerParser,
                         UserTraits>
          ExpressionAnalyzerLexerTraits;
}// traits
@lexer::namespace { ExpressionAnalyzer }
@lexer::init {
}//init
@lexer::header {
  /**
   *
   */
}//header
@lexer::includes {
  #include <fstream>
}// includes
@lexer::members {
}// members

//= Parser Sections ==========================================================|
@parser::traits {
  /**
   * Esta sección es necesaria cuando el lenguaje generado es C++.
   */
  typedef ExpressionAnalyzerLexerTraits ExpressionAnalyzerParserTraits;
}// traits
@parser::namespace { ExpressionAnalyzer }
@parser::init {
}//init
@parser::header {
  /**
   *
   */
}//header
@parser::includes {
  #include <iostream>
  #include <string>
  #include "ExpressionAnalyzerLexer.hpp"

  namespace ExpressionAnalyzer {
   /**
    * Comprueba que la cadena de caracteres es una expresión perteneciente
    * a la gramática e indica si la expresión es factible o no lo es.
    * @param  text      Cadena de caracteres que se desea comprobar si es una
    *                   expresión factible.
    * @param  max       Número máximo de columnas permitidas en la expresión.
    * @param  whereis   Cadena de caracteres que indica la procedencia del
    *                   parámetro text.
    * @param  line      Número de línea donde se encuentra el parámetro text.
    * @return true      Si la expresión pertenece a la gramática y es factible.
    *         false     En caso contrario.
    */
    bool isFactible(const std::string  &text,
                    const unsigned int &max,
                    const std::string  &whereis,
                    const unsigned int &line);
  }
}// includes
@parser::postinclude {
}//postinclude
@parser::members {
  /**
   *  Número máximo de columnas permitidas en la gramática de expresiones.
   */
  unsigned int maxColumn;
   /**
    * Comprueba que la cadena de caracteres es una expresión perteneciente
    * a la gramática e indica si la expresión es factible o no lo es.
    * @param  text      Cadena de caracteres que se desea comprobar si es una
    *                   expresión factible.
    * @param  max       Número máximo de columnas permitidas en la expresión.
    * @param  whereis   Cadena de caracteres de donde procede el parámetro 
    *                   text.
    * @param  line      Número de línea donde se encuentra el parámetro text.
    * @return true      Si la expresión pertenece a la gramática y es factible.
    *         false     En caso contrario.
    */
  bool isFactible(const std::string  &text,
                  const unsigned int &max,
                  const std::string  &whereis,
                  const unsigned int &line) {
    bool retVal = false;
    maxColumn = max;
    ExpressionAnalyzerLexer::InputStreamType input((ANTLR_UINT8*)text.c_str(),
                                                    ANTLR_ENC_8BIT,
                                                    text.length(),
                                                   (ANTLR_UINT8*)whereis.c_str());
    input.set_line((ANTLR_UINT32) line);
    ExpressionAnalyzerLexer lexer(&input);
    ExpressionAnalyzerParser::TokenStreamType tokenStream(ANTLR_SIZE_HINT,
                                                       lexer.get_tokSource());
    ExpressionAnalyzerParser parser(&tokenStream);
    retVal = parser.run();
    retVal &= lexer.get_lexstate()->get_errorCount() == 0;
    return retVal;

  }
  /**
   * Comrprueba la fatibilidad de una columna.
   * @param  text   Texto del token columna (COLUMN).
   * @return true   Si el número de la columna es mayor que cero (> 0) 
   *                y menor o igual que el número de columnas máxima permitida
   *                (>= maxColumn).
   *         false  En caso contrario.
   */
  inline bool factibleColumn(const std::string &text) {
    unsigned int indexColumn = std::stoi(text.substr(1));
    return indexColumn > 0 && --indexColumn < maxColumn;
  }

}// members

//= Grammar Rules ============================================================|
run
returns [bool ok]
@init {$ok=false;}
  : expression {$ok=$expression.ok;} EOF
  ;

expression
returns [bool ok]
@init {$ok=false;}
  : (binary {$ok=$binary.ok;})
  ;

binary
returns [bool ok]
@init {$ok=false;}
  : (op1=unary         {$ok =$op1.ok;})
      ( '+' op2=unary  {$ok&=$op2.ok;}
       |'-' op2=unary  {$ok&=$op2.ok;}
       |'*' op2=unary  {$ok&=$op2.ok;}
       |'/' op2=unary  {$ok&=$op2.ok;}
       |'^' op2=unary  {$ok&=$op2.ok;}
      )*
  ;

unary
returns [bool ok]
@init {$ok=false;}
  : (sign)? primary {$ok=$primary.ok;}
  ;

sign
  : '+'
  | '-'
  ;

/**
 *  Si el lenguaje de destino es C++ (Cpp) habrá un error en el
 *  proceso de compilación de los ficheros generados. Para solu-
 *  cionarlo debes declarar cómo constante (const) la declarión
 *  de la variable 'columnNumber', quedando de esta manera:
 *
 *  ```
 *  ...
 *  const ImplTraits::CommonTokenType* column = NULL;
 *  ...
 *  ```
 *  
 */
primary
returns [bool ok]
@init {$ok=false;}
  : column=COLUMN       {$ok=factibleColumn($column.text);}
  | INT                 {$ok=true;}
  | FLOAT               {$ok=true;}
  | '(' expression ')'  {$ok=true;}
  | 'pi'                {$ok=true;}
  | 'epsilon'           {$ok=true;}
  | function            {$ok=$function.ok;}
  ;

/**
 * Algunas funciones soportadas por [C++ Mathematical Expression Library]:
 * http://www.partow.net/programming/exprtk/index.html
 *
 * La expresión debe estar en minúscula (lower case) para que el análisis
 * sea satisfactorio.
 *
 * Funciones soportadas: abs, acos, acosh, asin, asinh, atan, atanh,
 *                       ceil, cos, cosh, exp, expm1, floor, log, log10,
 *                       log2, log1p, round, sin, sinc, sinh, sec, csc,
 *                       sqrt, tan, tanh, cot, rad2deg, deg2rad, deg2grad,
 *                       grad2deg, sgn, not, erf, erfc, ncdf, frac, 
 *                       trunc, atan2, mod, logn, pow, root, roundn,
 *                       equal, not_equal, hypot, shr, shl, clamp,
 *                       iclamp, inrange, sum, mul, min, max, avg.
 *
 *  - sum: sólo soportada para elementos que no son vectores
 *  - Las funciones trigonométricas hacen sus cálculos asumiendo que sus
 *    valores están expresado en radianes.
 *
 * Funciones no soportadas: swap.
 *
 */
//*
function
returns [bool ok]
@init {$ok=false;}
  // Funciones generales de un argumento
  : 'abs'       '(' expression ')' {$ok=$expression.ok;}
  | 'acos'      '(' expression ')' {$ok=$expression.ok;}
  | 'acosh'     '(' expression ')' {$ok=$expression.ok;}
  | 'asin'      '(' expression ')' {$ok=$expression.ok;}
  | 'asinh'     '(' expression ')' {$ok=$expression.ok;}
  | 'atan'      '(' expression ')' {$ok=$expression.ok;}
  | 'atanh'     '(' expression ')' {$ok=$expression.ok;}
  | 'ceil'      '(' expression ')' {$ok=$expression.ok;}
  | 'cos'       '(' expression ')' {$ok=$expression.ok;}
  | 'cosh'      '(' expression ')' {$ok=$expression.ok;}
  | 'exp'       '(' expression ')' {$ok=$expression.ok;}
  | 'expm1'     '(' expression ')' {$ok=$expression.ok;}
  | 'floor'     '(' expression ')' {$ok=$expression.ok;}
  | 'log'       '(' expression ')' {$ok=$expression.ok;}
  | 'log10'     '(' expression ')' {$ok=$expression.ok;}
  | 'log2'      '(' expression ')' {$ok=$expression.ok;}
  | 'log1p'     '(' expression ')' {$ok=$expression.ok;}
  | 'round'     '(' expression ')' {$ok=$expression.ok;}
  | 'sin'       '(' expression ')' {$ok=$expression.ok;}
  | 'sinc'      '(' expression ')' {$ok=$expression.ok;}
  | 'sinh'      '(' expression ')' {$ok=$expression.ok;}
  | 'sec'       '(' expression ')' {$ok=$expression.ok;}
  | 'csc'       '(' expression ')' {$ok=$expression.ok;}
  | 'sqrt'      '(' expression ')' {$ok=$expression.ok;}
  | 'tan'       '(' expression ')' {$ok=$expression.ok;}
  | 'tanh'      '(' expression ')' {$ok=$expression.ok;}
  | 'cot'       '(' expression ')' {$ok=$expression.ok;}
  | 'rad2deg'   '(' expression ')' {$ok=$expression.ok;}
  | 'deg2rad'   '(' expression ')' {$ok=$expression.ok;}
  | 'deg2grad'  '(' expression ')' {$ok=$expression.ok;}
  | 'grad2deg'  '(' expression ')' {$ok=$expression.ok;}
  | 'sgn'       '(' expression ')' {$ok=$expression.ok;}
  | 'not'       '(' expression ')' {$ok=$expression.ok;}
  | 'erf'       '(' expression ')' {$ok=$expression.ok;}
  | 'erfc'      '(' expression ')' {$ok=$expression.ok;}
  | 'ncdf'      '(' expression ')' {$ok=$expression.ok;}
  | 'frac'      '(' expression ')' {$ok=$expression.ok;}
  | 'trunc'     '(' expression ')' {$ok=$expression.ok;}
  // Funciones generales de dos argumentos
  | 'atan2'     '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'mod'       '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'logn'      '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'pow'       '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'root'      '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'roundn'    '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'equal'     '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'not_equal' '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'hypot'     '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'shr'       '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  | 'shl'       '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ')'
  // Funciones generales de tres argumentos
  | 'clamp'     '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ',' exp3=expression {$ok&=$exp3.ok;} ')'  
  | 'iclamp'    '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ',' exp3=expression {$ok&=$exp3.ok;} ')'
  | 'inrange'   '(' exp1=expression {$ok=$exp1.ok;} ',' exp2=expression {$ok&=$exp2.ok;} ',' exp3=expression {$ok&=$exp3.ok;} ')'
  // Funciones con multiples argumentos
  | 'sum'     '(' exp1=expression {$ok=$exp1.ok;} (',' exp2=expression {$ok&=$exp2.ok;})* ')'
  | 'mul'     '(' exp1=expression {$ok=$exp1.ok;} (',' exp2=expression {$ok&=$exp2.ok;})* ')'
  | 'min'     '(' exp1=expression {$ok=$exp1.ok;} (',' exp2=expression {$ok&=$exp2.ok;})* ')'
  | 'max'     '(' exp1=expression {$ok=$exp1.ok;} (',' exp2=expression {$ok&=$exp2.ok;})* ')'
  | 'avg'     '(' exp1=expression {$ok=$exp1.ok;} (',' exp2=expression {$ok&=$exp2.ok;})* ')'
  ;
//*/
//= Lexer Rules ==============================================================|
COLUMN
  : ('c'|'C') ('0'..'9')+
  ;

INT
  : ('0'..'9')+
  ;

FLOAT
  : ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
  |             '.' ('0'..'9')+ EXPONENT?
  | ('0'..'9')+                 EXPONENT
  ;

fragment
EXPONENT
  : ('e'|'E') ('+'|'-')? ('0'..'9')+
  ;

WHITESPACE
  : (' ' | '\t' | '\r' | '\n' | '\u000C')+ { $channel=HIDDEN; }
  ;

COMMENT
  : '//' ~('\n'|'\r')* '\r'? '\n'              {$channel=HIDDEN;}
  | '#'  ~('\n'|'\r')* '\r'? '\n'              {$channel=HIDDEN;}
  | '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
  ;