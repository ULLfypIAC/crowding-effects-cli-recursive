/**
|-----------------------------------------------------------------------------|
| Overall ANTLR Grammar File Structure                                        |
| [https://theantlrguy.atlassian.net/wiki/display/ANTLR3/Grammars]            |
|-----------------------------------------------------------------------------|
| «grammarType» grammar name;                                                 |
|   «optionsSpec»     = options { ... }                                       |
|   «tokensSpec»      = tokens { ... }                                        |
|   «attributeScopes» = «@action-scope-name::action-name» { ... }             |
|   «actions»                                                                 |
|   «rules»                                                                   |
|_____________________________________________________________________________|
|   «grammarType»         = ( lexer | parser | tree | <empty> )               |
|   «optionsSpec»         = options { ... }                                   |
|   «tokensSpec»          = tokens { ... }                                    |
|   «attributeScopes»     =                                                   |
|   «actions»             = @«action-scope-name»::«action-name» { ... }       |
|    «action-scope-name» = ( lexer | parser | tree | <empty> = parser )       |
|    «action-name»       = ( header | members | rulecatch | synpredgate | ...)|
|-----------------------------------------------------------------------------|
*/
grammar StarAnalyzer;

//= Options ==================================================================|
options {
  language = Cpp;
  backtrack = true;
  k = 2;
}// options

//= Lexer Sections ===========================================================|
@lexer::traits {
  /**
   * Esta sección es necesaria cuando el lenguaje generado es C++.
   */
  class StarAnalyzerLexer;
  class StarAnalyzerParser;
  /**
   * Objeto que permite modificar el comportamiento por defecto
   * del ANTLR para C++.
   *
   * Si no se desea modificar el comportamiento por defecto, del 
   * analizador basta con borrar esta clase del 'typedef' de más
   * abajo.
   *
   * Para mayor información debes ver el fichero "antlr3traits.hpp".
   */
  template<class ImplTraits>
  class UserTraits : public antlr3::CustomTraitsBase<ImplTraits> {
    public:
      /**
       * Función invocada cuando existe algún error léxico.
       * 
       */
      static void displayRecognitionError(const std::string &str) {
        std::ofstream out("StarAnalyzer.log",
                          std::ofstream::out | std::ofstream::app);
        out << str;
        out.close();
      }
  };
  typedef antlr3::Traits<StarAnalyzerLexer, StarAnalyzerParser, UserTraits> StarAnalyzerLexerTraits;
}// traits
@lexer::namespace { StarAnalyzer }
@lexer::init {
}//init
@lexer::header {
}//header
@lexer::includes {
  #include <fstream>
}// includes
@lexer::members {
  /**
   * Variable booleana que es utilizada como un habilitador de predicado
   * semántico (Gated semantic predicate) para una regla léxica que consume
   * cualquier caracter, excepto el EOF.
   *
   * Ver:
   * http://stackoverflow.com/questions/8553907/antlr-parsing-header-followed-by-binary-data-chunk-with-unknown-length
   *
   */
  //bool hasNaN;
}// members

//= Parser Sections ==========================================================|
@parser::traits {
  /**
   * Esta sección es necesaria cuando el lenguaje generado es C++.
   */
  typedef StarAnalyzerLexerTraits StarAnalyzerParserTraits;
}// traits
@parser::namespace { StarAnalyzer }
@parser::init {
}//init
@parser::header {
}//header
@parser::includes {
  #include <iostream>
  #include <string>
  #include "StarAnalyzerLexer.hpp"

  namespace StarAnalyzer {
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   float              *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line);
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   double             *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line);
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   long double        *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line);
  }// namespace
}// includes
@parser::postinclude {
}//postinclude
@parser::members {
  /**
   *  Número máximo de columnas permitidas en la gramática de expresiones.
   */
  unsigned int max;
  /**
   *  Stream en el que se incluirán los números reales como texto y serán
   *  extraídos como float, double o long double, según la función invocada.
   */
  std::stringstream ss;
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   float              *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line) {
    bool retVal = false;
    max = _max;
    StarAnalyzerLexer::InputStreamType input((ANTLR_UINT8*)text.c_str(),
                                                 ANTLR_ENC_8BIT,
                                                 text.length(),
                                                 (ANTLR_UINT8*)whereis.c_str());
    input.set_line((ANTLR_UINT32) line);
    StarAnalyzerLexer lexer(&input);
    StarAnalyzerParser::TokenStreamType tokenStream(ANTLR_SIZE_HINT,
                                                    lexer.get_tokSource());
    StarAnalyzerParser parser(&tokenStream);
    retVal = parser.star();
    retVal &= lexer.get_lexstate()->get_errorCount() == 0;
    retVal &= parser.get_psrstate()->get_errorCount() == 0;
    if (retVal) {
      for (unsigned int i(0); i < _max; i++) {
        ss >> ptr[i];
      }
    }
    return retVal;
  }
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   double             *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line) {
    bool retVal = false;
    max = _max;
    StarAnalyzerLexer::InputStreamType input((ANTLR_UINT8*)text.c_str(),
                                                 ANTLR_ENC_8BIT,
                                                 text.length(),
                                                 (ANTLR_UINT8*)whereis.c_str());
    input.set_line((ANTLR_UINT32) line);
    StarAnalyzerLexer lexer(&input);
    StarAnalyzerParser::TokenStreamType tokenStream(ANTLR_SIZE_HINT,
                                                    lexer.get_tokSource());
    StarAnalyzerParser parser(&tokenStream);
    retVal = parser.star();
    retVal &= lexer.get_lexstate()->get_errorCount() == 0;
    retVal &= parser.get_psrstate()->get_errorCount() == 0;
    if (retVal) {
      for (unsigned int i(0); i < _max; i++) {
        ss >> ptr[i];
      }
    }
    return retVal;
  }
  /**
   * Analiza una cadena de caracteres que debe contener valores reales,
   * introduce dichos valores en un vector lineal de números reales e indica
   * si el vector se pudo o no rellenar satisfatoriamente hasta donde indica
   * el valor del parámetro _max.
   * @param  text      Cadena de caracteres que debe contener valores reales
   *                   separados por espacios y/o tabuladores
   * @param  ptr       Puntero a un vector lineal de números reales donde se
   *                   guardará la información contenida en el parámetro text.
   * @param  _max      Número máximo de elementos en el vector al que apunta
   *                   el parámetro ptr.
   * @param  whereis   Cadena de caracteres de donde procede el parámetro 
   *                   text.
   * @param  line      Número de línea donde se encuentra el parámetro text.
   * @return true      Si el vector lineal de números reales, al que apunta 
   *                   ptr, se ha rellenado hasta alcanzar el n-ésimo elemento,
   *                   indicado por el parámetro _max.
   *         false     En caso contrario.
   */
  bool fillColumns(const std::string   &text,
                   long double        *&ptr,
                   const unsigned int  &_max,
                   const std::string   &whereis,
                   const unsigned int  &line) {
    bool retVal = false;
    max = _max;
    StarAnalyzerLexer::InputStreamType input((ANTLR_UINT8*)text.c_str(),
                                                 ANTLR_ENC_8BIT,
                                                 text.length(),
                                                 (ANTLR_UINT8*)whereis.c_str());
    input.set_line((ANTLR_UINT32) line);
    StarAnalyzerLexer lexer(&input);
    StarAnalyzerParser::TokenStreamType tokenStream(ANTLR_SIZE_HINT,
                                                    lexer.get_tokSource());
    StarAnalyzerParser parser(&tokenStream);
    retVal = parser.star();
    retVal &= lexer.get_lexstate()->get_errorCount() == 0;
    retVal &= parser.get_psrstate()->get_errorCount() == 0;
    if (retVal) {
      for (unsigned int i(0); i < _max; i++) {
        ss >> ptr[i];
      }
    }
    return retVal;
  }
}// members

//= Grammar Rules ============================================================|
star
returns [bool ok]
@init {
  $ok=false;
  unsigned int i(0);
  ss.str("");
  ss.clear();
}
@after {
}
  : ( number=NUMBER
      {
        ss << $number.text << " ";
        i++;
        if (i == max) {
          $ok=true;
          return $ok;
        }
      }
  /*
    | NaN
      {
        if (i < max) {
          $ok = false;
          return $ok;
        }
      }
  //*/
    )* EOF
  ;

//= Lexer Rules ==============================================================|
NUMBER
  : SIGN? ('0'..'9')+ '.' ('0'..'9')* EXPONENT?
  | SIGN?             '.' ('0'..'9')+ EXPONENT?
  | SIGN? ('0'..'9')+                 EXPONENT?
  ;

NaN
  : SIGN? ~('0'..'9'|'.'|SIGN|WS) ~(WS)* //{hasNaN=true;}
  ;


WHITESPACE
  : (WS)+ { $channel=HIDDEN; }
  ;

/*
IGNORE
  : ({hasNaN}?=> .)+
  ;
*/

fragment
EXPONENT
  : ('e'|'E') SIGN? ('0'..'9')+
  ;

fragment
SIGN
  : '+'
  | '-'
  ;

fragment
WS
  : ' '
  | '\t'
  | '\u000C'
  ;

//============================================================================|
/* Ejemplo en JAVA
grammar StarAnalizer;

@lexer::members {
  private boolean hasNaN = false;
}

star
returns [boolean ok]
@init {
  int n=0;
  $ok=true;
}
@after {
  System.out.println("n: "+n);
  System.out.println("ok: "+$ok);
}
  : ( {$ok}?=> good {$ok=$good.ok;}
    | {!$ok}?=> bad
    )* EOF
  ;

good
returns [boolean ok]
  : number=NUMBER {$ok=true; System.out.println("value: "+$number.text);}
  | NaN        {$ok=false;}
  ;
  
bad
  : IGNORE
  ;

//= Lexer Rules ==============================================================|
NUMBER
returns [float value]
  : SIGN? ('0'..'9')+ '.' ('0'..'9')* EXPONENT? {$value=Float.parseFloat($text);}
  | SIGN?             '.' ('0'..'9')+ EXPONENT? {$value=Float.parseFloat($text);}
  | SIGN? ('0'..'9')+                 EXPONENT? {$value=Float.parseFloat($text);}
  ;

fragment
EXPONENT
  : ('e'|'E') SIGN? ('0'..'9')+
  ;

NaN
  : SIGN? ~('0'..'9'|'.'|SIGN|WS) ~(WS)* {hasNaN=true;}
  ;

WHITESPACE
  : (WS)+ { $channel=HIDDEN; }
  ;

IGNORE
  : ({hasNaN}?=> .)+
  ;

fragment
SIGN
  : '+'
  | '-'
  ;

fragment
WS
  : ' '
  | '\r'
  | '\n'
  | '\t'
  | '\u000C'
  ;

*/