################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../sources/test/Cpp.cc \
../sources/test/Element.cc \
../sources/test/Index.cc \
../sources/test/List.cc \
../sources/test/Node.cc \
../sources/test/Tree.cc \
../sources/test/json.cc \
../sources/test/runUnitTests.cc \
../sources/test/traits.cc 

CC_DEPS += \
./sources/test/Cpp.d \
./sources/test/Element.d \
./sources/test/Index.d \
./sources/test/List.d \
./sources/test/Node.d \
./sources/test/Tree.d \
./sources/test/json.d \
./sources/test/runUnitTests.d \
./sources/test/traits.d 

OBJS += \
./sources/test/Cpp.o \
./sources/test/Element.o \
./sources/test/Index.o \
./sources/test/List.o \
./sources/test/Node.o \
./sources/test/Tree.o \
./sources/test/json.o \
./sources/test/runUnitTests.o \
./sources/test/traits.o 


# Each subdirectory must supply rules for building sources it contributes
sources/test/%.o: ../sources/test/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -D__cplusplus=201103L -I"/home/anthony/Projects/crowding-effects-cli/includes" -I"/home/anthony/Projects/crowding-effects-cli/sources" -O0 -g3 -Wall -c -fmessage-length=0 -std=c++11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


